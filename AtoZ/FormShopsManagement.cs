﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormShopsManagement : Form
    {
        public FormShopsManagement()
        {
            InitializeComponent();
        }

        private void shopsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.shopsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.atoZDatabaseDataSet_Shops);
            ShopsDetailHelper.refreshData();

        }

        private void FormShopsManagement_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet_Shops.shops' table. You can move, or remove it, as needed.
            this.shopsTableAdapter.Fill(this.atoZDatabaseDataSet_Shops.shops);

        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            foreach(DataGridViewRow row in this.shopsDataGridView.Rows)
            {
                if (row.Cells[1] == null || row.Cells[1].Value == null || String.IsNullOrWhiteSpace(row.Cells[1].Value.ToString()))
                    continue;

                if(row.Cells[1].Value.ToString().ToLower().Contains(this.textBoxSearch.Text.ToLower()))
                {
                    row.Cells[1].Style.BackColor = Color.Green;
                }
                else
                {
                    row.Cells[1].Style.BackColor = Color.White;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormDebugShipmentReceived : Form
    {
        public FormDebugShipmentReceived()
        {
            InitializeComponent();
        }

        private void shipmentReceivedBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.shipmentReceivedBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.atoZDatabaseDataSet_ShipmentReceived);

        }

        private void FormDebugShipmentReceived_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet_ShipmentReceived.ShipmentReceived' table. You can move, or remove it, as needed.
            this.shipmentReceivedTableAdapter.Fill(this.atoZDatabaseDataSet_ShipmentReceived.ShipmentReceived);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormDebugShopStatus : Form
    {
        public FormDebugShopStatus()
        {
            InitializeComponent();
        }

        private void shopStatusBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.shopStatusBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.atoZDatabaseDataSet_ShopStatus);

        }

        private void FormDebugShopStatus_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet_ShopStatus.shopStatus' table. You can move, or remove it, as needed.
            this.shopStatusTableAdapter.Fill(this.atoZDatabaseDataSet_ShopStatus.shopStatus);

        }
    }
}

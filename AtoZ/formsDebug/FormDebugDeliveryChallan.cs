﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormDebugDeliveryChallan : Form
    {
        public FormDebugDeliveryChallan()
        {
            InitializeComponent();
        }

        private void deliveryChallanBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.deliveryChallanBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.atoZDatabaseDataSet_DeliveryChallan);

        }

        private void FormDebugDeliveryChallan_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan' table. You can move, or remove it, as needed.
            this.deliveryChallanTableAdapter.Fill(this.atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan);

        }
    }
}

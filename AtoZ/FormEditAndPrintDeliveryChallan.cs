﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormEditAndPrintDeliveryChallan : Form
    {
        private Boolean isDataDirty = false;

        public FormEditAndPrintDeliveryChallan()
        {
            InitializeComponent();
        }

        private void deliveryChallanBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            int itemCount = 0;
            List<AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow> rowsToBeRemoved = 
                new List<AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow>();
            foreach(AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow row in
                this.atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan.Rows)
            {
                if (row.itemQuantity == 0)
                {
                    rowsToBeRemoved.Add(row);
                }
                else
                {
                    row.challanDate = dateTimePicker1.Value;
                    if (!string.IsNullOrWhiteSpace(textBoxNewOrderId.Text))
                    {
                        row.orderNo = textBoxNewOrderId.Text;
                    }
                    itemCount++;
                }
                
            }

            

            var response = MessageBox.Show("Are you sure you want to save changes? New item cout : " +
                itemCount, "", MessageBoxButtons.YesNo);
            if(response.Equals(DialogResult.No))
            {
                return;
            }

            //update inventory by adding it back to inventory stock first
            addInventory(getChallanId());

            this.deliveryChallanDataGridView.DataSource = null;
            rowsToBeRemoved.ForEach(x => x.Delete());
            this.deliveryChallanDataGridView.DataSource = this.deliveryChallanBindingSource; 
            
            

            this.Validate();
            this.deliveryChallanBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.atoZDatabaseDataSet_DeliveryChallan);

            //update inventory stock
            ClassUpdateInventory updateInventory = new ClassUpdateInventory();
            updateInventory.removeInventory(this.atoZDatabaseDataSet_DeliveryChallan);

            // refresh view
            this.RefreshDeliveryChallanDetailTable();
            this.refrestTableRowNumber();

            this.dataGridView1.CurrentRow.Cells[1].Value = dateTimePicker1.Value.ToShortDateString();

            this.isDataDirty = false;
            this.dateTimePicker1.Font = new Font(this.dateTimePicker1.Font.FontFamily, 8.25F);
            this.textBoxNewOrderId.BackColor = Color.White;
        }

        private void addInventory(int challanId)
        {
            AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter tableAdapter =
                new AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();

            // read record set first
            AtoZDatabaseDataSet_DeliveryChallan tempDataset = new AtoZDatabaseDataSet_DeliveryChallan();
            tableAdapter.FillBy(tempDataset.DeliveryChallan, challanId);

            //update inventory
            ClassUpdateInventory updateInventory = new ClassUpdateInventory();
            updateInventory.addInventory(tempDataset);

        }


        private void FormEditAndPrintDeliveryChallan_Load(object sender, EventArgs e)
        {

            // TODO: This line of code loads data into the 'atoZDatabaseDataSet13.DeliveryChallan' table. You can move, or remove it, as needed.
            //this.deliveryChallanTableAdapter1.Fill(this.atoZDatabaseDataSet13.DeliveryChallan);
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet10.shops' table. You can move, or remove it, as needed.
            this.shopsTableAdapter.Fill(this.atoZDatabaseDataSet10.shops);
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan' table. You can move, or remove it, as needed.
            // following line is required so that correct row id is picked for new records
            // PK logic moved to DB by making property IDENTITY this.deliveryChallanTableAdapter.FillByGetOneRowWithMaxId(this.atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan);
           
            //select first row of shop list
            shopsDataGridView_CellClick(null, null);
        }


        private void shopsDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.updateChallanListTable();
                this.dataGridView1_CellClick(null, null);
                
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                RefreshDeliveryChallanDetailTable();
                refrestTableRowNumber();
                this.dateTimePicker1.Value =  DateTime.Parse(dataGridView1.CurrentRow.Cells[1].Value.ToString());
                
                if(this.checkBoxShowAllChallans.Checked)
                {
                    //select the shop for this challan
                    int shopId = ((AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow)
                        this.atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan.Rows[0]).shopId;

                    HelperMethods.selectRowOnCol0(shopId, this.shopsDataGridView);
                }
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }


        private void RefreshDeliveryChallanDetailTable()
        {
            this.deliveryChallanTableAdapter.FillBy(this.atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan,
                getChallanId());
           
            this.textBoxNewOrderId.Text = ((AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow)
                this.atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan.Rows[0]).IsorderNoNull()?
                "":
                ((AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow)
                this.atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan.Rows[0]).orderNo;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(isDataDirty)
            {
                MessageBox.Show("Please save changes before printing");
                return;
            }

            FormChallanReport form = new  FormChallanReport();
            form.setDataSource(this.atoZDatabaseDataSet_DeliveryChallan);
            form.Show();
        }

        private void buttonMergeChallan_Click(object sender, EventArgs e)
        {
            Merge(true);
        }

        private void Merge(Boolean merge)
        {
            if (isDataDirty)
            {
                MessageBox.Show("Please save changes before merge/unmerge");
                return;
            }

            int challaId = getChallanId();
            int itemCount = deliveryChallanDataGridView.Rows.Count;
            String shopName = shopsDataGridView.CurrentRow.Cells[1].Value.ToString();

            var result = MessageBox.Show("Are you sure you want to merge/unmerge challan # " + challaId + " of shop " + shopName + " with " + itemCount + " items",
                    "Confirm Merge",
                    MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
            {
                return;
            }
            else
            {
                // update inventory and shop stock
                AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter tableAdapter =
                new AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();

                // read record set first
                AtoZDatabaseDataSet_DeliveryChallan tempDataset = new AtoZDatabaseDataSet_DeliveryChallan();
                tableAdapter.FillBy(tempDataset.DeliveryChallan, challaId);

                //update inventory
                ClassUpdateInventory updateInventory = new ClassUpdateInventory();
                if (merge)
                {
                    updateInventory.removeInventory(tempDataset);
                }
                else
                {
                    updateInventory.addInventory(tempDataset);
                }

                MessageBox.Show("Merge/unmerge complete");
                return;
            }
        }

      
        private void buttonUnmergeChallan_Click(object sender, EventArgs e)
        {
            Merge(false);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonDeleteChallan_Click(object sender, EventArgs e)
        {
            if (isDataDirty)
            {
                MessageBox.Show("Please save changes before deleteing");
                return;
            }

            try
            {
                String shopName = shopsDataGridView.CurrentRow.Cells[1].Value.ToString();
                int challaId = getChallanId();
                int itemCount = deliveryChallanDataGridView.Rows.Count ;
                var result = MessageBox.Show("Are you sure you want to delete challan # " + challaId + " of shop " + shopName + " with " + itemCount + " items",
                    "Confirm Delete",
                    MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    return;
                }
                else
                {
                    int rows = DeleteDeliveryChallan(challaId);

                    this.atoZDatabaseDataSet_DeliveryChallan.Clear();

                    MessageBox.Show( rows + " Row deleted");

                    //refresh view
                    shopsDataGridView_CellClick(null, null);

                    // clear show all flag
                    if (this.checkBoxShowAllChallans.Checked)
                    {
                        this.ignoreNextEventForShowAllCheckBox = true;
                        this.checkBoxShowAllChallans.Checked = false;
                    }
                    
                    
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private int getChallanId()
        {
            int challaId = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            return challaId;
        }

        private static int DeleteDeliveryChallan(int challaId)
        {
            AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter tableAdapter =
                new AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();

            // read record set first
            AtoZDatabaseDataSet_DeliveryChallan tempDataset = new AtoZDatabaseDataSet_DeliveryChallan();
            tableAdapter.FillBy(tempDataset.DeliveryChallan, challaId);

            // delete dataset from table
            int rows = tableAdapter.DeleteChallanById(challaId); 
            Console.WriteLine("Rows Deleted:" + rows);

            //update inventory
            ClassUpdateInventory updateInventory = new ClassUpdateInventory();
            updateInventory.addInventory(tempDataset);

            return rows;
        }

        private void buttonAddMoreItems_Click(object sender, EventArgs e)
        {
            List<int> currentItemIds = new List<int>();
            foreach(AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow 
                row in this.atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan.Rows)
            {
                currentItemIds.Add(row.itemCatNo);
            }
            int shopId = ((AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow)
                        this.atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan.Rows[0]).shopId;

            
            Dictionary<int, ItemDetailsData> allItems;

            if(ShopsDetailHelper.isPostTaxShop(shopId))
            {
                allItems = ItemDetailsHelper.getAllPostTaxItems();
            }
            else
            {
                allItems = ItemDetailsHelper.getAllItems();
            }

            foreach(var kvp in allItems)
            {
                if(!currentItemIds.Contains(kvp.Key) && kvp.Value.category == ItemCategory.DeliveryChallanOnly)
                {
                    
                        AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow newRow = 
                            (AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow)
                            this.atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan.NewRow();
                    newRow.itemCatNo = kvp.Key;
                    newRow.itemName = kvp.Value.name;
                    newRow.challanDate = new DateTime();// date is picked with save is clicked
                    newRow.challanId = getChallanId();
                    //newRow.itemPrice = 0;
                    newRow.itemQuantity = 0;
                    //newRow.itemRemarks = ;
                    newRow.orderNo = null;
                    newRow.shopId = int.Parse(this.shopsDataGridView.CurrentRow.Cells[0].Value.ToString());
                    //newRow.shopName = this.shopsDataGridView.CurrentRow.Cells[1].Value.ToString();

                    this.atoZDatabaseDataSet_DeliveryChallan.DeliveryChallan.AddDeliveryChallanRow(newRow);
                }
            }

            refrestTableRowNumber();
        }

        void refrestTableRowNumber()
        {
            HelperMethods.setRowNumbers(this.deliveryChallanDataGridView);
        }

        void updateChallanListTable()
        {
            this.deliveryChallanTableAdapter1.FillBy(this.atoZDatabaseDataSet13.DeliveryChallan,
                   int.Parse(shopsDataGridView.CurrentRow.Cells[0].Value.ToString()));

            
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {

        }

        private void shopsDataGridView_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.shopsDataGridView_CellClick(null, null);
        }

        private void shopsDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            this.shopsDataGridView_CellClick(null, null);

        }

        private void shopsDataGridView_KeyUp(object sender, KeyEventArgs e)
        {
            this.shopsDataGridView_CellClick(null, null);

        }


        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.dataGridView1_CellClick(null, null);
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            this.dataGridView1_CellClick(null, null);
        }

        private void dataGridView1_KeyUp(object sender, KeyEventArgs e)
        {
            this.dataGridView1_CellClick(null, null);
        }

        private void deliveryChallanDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.deliveryChallanDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Red;
            this.isDataDirty = true;
        }

        Boolean ignoreNextEventForShowAllCheckBox = false;
        private void checkBoxShowAllChallans_CheckedChanged(object sender, EventArgs e)
        {
            if (ignoreNextEventForShowAllCheckBox) 
            {
                // ignore this event as we manually updated the flag below
                ignoreNextEventForShowAllCheckBox = false;
                return;
            }

            if (isDataDirty)
            {
                var resp = MessageBox.Show("You have unsaved changed that will be lost. Are you sure you want to proceed?",
                    "", MessageBoxButtons.YesNo);
                if (resp.Equals(DialogResult.No))
                {
                    ignoreNextEventForShowAllCheckBox = true;
                    checkBoxShowAllChallans.Checked = !checkBoxShowAllChallans.Checked;
                    return;
                }

            }

            int currentChallanId = -1;

            if (this.dataGridView1.CurrentRow!=null && this.dataGridView1.CurrentRow.Index >= 0)
            {
                currentChallanId = int.Parse(this.dataGridView1.CurrentRow.Cells[0].Value.ToString());
            }


            if (checkBoxShowAllChallans.Checked == true)
            {
                this.deliveryChallanTableAdapter1.Fill(this.atoZDatabaseDataSet13.DeliveryChallan);
                this.dataGridView1.Sort(this.dataGridView1.Columns[0], ListSortDirection.Ascending);
                
                if (currentChallanId != -1)
                {
                    HelperMethods.selectRowOnCol0(currentChallanId, this.dataGridView1);
                }
                
                this.dataGridView1_CellClick(null, null);

                this.shopsDataGridView.Enabled = false;
                //this.deliveryChallanBindingNavigatorSaveItem.Enabled = false;
                //this.buttonAddMoreItems.Enabled = false;
                //this.deliveryChallanDataGridView.Enabled = false;

            }
            else
            {
                
                this.deliveryChallanTableAdapter1.FillBy(this.atoZDatabaseDataSet13.DeliveryChallan,
                   int.Parse(shopsDataGridView.CurrentRow.Cells[0].Value.ToString()));

                if (currentChallanId != -1)
                {
                    HelperMethods.selectRowOnCol0(currentChallanId, this.dataGridView1);
                }

                this.dataGridView1_CellClick(null, null);

                this.shopsDataGridView.Enabled = true;
                //this.deliveryChallanBindingNavigatorSaveItem.Enabled = true;
                //this.buttonAddMoreItems.Enabled = true;
                //this.deliveryChallanDataGridView.Enabled = true;
            }

            this.isDataDirty = false;
        }


        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if(!this.dataGridView1.CurrentRow.Cells[1].Value.ToString().Equals(dateTimePicker1.Value.ToString()))
            {
                this.isDataDirty = true;
                this.dateTimePicker1.Font = new Font(this.dateTimePicker1.Font.FontFamily, 10.25F);
            }
        }

        private void textBoxNewOrderId_Enter(object sender, EventArgs e)
        {
            this.isDataDirty = true;
            this.textBoxNewOrderId.BackColor = Color.Red;

        }

        private void FormEditAndPrintDeliveryChallan_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isDataDirty)
            {
                var resp = MessageBox.Show("You have unsaved changed. Are you sure you want to close",
                    "",MessageBoxButtons.YesNo);
                if(resp.Equals(DialogResult.No))
                {
                    e.Cancel = true;
                }

            }
        }


    }
}

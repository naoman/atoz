﻿using AtoZ.reports;
using AtoZ.datasets;

namespace AtoZ
{
    partial class FormShopSalesInvoiceDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormShopSalesInvoiceDetails));
            this.richTextBoxTaxCatDetails = new System.Windows.Forms.RichTextBox();
            this.shopSalesInvoiceBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.shopSalesInvoiceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet_ShopSalesInvoice = new AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoice();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.shopSalesInvoiceBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.shopSalesInvoiceDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custom1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValExcTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taxPaid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.actualTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet8 = new AtoZ.datasets.AtoZDatabaseDataSet8();
            this.shopsTableAdapter = new AtoZ.datasets.AtoZDatabaseDataSet8TableAdapters.shopsTableAdapter();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.billIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.billDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopSalesInvoiceBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet9 = new AtoZ.datasets.AtoZDatabaseDataSet9();
            this.shopSalesInvoiceTableAdapter = new AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();
            this.tableAdapterManager = new AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.TableAdapterManager();
            this.buttonPrintBill = new System.Windows.Forms.Button();
            this.shopSalesInvoiceTableAdapter1 = new AtoZ.datasets.AtoZDatabaseDataSet9TableAdapters.ShopSalesInvoiceTableAdapter();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxSalesInvoiceId = new System.Windows.Forms.TextBox();
            this.dateTimePickerSalesInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerBillDate = new System.Windows.Forms.DateTimePicker();
            this.textBoxTaxRegNo = new System.Windows.Forms.TextBox();
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.buttonDeleteBill = new System.Windows.Forms.Button();
            this.buttonAddItems = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBoxShowAllBills = new System.Windows.Forms.CheckBox();
            this.labelKK = new System.Windows.Forms.Label();
            this.checkBoxTaxDetails = new System.Windows.Forms.CheckBox();
            this.buttonCalTax = new System.Windows.Forms.Button();
            this.label17Percent = new System.Windows.Forms.Label();
            this.label5Percent = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceBindingNavigator)).BeginInit();
            this.shopSalesInvoiceBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet_ShopSalesInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet9)).BeginInit();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBoxTaxCatDetails
            // 
            this.richTextBoxTaxCatDetails.Location = new System.Drawing.Point(9, 227);
            this.richTextBoxTaxCatDetails.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.richTextBoxTaxCatDetails.Name = "richTextBoxTaxCatDetails";
            this.richTextBoxTaxCatDetails.Size = new System.Drawing.Size(441, 66);
            this.richTextBoxTaxCatDetails.TabIndex = 23;
            this.richTextBoxTaxCatDetails.Text = "";
            // 
            // shopSalesInvoiceBindingNavigator
            // 
            this.shopSalesInvoiceBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.shopSalesInvoiceBindingNavigator.BindingSource = this.shopSalesInvoiceBindingSource;
            this.shopSalesInvoiceBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.shopSalesInvoiceBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.shopSalesInvoiceBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.shopSalesInvoiceBindingNavigatorSaveItem});
            this.shopSalesInvoiceBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.shopSalesInvoiceBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.shopSalesInvoiceBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.shopSalesInvoiceBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.shopSalesInvoiceBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.shopSalesInvoiceBindingNavigator.Name = "shopSalesInvoiceBindingNavigator";
            this.shopSalesInvoiceBindingNavigator.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.shopSalesInvoiceBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.shopSalesInvoiceBindingNavigator.Size = new System.Drawing.Size(958, 25);
            this.shopSalesInvoiceBindingNavigator.TabIndex = 0;
            this.shopSalesInvoiceBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Enabled = false;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // shopSalesInvoiceBindingSource
            // 
            this.shopSalesInvoiceBindingSource.DataMember = "ShopSalesInvoice";
            this.shopSalesInvoiceBindingSource.DataSource = this.atoZDatabaseDataSet_ShopSalesInvoice;
            // 
            // atoZDatabaseDataSet_ShopSalesInvoice
            // 
            this.atoZDatabaseDataSet_ShopSalesInvoice.DataSetName = "AtoZDatabaseDataSet_ShopSalesInvoice";
            this.atoZDatabaseDataSet_ShopSalesInvoice.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Enabled = false;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(27, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // shopSalesInvoiceBindingNavigatorSaveItem
            // 
            this.shopSalesInvoiceBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.shopSalesInvoiceBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("shopSalesInvoiceBindingNavigatorSaveItem.Image")));
            this.shopSalesInvoiceBindingNavigatorSaveItem.Name = "shopSalesInvoiceBindingNavigatorSaveItem";
            this.shopSalesInvoiceBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.shopSalesInvoiceBindingNavigatorSaveItem.Text = "Save Data";
            this.shopSalesInvoiceBindingNavigatorSaveItem.Click += new System.EventHandler(this.shopSalesInvoiceBindingNavigatorSaveItem_Click);
            // 
            // shopSalesInvoiceDataGridView
            // 
            this.shopSalesInvoiceDataGridView.AllowUserToAddRows = false;
            this.shopSalesInvoiceDataGridView.AllowUserToDeleteRows = false;
            this.shopSalesInvoiceDataGridView.AutoGenerateColumns = false;
            this.shopSalesInvoiceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.shopSalesInvoiceDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn7,
            this.itemTax,
            this.custom1,
            this.dataGridViewTextBoxColumn10,
            this.ValExcTax,
            this.TotalTax,
            this.TotalValue,
            this.taxPaid,
            this.actualTax});
            this.tableLayoutPanel1.SetColumnSpan(this.shopSalesInvoiceDataGridView, 3);
            this.shopSalesInvoiceDataGridView.DataSource = this.shopSalesInvoiceBindingSource;
            this.shopSalesInvoiceDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shopSalesInvoiceDataGridView.Location = new System.Drawing.Point(2, 263);
            this.shopSalesInvoiceDataGridView.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.shopSalesInvoiceDataGridView.Name = "shopSalesInvoiceDataGridView";
            this.shopSalesInvoiceDataGridView.RowHeadersWidth = 80;
            this.shopSalesInvoiceDataGridView.RowTemplate.Height = 33;
            this.shopSalesInvoiceDataGridView.Size = new System.Drawing.Size(954, 258);
            this.shopSalesInvoiceDataGridView.TabIndex = 1;
            this.shopSalesInvoiceDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.shopSalesInvoiceDataGridView_CellDoubleClick);
            this.shopSalesInvoiceDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.shopSalesInvoiceDataGridView_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "itemCatNo";
            this.dataGridViewTextBoxColumn3.HeaderText = "itemCatNo";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 81;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "itemName";
            this.dataGridViewTextBoxColumn5.HeaderText = "itemName";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 79;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "itemQuantity";
            this.dataGridViewTextBoxColumn4.HeaderText = "itemQuantity";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 90;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "itemPrice";
            this.dataGridViewTextBoxColumn7.HeaderText = "itemPrice";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 75;
            // 
            // itemTax
            // 
            this.itemTax.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.itemTax.DataPropertyName = "itemTax";
            this.itemTax.HeaderText = "itemTax";
            this.itemTax.Name = "itemTax";
            this.itemTax.Width = 69;
            // 
            // custom1
            // 
            this.custom1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.custom1.DataPropertyName = "custom1";
            this.custom1.HeaderText = "Actual Tax";
            this.custom1.Name = "custom1";
            this.custom1.Width = 77;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn10.DataPropertyName = "itemRemarks";
            this.dataGridViewTextBoxColumn10.HeaderText = "itemRemarks";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 93;
            // 
            // ValExcTax
            // 
            this.ValExcTax.HeaderText = "Val Exc Tax";
            this.ValExcTax.Name = "ValExcTax";
            this.ValExcTax.ReadOnly = true;
            // 
            // TotalTax
            // 
            this.TotalTax.HeaderText = "Total Tax";
            this.TotalTax.Name = "TotalTax";
            this.TotalTax.ReadOnly = true;
            // 
            // TotalValue
            // 
            this.TotalValue.HeaderText = "Total Val";
            this.TotalValue.Name = "TotalValue";
            this.TotalValue.ReadOnly = true;
            // 
            // taxPaid
            // 
            this.taxPaid.HeaderText = "Total Act Tax Paid";
            this.taxPaid.Name = "taxPaid";
            // 
            // actualTax
            // 
            this.actualTax.HeaderText = "Actual Tax Per item";
            this.actualTax.Name = "actualTax";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.shopsBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(2, 2);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(235, 257);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            this.dataGridView1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyUp);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Width = 41;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Width = 58;
            // 
            // shopsBindingSource
            // 
            this.shopsBindingSource.DataMember = "shops";
            this.shopsBindingSource.DataSource = this.atoZDatabaseDataSet8;
            // 
            // atoZDatabaseDataSet8
            // 
            this.atoZDatabaseDataSet8.DataSetName = "AtoZDatabaseDataSet8";
            this.atoZDatabaseDataSet8.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // shopsTableAdapter
            // 
            this.shopsTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.billIdDataGridViewTextBoxColumn,
            this.billDateDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.shopSalesInvoiceBindingSource1;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(241, 2);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 33;
            this.dataGridView2.Size = new System.Drawing.Size(187, 257);
            this.dataGridView2.TabIndex = 4;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            this.dataGridView2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView2_KeyDown);
            this.dataGridView2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridView2_KeyUp);
            // 
            // billIdDataGridViewTextBoxColumn
            // 
            this.billIdDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.billIdDataGridViewTextBoxColumn.DataPropertyName = "billId";
            this.billIdDataGridViewTextBoxColumn.HeaderText = "billId";
            this.billIdDataGridViewTextBoxColumn.Name = "billIdDataGridViewTextBoxColumn";
            this.billIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.billIdDataGridViewTextBoxColumn.Width = 53;
            // 
            // billDateDataGridViewTextBoxColumn
            // 
            this.billDateDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.billDateDataGridViewTextBoxColumn.DataPropertyName = "billDate";
            this.billDateDataGridViewTextBoxColumn.HeaderText = "billDate";
            this.billDateDataGridViewTextBoxColumn.Name = "billDateDataGridViewTextBoxColumn";
            this.billDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.billDateDataGridViewTextBoxColumn.Width = 67;
            // 
            // shopSalesInvoiceBindingSource1
            // 
            this.shopSalesInvoiceBindingSource1.DataMember = "ShopSalesInvoice";
            this.shopSalesInvoiceBindingSource1.DataSource = this.atoZDatabaseDataSet9;
            // 
            // atoZDatabaseDataSet9
            // 
            this.atoZDatabaseDataSet9.DataSetName = "AtoZDatabaseDataSet9";
            this.atoZDatabaseDataSet9.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // shopSalesInvoiceTableAdapter
            // 
            this.shopSalesInvoiceTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ShopSalesInvoiceTableAdapter = this.shopSalesInvoiceTableAdapter;
            this.tableAdapterManager.UpdateOrder = AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // buttonPrintBill
            // 
            this.buttonPrintBill.Location = new System.Drawing.Point(304, 82);
            this.buttonPrintBill.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonPrintBill.Name = "buttonPrintBill";
            this.buttonPrintBill.Size = new System.Drawing.Size(76, 38);
            this.buttonPrintBill.TabIndex = 8;
            this.buttonPrintBill.Text = "Print Bill";
            this.buttonPrintBill.UseVisualStyleBackColor = true;
            this.buttonPrintBill.Click += new System.EventHandler(this.buttonPrintBill_Click);
            // 
            // shopSalesInvoiceTableAdapter1
            // 
            this.shopSalesInvoiceTableAdapter1.ClearBeforeFill = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Terms of Sales";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 35);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Dated:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 61);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Bill Date:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 84);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Tax Reg No:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 107);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Status:";
            // 
            // textBoxSalesInvoiceId
            // 
            this.textBoxSalesInvoiceId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxSalesInvoiceId.Location = new System.Drawing.Point(112, 7);
            this.textBoxSalesInvoiceId.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxSalesInvoiceId.Name = "textBoxSalesInvoiceId";
            this.textBoxSalesInvoiceId.Size = new System.Drawing.Size(188, 20);
            this.textBoxSalesInvoiceId.TabIndex = 8;
            this.textBoxSalesInvoiceId.Enter += new System.EventHandler(this.textBoxSalesInvoiceId_Enter);
            // 
            // dateTimePickerSalesInvoiceDate
            // 
            this.dateTimePickerSalesInvoiceDate.Location = new System.Drawing.Point(66, 33);
            this.dateTimePickerSalesInvoiceDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimePickerSalesInvoiceDate.Name = "dateTimePickerSalesInvoiceDate";
            this.dateTimePickerSalesInvoiceDate.Size = new System.Drawing.Size(234, 20);
            this.dateTimePickerSalesInvoiceDate.TabIndex = 8;
            this.dateTimePickerSalesInvoiceDate.ValueChanged += new System.EventHandler(this.dateTimePickerSalesInvoiceDate_ValueChanged);
            // 
            // dateTimePickerBillDate
            // 
            this.dateTimePickerBillDate.Location = new System.Drawing.Point(66, 59);
            this.dateTimePickerBillDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimePickerBillDate.Name = "dateTimePickerBillDate";
            this.dateTimePickerBillDate.Size = new System.Drawing.Size(234, 20);
            this.dateTimePickerBillDate.TabIndex = 13;
            this.dateTimePickerBillDate.ValueChanged += new System.EventHandler(this.dateTimePickerBillDate_ValueChanged);
            // 
            // textBoxTaxRegNo
            // 
            this.textBoxTaxRegNo.Location = new System.Drawing.Point(76, 84);
            this.textBoxTaxRegNo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxTaxRegNo.Name = "textBoxTaxRegNo";
            this.textBoxTaxRegNo.Size = new System.Drawing.Size(224, 20);
            this.textBoxTaxRegNo.TabIndex = 14;
            this.textBoxTaxRegNo.Enter += new System.EventHandler(this.textBoxTaxRegNo_Enter);
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.Location = new System.Drawing.Point(58, 103);
            this.textBoxStatus.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.Size = new System.Drawing.Size(242, 20);
            this.textBoxStatus.TabIndex = 15;
            this.textBoxStatus.Enter += new System.EventHandler(this.textBoxStatus_Enter);
            // 
            // buttonDeleteBill
            // 
            this.buttonDeleteBill.Location = new System.Drawing.Point(304, 7);
            this.buttonDeleteBill.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonDeleteBill.Name = "buttonDeleteBill";
            this.buttonDeleteBill.Size = new System.Drawing.Size(76, 24);
            this.buttonDeleteBill.TabIndex = 8;
            this.buttonDeleteBill.Text = "Delete Bill";
            this.buttonDeleteBill.UseVisualStyleBackColor = true;
            this.buttonDeleteBill.Click += new System.EventHandler(this.buttonDeleteBill_Click);
            // 
            // buttonAddItems
            // 
            this.buttonAddItems.Location = new System.Drawing.Point(304, 44);
            this.buttonAddItems.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonAddItems.Name = "buttonAddItems";
            this.buttonAddItems.Size = new System.Drawing.Size(76, 24);
            this.buttonAddItems.TabIndex = 16;
            this.buttonAddItems.Text = "Add Items";
            this.buttonAddItems.UseVisualStyleBackColor = true;
            this.buttonAddItems.Click += new System.EventHandler(this.buttonAddItems_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.richTextBoxTaxCatDetails);
            this.panel1.Controls.Add(this.checkBoxShowAllBills);
            this.panel1.Controls.Add(this.labelKK);
            this.panel1.Controls.Add(this.checkBoxTaxDetails);
            this.panel1.Controls.Add(this.buttonCalTax);
            this.panel1.Controls.Add(this.label17Percent);
            this.panel1.Controls.Add(this.label5Percent);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.buttonAddItems);
            this.panel1.Controls.Add(this.buttonPrintBill);
            this.panel1.Controls.Add(this.buttonDeleteBill);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBoxStatus);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.textBoxTaxRegNo);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.dateTimePickerBillDate);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.dateTimePickerSalesInvoiceDate);
            this.panel1.Controls.Add(this.textBoxSalesInvoiceId);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(432, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(524, 257);
            this.panel1.TabIndex = 17;
            // 
            // checkBoxShowAllBills
            // 
            this.checkBoxShowAllBills.AutoSize = true;
            this.checkBoxShowAllBills.Location = new System.Drawing.Point(263, 131);
            this.checkBoxShowAllBills.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBoxShowAllBills.Name = "checkBoxShowAllBills";
            this.checkBoxShowAllBills.Size = new System.Drawing.Size(87, 17);
            this.checkBoxShowAllBills.TabIndex = 22;
            this.checkBoxShowAllBills.Text = "Show all Bills";
            this.checkBoxShowAllBills.UseVisualStyleBackColor = true;
            this.checkBoxShowAllBills.CheckedChanged += new System.EventHandler(this.checkBoxShowAllBills_CheckedChanged);
            // 
            // labelKK
            // 
            this.labelKK.AutoSize = true;
            this.labelKK.Location = new System.Drawing.Point(6, 198);
            this.labelKK.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelKK.Name = "labelKK";
            this.labelKK.Size = new System.Drawing.Size(24, 13);
            this.labelKK.TabIndex = 21;
            this.labelKK.Text = "KK:";
            // 
            // checkBoxTaxDetails
            // 
            this.checkBoxTaxDetails.AutoSize = true;
            this.checkBoxTaxDetails.Location = new System.Drawing.Point(99, 131);
            this.checkBoxTaxDetails.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBoxTaxDetails.Name = "checkBoxTaxDetails";
            this.checkBoxTaxDetails.Size = new System.Drawing.Size(103, 17);
            this.checkBoxTaxDetails.TabIndex = 20;
            this.checkBoxTaxDetails.Text = "Print Tax Details";
            this.checkBoxTaxDetails.UseVisualStyleBackColor = true;
            // 
            // buttonCalTax
            // 
            this.buttonCalTax.Location = new System.Drawing.Point(9, 127);
            this.buttonCalTax.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonCalTax.Name = "buttonCalTax";
            this.buttonCalTax.Size = new System.Drawing.Size(76, 20);
            this.buttonCalTax.TabIndex = 19;
            this.buttonCalTax.Text = "Cal Tax";
            this.buttonCalTax.UseVisualStyleBackColor = true;
            this.buttonCalTax.Click += new System.EventHandler(this.buttonCalTax_Click);
            // 
            // label17Percent
            // 
            this.label17Percent.AutoSize = true;
            this.label17Percent.Location = new System.Drawing.Point(6, 161);
            this.label17Percent.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17Percent.Name = "label17Percent";
            this.label17Percent.Size = new System.Drawing.Size(21, 13);
            this.label17Percent.TabIndex = 18;
            this.label17Percent.Text = "G: ";
            // 
            // label5Percent
            // 
            this.label5Percent.AutoSize = true;
            this.label5Percent.Location = new System.Drawing.Point(6, 179);
            this.label5Percent.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5Percent.Name = "label5Percent";
            this.label5Percent.Size = new System.Drawing.Size(18, 13);
            this.label5Percent.TabIndex = 17;
            this.label5Percent.Text = "H:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.shopSalesInvoiceDataGridView, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(958, 523);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // FormShopSalesInvoiceDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 548);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.shopSalesInvoiceBindingNavigator);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormShopSalesInvoiceDetails";
            this.Text = "FormShopSalesInvoiceDetails";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormShopSalesInvoiceDetails_FormClosing);
            this.Load += new System.EventHandler(this.FormShopSalesInvoiceDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceBindingNavigator)).EndInit();
            this.shopSalesInvoiceBindingNavigator.ResumeLayout(false);
            this.shopSalesInvoiceBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet_ShopSalesInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet9)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AtoZDatabaseDataSet_ShopSalesInvoice atoZDatabaseDataSet_ShopSalesInvoice;
        private System.Windows.Forms.BindingSource shopSalesInvoiceBindingSource;
        private AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter shopSalesInvoiceTableAdapter;
        private AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator shopSalesInvoiceBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton shopSalesInvoiceBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView shopSalesInvoiceDataGridView;
        private System.Windows.Forms.DataGridView dataGridView1;
        private AtoZDatabaseDataSet8 atoZDatabaseDataSet8;
        private System.Windows.Forms.BindingSource shopsBindingSource;
        private AtoZ.datasets.AtoZDatabaseDataSet8TableAdapters.shopsTableAdapter shopsTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView2;
        private AtoZDatabaseDataSet9 atoZDatabaseDataSet9;
        private System.Windows.Forms.BindingSource shopSalesInvoiceBindingSource1;
        private AtoZ.datasets.AtoZDatabaseDataSet9TableAdapters.ShopSalesInvoiceTableAdapter shopSalesInvoiceTableAdapter1;
        private System.Windows.Forms.Button buttonPrintBill;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxSalesInvoiceId;
        private System.Windows.Forms.DateTimePicker dateTimePickerSalesInvoiceDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerBillDate;
        private System.Windows.Forms.TextBox textBoxTaxRegNo;
        private System.Windows.Forms.TextBox textBoxStatus;
        private System.Windows.Forms.Button buttonDeleteBill;
        private System.Windows.Forms.Button buttonAddItems;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button buttonCalTax;
        private System.Windows.Forms.Label label17Percent;
        private System.Windows.Forms.Label label5Percent;
        private System.Windows.Forms.CheckBox checkBoxTaxDetails;
        private System.Windows.Forms.Label labelKK;
        private System.Windows.Forms.CheckBox checkBoxShowAllBills;
        private System.Windows.Forms.DataGridViewTextBoxColumn billIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn billDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.RichTextBox richTextBoxTaxCatDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn custom1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValExcTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn taxPaid;
        private System.Windows.Forms.DataGridViewTextBoxColumn actualTax;
    }
}
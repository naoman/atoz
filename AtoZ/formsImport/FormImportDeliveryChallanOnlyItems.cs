﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormImportDeliveryChallanOnlyItems : Form
    {
        public FormImportDeliveryChallanOnlyItems()
        {
            InitializeComponent();
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            List<int> items = new List<int>();

            string[] lines = this.richTextBox1.Text.Split('\n');

            foreach (string line in lines)
            {
                string[] cols = line.Split('\t');

                items.Add(int.Parse(cols[0]));
               
            }

            int counter = 0;
            foreach(AtoZDatabaseDataSet_Items.itemsRow itemRow in _parentFormPointer.getItems().items)
            {
                if(items.Contains(itemRow.catNo))
                {
                    itemRow.category = ((int)ItemCategory.DeliveryChallanOnly) + "";
                    counter++;
                }
            }

            MessageBox.Show(lines.Length + " records entered, " + counter + " records update");

            this.Close();
        }

        private FormInventoryManagement _parentFormPointer;

        public FormInventoryManagement ParentFormPointer
        {
            set { _parentFormPointer = value; }
        }
    }
}

﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormImportShopStock : Form
    {
        public FormImportShopStock()
        {
            InitializeComponent();
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            Console.Out.WriteLine("button pressed .......");
            string[] lines = this.richTextBox1.Text.Split('\n');
            int rowsImported = 0;
            List<AtoZDatabaseDataSet_ShopStatus.shopStatusRow> rows = new List<AtoZDatabaseDataSet_ShopStatus.shopStatusRow>();
            foreach (string line in lines)
            {
                string[] cols = line.Split('\t');
                if (cols.Length != 4)
                {
                    MessageBox.Show("Unexpected number of columns : >" + line + " <");
                    return;
                }
                else
                {
                    foreach (string col in cols)
                    {
                        Console.WriteLine(col);

                    }

                    AtoZDatabaseDataSet_ShopStatus.shopStatusRow newRow = shopStatus.shopStatus.NewshopStatusRow();

                    try
                    {
                        newRow.itemId = int.Parse(cols[0]);
                        newRow.shopId = shopId;
                        newRow.balance = double.Parse(cols[2]);
                        rows.Add(newRow);
                        rowsImported++;
                    }
                    catch (Exception er)
                    {
                        MessageBox.Show("Got Error:" + er.Message + " : >" + line + " <" );
                        return;
                    }
                }
            }
            rows.ForEach(x =>  shopStatus.shopStatus.Rows.Add(x));
            MessageBox.Show(rowsImported+ " records imported");

            this.Close();
        }

        public AtoZDatabaseDataSet_ShopStatus shopStatus;
        public int shopId;
    }

    
}

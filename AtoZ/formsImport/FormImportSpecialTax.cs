﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormImportSpecialTax : Form
    {
        public FormImportSpecialTax()
        {
            InitializeComponent();
        }

        public DataGridView dataGridView = null;
        public int colIndex = -1;

        private void buttonImport_Click(object sender, EventArgs e)
        {
            try
            {

                Dictionary<string,string> newTaxes = new Dictionary<string,string>();
                string[] lines = this.richTextBox1.Text.Split('\n');
                foreach (string line in lines)
                {
                    string[] cols = line.Split('\t');
                    newTaxes.Add(cols[0],cols[1]);
                }

                if(dataGridView == null || colIndex <0)
                {
                    MessageBox.Show("datagridview not provided or invalid column index");
                    return;
                }

                int itemsAdded = 0;
                foreach(DataGridViewRow row in dataGridView.Rows)
                {
                    if (row.Cells[0] == null || row.Cells[0].Value == null)
                        continue;

                    if(newTaxes.ContainsKey(row.Cells[0].Value.ToString()))
                    {
                        row.Cells[colIndex].Value = newTaxes[row.Cells[0].Value.ToString()];
                        row.Cells[colIndex].Style.BackColor = Color.Red;
                        itemsAdded++;
                    }
                }

                MessageBox.Show("List cout: " + newTaxes.Count + " Items Added:" + itemsAdded);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormImportData : Form
    {
        public FormImportData()
        {
            InitializeComponent();
        }

        private FormInventoryManagement _parentFormPointer;

        public FormInventoryManagement ParentFormPointer
        {
            set { _parentFormPointer = value; }
        }
        private void import_Click(object sender, EventArgs e)
        {
            Console.Out.WriteLine("button pressed .......");
            string[] lines = this.richTextBox1.Text.Split('\n');
            foreach (string line in lines)
            {
                string[] cols = line.Split('\t');
                

                AtoZDatabaseDataSet_Items.itemsRow newItemsRow = _parentFormPointer.getItems().items.NewitemsRow();

                newItemsRow.catNo = int.Parse(cols[0]);
                newItemsRow.name = cols[1];
                newItemsRow.isActive = 1;
                newItemsRow.price = float.Parse(cols[2]);
                newItemsRow.packingSize = 1;
                newItemsRow.stockSize = 0;
                newItemsRow.tax = float.Parse(cols[4]);
                _parentFormPointer.getItems().items.Rows.Add(newItemsRow);
            }

            MessageBox.Show(lines.Length + " records imported");

            this.Close();

        }


    }
}

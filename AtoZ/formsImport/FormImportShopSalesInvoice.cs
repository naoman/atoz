﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{

    public partial class FormImportShopSalesInvoice : Form
    {
        public FormImportShopSalesInvoice()
        {
            InitializeComponent();
            this.textBoxId.Text = ""+10;
            this.textBoxName.Text = "" + 17;
            this.textBoxQuantity.Text = "" + 62;
            this.textBoxItemBalance.Text = "" + 45;
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            Console.Out.WriteLine("button pressed .......");
            string[] lines = this.richTextBox1.Text.Split('\n');
            int rowsImported = 0;
            List<ListData> dataList = new List<ListData>();
            string currentLine = "";
            try
            {
                    
                int idIndex = int.Parse(this.textBoxId.Text);
                int nameIndex = int.Parse(this.textBoxName.Text);
                int quantityIndex = int.Parse(this.textBoxQuantity.Text);
                int balanceIndex = int.Parse(this.textBoxItemBalance.Text);

                foreach (string line in lines)
                {
                    currentLine = line;
                    string[] cols = line.Split('\t');
                   /* if (cols.Length != 8)
                    {
                        MessageBox.Show("Unexpected number of columns : >" + line + " <");
                        return;
                    }
                    */ 
                    //else
                    {
                        int itemId = int.Parse(cols[idIndex]);
                        string itemName = cols[nameIndex];
                        double itemQuantity = double.Parse(cols[quantityIndex]);
                        double balance = double.Parse(cols[balanceIndex]);
                        dataList.Add(new ListData(itemId, itemName, itemQuantity,balance));
                        rowsImported++;
                    }
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Got Error:" + er.Message + " : >" + currentLine + " <" );
                return;
            }
             

            dataList.ForEach(x => itemsGridView.Rows.Add(x.itemId, x.itemName, x.itemQuantity,x.itemBalance));
            
            MessageBox.Show(rowsImported+ " records imported");

            this.Close();
        }

        public System.Windows.Forms.DataGridView itemsGridView;
    }

    class ListData
    {
        public int itemId;
        public string itemName;
        public double itemQuantity;
        public double itemBalance;
        public ListData(int ii, string iN, double iq, double ib)
        {
            itemId = ii;
            itemName = iN;
            itemQuantity = iq;
            itemBalance = ib;
        }
    }
}

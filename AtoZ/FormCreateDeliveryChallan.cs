﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormCreateDeliveryChallan : Form
    {
        private const int INDEX_NEW_DEMAND = 0; // modify .Rows.Add manually
        private const int INDEX_ITEM_ID = 1;
        private const int INDEX_ITEM_NAME = 2;
        private const int INDEX_CURRENT_STOCK = 3;
        private const int INDEX_LAST_SALE = 4;
        private const int INDEX_LAST_DELIVERY = 5;

        public Boolean postTaxShops = false;

        public FormCreateDeliveryChallan()
        {
            InitializeComponent();
        }

        private void FormCreateDeliveryChallan_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet12.DeliveryChallan' table. You can move, or remove it, as needed.
            //disabled this.deliveryChallanTableAdapter.Fill(this.atoZDatabaseDataSet12.DeliveryChallan);
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet11.ShopSalesInvoice' table. You can move, or remove it, as needed.
           //disabled this.shopSalesInvoiceTableAdapter.Fill(this.atoZDatabaseDataSet11.ShopSalesInvoice);
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet10.shops' table. You can move, or remove it, as needed.
            // replaced by code below this.shopsTableAdapter.Fill(this.atoZDatabaseDataSet10.shops);

            if(postTaxShops)
            {
                this.shopsTableAdapter.GetPostTaxShops(this.atoZDatabaseDataSet10.shops);
            }
            else
            {
                this.shopsTableAdapter.GetRegularShops(this.atoZDatabaseDataSet10.shops);
            }

            int? currentMaxChallanId = 0;
            /*
            foreach(DataRow row in atoZDatabaseDataSet12.DeliveryChallan.Rows)
            {
                int rowChallanId = int.Parse(row["challanId"].ToString()) ;
                if(rowChallanId > currentMaxChallanId)
                {
                    currentMaxChallanId = rowChallanId;
                }

            }*/

            AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter tableAdapter =
                new  AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();
            currentMaxChallanId = tableAdapter.GetMaxChallanId();

            currentMaxChallanId++;

            this.textBoxNewChallanId.Text = (currentMaxChallanId).ToString();

            //calling cell clicked to select first row on the startup of this form
            //dataGridView1_CellClick(null, null);
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.shopSalesInvoiceTableAdapter.FillBy(this.atoZDatabaseDataSet11.ShopSalesInvoice, ((int)(System.Convert.ChangeType(shopIdToolStripTextBox.Text, typeof(int)))));
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                bool calledForTheFirstTime = false;
                if (this.shopId == -1)
                {
                    calledForTheFirstTime = true;
                }

                shopId = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                
                if(calledForTheFirstTime)
                {
                    addItemsWithZeroCount(shopId);
                }
                this.labelShopId.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();

                /*disabled
                 
                this.shopSalesInvoiceTableAdapter.FillBy(this.atoZDatabaseDataSet11.ShopSalesInvoice, 
                    shopId);
                this.deliveryChallanTableAdapter.FillBy(this.atoZDatabaseDataSet12.DeliveryChallan, 
                    shopId);
                disable */

                // reset selections for deliveryChallan and Sale
                this.challanId = -1;
                this.salesInvoiceId = "";
                this.labelChallanID.Text = "";
                this.labelInvoiceID.Text = "";

                /* last data info logic disables
            
                this.updateTable();
                */

                this.dataGridView4.Enabled = true;

            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void fillByToolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                this.deliveryChallanTableAdapter.FillBy(this.atoZDatabaseDataSet12.DeliveryChallan, new System.Nullable<int>(((int)(System.Convert.ChangeType(shopIdToolStripTextBox1.Text, typeof(int))))));
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        int shopId = -1;
        String salesInvoiceId = "";
        int challanId = -1;

        private void updateTable()
        {
            // reset table
            this.dataGridView4.Rows.Clear();

            Dictionary<int, CreateDeliveryChallanDataRow> dict = new Dictionary<int, CreateDeliveryChallanDataRow>();

            //add shop stock
            AddShopStock(dict);

            //add last sale
            AddLastSale(dict);

            //add last delivery
            AddLastDelivery(dict);

            foreach(var kv in dict)
            {
                this.dataGridView4.Rows.Add("",kv.Key, kv.Value.itemName, kv.Value.currentStock, kv.Value.lastSale, kv.Value.lastDemand);
            }
            this.dataGridView4.PerformLayout();
            HelperMethods.setRowNumbers(dataGridView4);
        }

        private void AddShopStock(Dictionary<int, CreateDeliveryChallanDataRow> dict)
        {
            AtoZDatabaseDataSet_ShopStatus dataSet_ShopStatus = new AtoZDatabaseDataSet_ShopStatus();
            AtoZ.datasets.AtoZDatabaseDataSet_ShopStatusTableAdapters.shopStatusTableAdapter tableAdapter_ShopStatus =
                new AtoZ.datasets.AtoZDatabaseDataSet_ShopStatusTableAdapters.shopStatusTableAdapter();


            int count = tableAdapter_ShopStatus.FillByShopId(dataSet_ShopStatus.shopStatus, shopId);

            foreach (var row in dataSet_ShopStatus.shopStatus)
            {
                CreateDeliveryChallanDataRow newRow = new CreateDeliveryChallanDataRow();
                newRow.itemName = ItemDetailsHelper.getItemName(row.itemId);
                newRow.currentStock = row.balance;
                dict.Add(row.itemId, newRow);
            }
        }

        private void AddLastSale(Dictionary<int, CreateDeliveryChallanDataRow> dict)
        {
            /*
            AtoZDatabaseDataSet_ShopSalesInvoice dataSet = new AtoZDatabaseDataSet_ShopSalesInvoice();
            AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapter =
                new AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();

            int count = tableAdapter.FillBy(dataSet.ShopSalesInvoice, this.salesInvoiceId);

            foreach (var row in dataSet.ShopSalesInvoice)
            {
                CreateDeliveryChallanDataRow newRow = new CreateDeliveryChallanDataRow();

                //check if the row already exists
                if(dict.ContainsKey(row.itemCatNo))
                {
                    newRow = dict[row.itemCatNo];
                }
                else
                {
                    newRow.itemName = ItemDetailsHelper.getItemName(row.itemCatNo);
                    dict.Add(row.itemCatNo, newRow);
                }

                newRow.lastSale = row.itemQuantity;
                
            }
             */ 
        }

        private void AddLastDelivery(Dictionary<int, CreateDeliveryChallanDataRow> dict)
        {
            AtoZDatabaseDataSet_DeliveryChallan dataSet = new AtoZDatabaseDataSet_DeliveryChallan();
            AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter tableAdapter =
                new AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();

            int count = tableAdapter.FillBy(dataSet.DeliveryChallan,this.challanId);

            foreach (var row in dataSet.DeliveryChallan)
            {
                CreateDeliveryChallanDataRow newRow = new CreateDeliveryChallanDataRow();

                //check if the row already exists
                if (dict.ContainsKey(row.itemCatNo))
                {
                    newRow = dict[row.itemCatNo];
                }
                else
                {
                    newRow.itemName = ItemDetailsHelper.getItemName(row.itemCatNo);
                    dict.Add(row.itemCatNo, newRow);
                }

                newRow.lastDemand = row.itemQuantity;

            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            /* last data info logic disables
              
            this.salesInvoiceId = dataGridView2.CurrentRow.Cells[0].Value.ToString();
            //this.labelInvoiceID.Text = salesInvoiceId.ToString();
            this.labelLastInvoice2.Text = "Last Invoice ID: " + salesInvoiceId.ToString();
            this.updateTable();
             */ 
        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            /* last data info logic disables
            
            this.challanId = int.Parse(dataGridView3.CurrentRow.Cells[0].Value.ToString());
            //this.labelChallanID.Text = challanId.ToString();
            this.labelLastDeliveryChallanId2.Text = "Last Delivery Challan ID:" + challanId.ToString();
            this.updateTable();
             */ 
        }



        private void buttonCreateChallan_Click(object sender, EventArgs e)
        {
            try
            {
                AtoZDatabaseDataSet_DeliveryChallan deliveryChallanDataset = new AtoZDatabaseDataSet_DeliveryChallan();
                AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter deliveryChallanTableAdapter =
                    new AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();

                int count = 0;
                    
                foreach (DataGridViewRow row in this.dataGridView4.Rows)
                {
                    double quantity;
                    if (row.Cells[INDEX_NEW_DEMAND].Value != null 
                        && !string.IsNullOrEmpty(row.Cells[INDEX_NEW_DEMAND].Value.ToString())
                        && double.TryParse(row.Cells[INDEX_NEW_DEMAND].Value.ToString(), out quantity))
                    {
                        if (quantity != 0)
                        {
                            AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow newRow =
                               deliveryChallanDataset.DeliveryChallan.NewDeliveryChallanRow();

                            newRow.challanId = int.Parse(this.textBoxNewChallanId.Text);
                            newRow.challanDate = this.dateTimePicker1.Value;
                            newRow.shopId = this.shopId;
                            newRow.itemCatNo = int.Parse(row.Cells[INDEX_ITEM_ID].Value.ToString());
                            newRow.itemName = row.Cells[INDEX_ITEM_NAME].Value.ToString();
                            newRow.itemQuantity = double.Parse(row.Cells[INDEX_NEW_DEMAND].Value.ToString());

                            if (!string.IsNullOrWhiteSpace(this.textBoxNewOrderId.Text))
                            {
                                newRow.orderNo = this.textBoxNewOrderId.Text;
                            }
                            deliveryChallanDataset.DeliveryChallan.AddDeliveryChallanRow(newRow);
                            count++;
                        }
                    }


                }

                var response = MessageBox.Show("Do you want to create challan with " + count + " item(s) \n For shop "+ 
                    this.labelShopId.Text,"", MessageBoxButtons.YesNo);
                if(response.Equals(DialogResult.No))
                {
                    return;
                }

                int recordsAdded = deliveryChallanTableAdapter.Update(deliveryChallanDataset);
                Console.WriteLine("Rows added " + recordsAdded);
                Console.WriteLine("DataSet Rows " + deliveryChallanDataset.DeliveryChallan.Rows.Count);
                deliveryChallanDataset.DeliveryChallan.AcceptChanges();

                ClassUpdateInventory updateInventory = new ClassUpdateInventory();
                updateInventory.removeInventory(deliveryChallanDataset);

                this.Close();

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void buttonLoadAllItems_Click(object sender, EventArgs e)
        {

            if(this.shopId == -1)
            {
                MessageBox.Show("Please select a shop first");
                return;
            }

            MessageBox.Show("Adding items for shop " + ShopsDetailHelper.getShopName(this.shopId));

            addItemsWithZeroCount(this.shopId);
        }

        private void addItemsWithZeroCount(int shopId)
        {
            this.dataGridView4.Rows.Clear();
          
            /*
            AtoZDatabaseDataSet_Items dataset = new AtoZDatabaseDataSet_Items();
            AtoZDatabaseDataSet_ItemsTableAdapters.itemsTableAdapter tableAdapter =
                new AtoZDatabaseDataSet_ItemsTableAdapters.itemsTableAdapter();
            tableAdapter.Fill(dataset.items);

            foreach (AtoZDatabaseDataSet_Items.itemsRow itemrRow in dataset.items)
            {
             */

            Dictionary<int, ItemDetailsData> allItems;

            if (ShopsDetailHelper.isPostTaxShop(shopId))
            {
                allItems = ItemDetailsHelper.getAllPostTaxItems();
            }
            else
            {
                allItems = ItemDetailsHelper.getAllItems();
            }

            foreach (var kvp in allItems)
            {
                if (kvp.Value.category == ItemCategory.DeliveryChallanOnly)
                {
                    this.dataGridView4.Rows.Add("0", kvp.Key, kvp.Value.name, "", "", "");
                    
                }
            }

            this.dataGridView4.PerformLayout();

            HelperMethods.setRowNumbers(dataGridView4);
            
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            dataGridView1_CellClick(null, null);
        }

        private void dataGridView1_KeyDown(object sender, KeyPressEventArgs e)
        {
            dataGridView1_CellClick(null, null);
        }

        private void dataGridView1_KeyUp(object sender, KeyEventArgs e)
        {
            dataGridView1_CellClick(null, null);
        }

        private void dataGridView2_KeyDown(object sender, KeyEventArgs e)
        {
            dataGridView2_CellClick(null, null);
        }

        private void dataGridView2_KeyPress(object sender, KeyPressEventArgs e)
        {
            dataGridView2_CellClick(null, null);
        }

        private void dataGridView2_KeyUp(object sender, KeyEventArgs e)
        {
            dataGridView2_CellClick(null, null);
        }

        private void dataGridView3_KeyDown(object sender, KeyEventArgs e)
        {
            dataGridView3_CellClick(null, null);
        }

        private void dataGridView3_KeyPress(object sender, KeyPressEventArgs e)
        {
            dataGridView3_CellClick(null, null);
        }

        private void dataGridView3_KeyUp(object sender, KeyEventArgs e)
        {
            dataGridView3_CellClick(null, null);
        }

        private void dataGridView4_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && (e.ColumnIndex == INDEX_NEW_DEMAND))
            {
                if ((dataGridView4.Rows[e.RowIndex].Cells[e.ColumnIndex] == null) ||
                    (dataGridView4.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null) ||
                    (string.IsNullOrWhiteSpace(dataGridView4.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()))
                    )
                {
                    MessageBox.Show("cell value is empty");
                }
            }
        }

       
    }


    class CreateDeliveryChallanDataRow
    {
        public string itemName;
        public double currentStock;
        public double lastSale;
        public double lastDemand;
    }
}

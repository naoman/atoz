﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtoZ
{
    class LoadTest
    {

        private const int TEST_ID_BASE = 7000000;
        void addItems(int startId, int endId)
        {

        }

        void deleteItems(int startId, int endId)
        {

        }

        public void addDeliveryChallan(int startId, int endId)
        {
            AtoZDatabaseDataSet_DeliveryChallan deliveryChallanDataset = new AtoZDatabaseDataSet_DeliveryChallan();
            AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter deliveryChallanTableAdapter =
                new AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();


            deliveryChallanTableAdapter.Fill(deliveryChallanDataset.DeliveryChallan);

            for (int i = startId; i <= endId; i ++ )
            {
                foreach(KeyValuePair<int,ItemDetailsData> item in ItemDetailsHelper.getAllItems())
                {
                    AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow newRow =
                    deliveryChallanDataset.DeliveryChallan.NewDeliveryChallanRow();

                    newRow.challanId = i + TEST_ID_BASE;
                    newRow.challanDate = DateTime.Now;
                    newRow.shopId = TEST_ID_BASE;


                    newRow.itemCatNo = item.Key;
                    newRow.itemName = item.Value.name;
                    newRow.itemQuantity = 100;

                    newRow.orderNo = "LONG ORDER NUMBER    123456";
                
                    deliveryChallanDataset.DeliveryChallan.AddDeliveryChallanRow(newRow);
                 }

                int recordsAdded = deliveryChallanTableAdapter.Update(deliveryChallanDataset);
                Console.WriteLine("Challan:" + (i+TEST_ID_BASE)+" Rows added " + recordsAdded);
                Console.WriteLine("Challan:" + (i + TEST_ID_BASE) + " DataSet Rows " + deliveryChallanDataset.DeliveryChallan.Rows.Count);

            }

            
            deliveryChallanDataset.DeliveryChallan.AcceptChanges();
        }

        public void deleteDeliveryChallan(int startId, int endId)
        {
            try
            {
                for (int i = startId; i <= endId; i++)
                {
                    AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter tableAdapter =
                            new AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();
                    int rows = tableAdapter.DeleteChallanById(i + TEST_ID_BASE);

                    Console.WriteLine("Challan " +(i + TEST_ID_BASE)+ "   " +rows + " Row deleted");

                }
            }
            catch (Exception ex)
            {
               Console.WriteLine(ex.Message);
            }
        }

        public void addSalesInvoice(int startId, int endId)
        {
            AtoZDatabaseDataSet_ShopSalesInvoice dataSet = new AtoZDatabaseDataSet_ShopSalesInvoice();
                AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapter =
                    new AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();


                tableAdapter.Fill(dataSet.ShopSalesInvoice);

            for (int i = startId; i <= endId; i ++ )
            {
                try
                {
                    foreach(KeyValuePair<int,ItemDetailsData> item in ItemDetailsHelper.getAllItems())
                    {
                    
                       
                        AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow newRow =
                                dataSet.ShopSalesInvoice.NewShopSalesInvoiceRow();
                        newRow.invoiceDate = DateTime.Now;
                        newRow.invoiceId = "LONG INVOICE ID 341328764876387946435";
                        newRow.shopId = TEST_ID_BASE;
                        newRow.shopName = "VERY LONG SHOP NAME 43829749823789472394799324792389";
                        newRow.itemCatNo = item.Key;
                        newRow.itemName = item.Value.name;
                        newRow.itemQuantity = 200;
                        newRow.status = RecordStatus.Unmerged.ToString();
                        newRow.itemPrice = item.Value.price;
                        newRow.itemTax = item.Value.tax;
                            
                        newRow.billId = TEST_ID_BASE + i;
                        newRow.billDate = DateTime.Now;
                        
                        newRow.salesTaxRegNo = "SOME LONG NUMBER 342423423423";
                        
                        dataSet.ShopSalesInvoice.AddShopSalesInvoiceRow(newRow);
                    }

                    int count = tableAdapter.Update(dataSet);
                    Console.WriteLine("SALES INVOICE: " + (TEST_ID_BASE + i)+" Rows added " + count);
                    Console.WriteLine("SALES INVOICE: " + (TEST_ID_BASE + i)+"DataSet Rows " + dataSet.ShopSalesInvoice.Rows.Count);
                    dataSet.ShopSalesInvoice.AcceptChanges();

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Got Exception for row number: " +  ex.Message);
                    return;
                }
           }
        }

        public void deleteSalesInvoice(int startId, int endId)
        {
            try
            {
                for (int i = startId; i <= endId; i++)
                {

                    AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter tableAdapter =
                            new AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();
                    int rows = tableAdapter.DeleteChallanById(i + TEST_ID_BASE);

                    Console.WriteLine("Sales Invoice  " + (i + TEST_ID_BASE) + "   " + rows + " Row deleted");

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}

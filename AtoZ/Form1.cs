﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System.Data.SqlClient;
using System.IO;

namespace AtoZ
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //ItemDetailsHelper.refreshData();
            //ShopsDetailHelper.refreshData();
        }

        FormInventoryManagement formInventoryManagement;
        private void inventoryManagement_Click(object sender, EventArgs e)
        {
            formInventoryManagement = new FormInventoryManagement();

            formInventoryManagement.ParentFormPointer = this;
            formInventoryManagement.Show();
        }

        private void deliveryChallanManagement_Click(object sender, EventArgs e)
        {

        }

        private void buttonPrintReport_Click(object sender, EventArgs e)
        {

        }

        private void buttonShopManagement_Click(object sender, EventArgs e)
        {
            FormShopsManagement formShopManagement = new FormShopsManagement();
            formShopManagement.Show();
        }

        private void buttonUpdateShopStock_Click(object sender, EventArgs e)
        {
            FormUpdateShopStock formUpdateShopStock = new FormUpdateShopStock();
            formUpdateShopStock.Show();
        }

        private void buttonEnterShopSalesReceipt_Click(object sender, EventArgs e)
        {
            FormEnterShopSalesInvoice form = new FormEnterShopSalesInvoice();
            form.Show();
        }

        private void buttonFormShopSalesInvoiceDetails_Click(object sender, EventArgs e)
        {
            FormShopSalesInvoiceDetails form = new FormShopSalesInvoiceDetails();
            form.Show();
        }

        private void buttonCreateDeliveryChallan_Click(object sender, EventArgs e)
        {
            FormCreateDeliveryChallan form = new FormCreateDeliveryChallan();
            form.Show();
        }

        private void buttonEditAndPrintDeliveryChallan_Click(object sender, EventArgs e)
        {
            FormEditAndPrintDeliveryChallan form = new FormEditAndPrintDeliveryChallan();
            form.Show();
        }

        private void buttonDebugShopSalesInvoice_Click(object sender, EventArgs e)
        {
            FormDebugShopSalesInvoice form = new FormDebugShopSalesInvoice();
            form.Show();
        }

        private void buttonDebugDeliveryChallan_Click(object sender, EventArgs e)
        {
            FormDebugDeliveryChallan form = new FormDebugDeliveryChallan();
            form.Show();
        }

        private void buttonBackupDatabase_Click(object sender, EventArgs e)
        {

            try
            {
                FolderBrowserDialog dlg = new FolderBrowserDialog();
                dlg.Description = "Select a folder";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    String fileName = dlg.SelectedPath + @"\BU-" + DateTime.Now.ToShortDateString().Replace('/', '-') +
                        "--" + DateTime.Now.ToLongTimeString().Replace(':', '-');
                    BackupDb(fileName);
                    MessageBox.Show("Backup created. File Name: " + fileName);
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private static void BackupDb(string backupFileName)
        {
            try
            {
                ServerConnection con = new ServerConnection(new SqlConnection(global::AtoZ.Properties.Settings.Default.AtoZDatabaseConnectionString));
                Server server = new Server(con);
                Backup source = new Backup();
                source.Action = BackupActionType.Database;
                source.Database = @"C:\ATOZDATABASE\ATOZDATABASE.MDF";
                BackupDeviceItem destination = new BackupDeviceItem(backupFileName, DeviceType.File);
                source.Devices.Add(destination);
                source.SqlBackup(server);
                
                con.Disconnect();
            }
            catch (Exception ex)
            {
                while (ex != null)
                {
                    MessageBox.Show(ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void buttonRestoreDatabase_Click(object sender, EventArgs e)
        {

            try
            {
                
                
                OpenFileDialog fileDialog = new OpenFileDialog();
                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    // backup first
                    String backupFileName = Path.GetDirectoryName(fileDialog.FileName)+@"\backup_before_restore-" + DateTime.Now.ToShortDateString().Replace('/', '-') +
                        "--" + DateTime.Now.ToLongTimeString().Replace(':', '-');
                    BackupDb(backupFileName);
                    MessageBox.Show("Backup of current data created before restoring. File Name: " + backupFileName);
                    
                    // backup done

                    
                    SqlConnection sqlConnection = new SqlConnection(global::AtoZ.Properties.Settings.Default.AtoZDatabaseConnectionString);
                    sqlConnection.Open();

                    string UseMaster = "USE master";
                    SqlCommand UseMasterCommand = new SqlCommand(UseMaster,sqlConnection);
                    UseMasterCommand.ExecuteNonQuery();
  
                    ServerConnection con = new ServerConnection(sqlConnection);
                   
                    Server server = new Server(con);

                    
                    

                    Restore destination = new Restore();
                    destination.Action = RestoreActionType.Database;
                    destination.Database = @"C:\ATOZDATABASE\ATOZDATABASE.MDF";
                    BackupDeviceItem source = new BackupDeviceItem(fileDialog.FileName, DeviceType.File);
                    destination.Devices.Add(source);
                    destination.ReplaceDatabase = true;
                    
                    destination.SqlRestore(server);
                    con.Disconnect();

                    MessageBox.Show("Database successfully restored from file:" + fileDialog.FileName);
                }

            }
            catch (Exception ex)
            {
                while (ex != null)
                {
                    MessageBox.Show(ex.Message);
                    ex = ex.InnerException;
                }
            }
        }

        private void buttonTaxSummary_Click(object sender, EventArgs e)
        {
            FormTaxSummary form = new FormTaxSummary();
            form.Show();
        }

        private void buttonCreateDeliveryChallanKohinoor_Click(object sender, EventArgs e)
        {
            FormCreateDeliveryChallan form = new FormCreateDeliveryChallan();
            form.postTaxShops = true;
            form.Show();
        }

        private void buttonEnterShopSalesInvoiceKohinoor_Click(object sender, EventArgs e)
        {
            FormEnterShopSalesInvoice form = new FormEnterShopSalesInvoice();
            form.postTaxShops = true;
            form.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonDebugShipmentReceived_Click(object sender, EventArgs e)
        {
            FormDebugShipmentReceived form = new FormDebugShipmentReceived();
            form.Show();
        }

        private void buttonCreateShipmentReceived_Click(object sender, EventArgs e)
        {
            FormEnterShipmentReceived form = new FormEnterShipmentReceived(ShipmentReceivedFormMode.ENTER_NEW);
            form.Show();

        }

        private void buttonEditShipmentReceived_Click(object sender, EventArgs e)
        {
            FormEnterShipmentReceived form = new FormEnterShipmentReceived(ShipmentReceivedFormMode.EDIT);
            form.Show();
        }

        private void buttonDebugShopStatus_Click(object sender, EventArgs e)
        {
            FormDebugShopStatus form = new FormDebugShopStatus();
            form.Show();
        }

        private void buttonComputeDemand_Click(object sender, EventArgs e)
        {
            FormComputeDemand form = new FormComputeDemand();
            form.Show();
        }

        private void buttonEvaluateLocalStock_Click(object sender, EventArgs e)
        {
            FormEvaluateLoalStock form = new FormEvaluateLoalStock();
            form.Show();
        }
    }
}

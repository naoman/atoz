﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public enum ItemCategory
    {
       DeliveryChallanOnly = 1,
        None
    }

    public partial class FormInventoryManagement : Form
    {
        public FormInventoryManagement()
        {
            InitializeComponent();
        }

        private void itemsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.itemsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.atoZDatabaseDataSet_Items);
            ItemDetailsHelper.refreshData();

        }

        private void FormInventoryManagement_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet_Items.items' table. You can move, or remove it, as needed.
            this.itemsTableAdapter.Fill(this.atoZDatabaseDataSet_Items.items);

        }

        private Form1 _parentFormPointer;
        public Form1 ParentFormPointer
        {
            set { _parentFormPointer = value; }
        }

        public AtoZDatabaseDataSet_Items getItems(){
            return this.atoZDatabaseDataSet_Items;
        }

        FormImportData formImportData;
        private void importData_Click(object sender, EventArgs e)
        {
            formImportData = new FormImportData();
            formImportData.ParentFormPointer = this;
            formImportData.Show();
        }

        private void exportData_Click(object sender, EventArgs e)
        {

        }

        private void buttonGetDeliveryChallanOnlyItems_Click(object sender, EventArgs e)
        {
            FormImportDeliveryChallanOnlyItems form = new FormImportDeliveryChallanOnlyItems();
            form.ParentFormPointer = this;
            form.Show();
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {

            if (this.textBoxSearch.Text.Length < 2)
            {
                foreach (DataGridViewRow row in this.itemsDataGridView.Rows)
                {
                    row.Visible = true;
                }
            }
            else
            {
                foreach (DataGridViewRow row in this.itemsDataGridView.Rows)
                {
                    if (row.Cells[1] == null || row.Cells[1].Value == null || String.IsNullOrWhiteSpace(row.Cells[1].Value.ToString()))
                        continue;

                    if (row.Cells[1].Value.ToString().ToLower().Contains(this.textBoxSearch.Text.ToLower()))
                    {
                        row.Visible = true;
                    }
                    else
                    {
                        try
                        {
                            row.Visible = false;
                        }
                        catch (Exception ex)
                        {
                            Console.Out.WriteLine(row.Index + "     " + ex.Message);
                        }
                    }
                }
            }
        }

        private void buttonImportSpecialTax_Click(object sender, EventArgs e)
        {
            FormImportSpecialTax form = new FormImportSpecialTax();
            form.dataGridView = this.itemsDataGridView;
            form.colIndex = 13;
            form.Show();
        }

        private void buttonFixItemPrice_Click(object sender, EventArgs e)
        {
            foreach(AtoZDatabaseDataSet_Items.itemsRow row in this.atoZDatabaseDataSet_Items.items.Rows)
            {
                row.price = Math.Round(row.price, 2);
            }
        }

        private void itemsDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.itemsDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Red;
            
        }
    }
}

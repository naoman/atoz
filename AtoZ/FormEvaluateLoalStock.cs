﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormEvaluateLoalStock : Form
    {
        public FormEvaluateLoalStock()
        {
            InitializeComponent();
            this.textBoxExpectedNoOfMonths.Text = "0";
            this.textBoxNoOfMonths.Text = "0";
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void buttonCompute_Click(object sender, EventArgs e)
        {
            int noOfSaleMonths;
            int expectedMonthsForInventory;
            try
            {
                noOfSaleMonths = int.Parse(this.textBoxNoOfMonths.Text);
                expectedMonthsForInventory = int.Parse(this.textBoxExpectedNoOfMonths.Text);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Please enter valid numbers");
                return;
            }
            Dictionary<int, int> itemIdSaleDataHashSet = new Dictionary<int, int>();

            AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapterBills = new
                datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();

            AtoZDatabaseDataSet_ShopSalesInvoice datasetBills = new AtoZDatabaseDataSet_ShopSalesInvoice();
            tableAdapterBills.FillByDateRange(datasetBills.ShopSalesInvoice,
                this.dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                this.dateTimePicker2.Value.ToString("yyyy-MM-dd HH:mm:ss"));

            foreach (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow row in datasetBills.ShopSalesInvoice)
            {
                if (!itemIdSaleDataHashSet.ContainsKey(row.itemCatNo))
                {
                    itemIdSaleDataHashSet.Add(row.itemCatNo,0);
                }
                itemIdSaleDataHashSet[row.itemCatNo] += (int)row.itemQuantity;
            }

            // update ItemDetailsHelper so that it picks the actual count for items
            ItemDetailsHelper.refreshData();
            var itemsDetails = ItemDetailsHelper.getAllItems();
            foreach(var kvp in itemsDetails)
            {
                int totalSale = 0;
                if(itemIdSaleDataHashSet.ContainsKey(kvp.Key))
                {
                    totalSale = itemIdSaleDataHashSet[kvp.Key]/noOfSaleMonths;
                }

                double averageSale = (double) totalSale/ (double) noOfSaleMonths;
                double stockMonths = (double) kvp.Value.stockSize / (double)averageSale;
                this.dataGridView1.Rows.Add(kvp.Key, kvp.Value.name, kvp.Value.stockSize,averageSale, totalSale, stockMonths.ToString("0.00"));

                if(stockMonths < expectedMonthsForInventory)
                {
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count -1].Cells[0].Style.BackColor = Color.Red;
                }
            }


        }
    }
}

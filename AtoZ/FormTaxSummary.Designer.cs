﻿using AtoZ.datasets;
namespace AtoZ
{
    partial class FormTaxSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.billIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.billDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopSalesInvoiceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet14 = new AtoZ.datasets.AtoZDatabaseDataSet14();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonFilter = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.radioButtonNone = new System.Windows.Forms.RadioButton();
            this.radioButtonGeneral = new System.Windows.Forms.RadioButton();
            this.radioButtonHosiery = new System.Windows.Forms.RadioButton();
            this.radioButtonKalaKola = new System.Windows.Forms.RadioButton();
            this.radioButtonExtraTax = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewSelectOne = new System.Windows.Forms.DataGridView();
            this.buttonDeliveryReport = new System.Windows.Forms.Button();
            this.buttonMonthlyDeliveryReport = new System.Windows.Forms.Button();
            this.buttonMonthlyInAndOut = new System.Windows.Forms.Button();
            this.buttonTopShopsBySale = new System.Windows.Forms.Button();
            this.buttonTopItemsBySale = new System.Windows.Forms.Button();
            this.buttonComputeDemand = new System.Windows.Forms.Button();
            this.buttonVisualizeData = new System.Windows.Forms.Button();
            this.shopSalesInvoiceTableAdapter = new AtoZ.datasets.AtoZDatabaseDataSet14TableAdapters.ShopSalesInvoiceTableAdapter();
            this.buttonPrint2 = new System.Windows.Forms.Button();
            this.dataGridViewTaxCat = new System.Windows.Forms.DataGridView();
            this.TaxCat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.atoZDatabaseDataSet = new AtoZ.datasets.AtoZDatabaseDataSet();
            this.dataGridViewShops = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSetShop_IdName = new AtoZ.AtoZDatabaseDataSetShop_IdName();
            this.shopsTableAdapter = new AtoZ.AtoZDatabaseDataSetShop_IdNameTableAdapters.shopsTableAdapter();
            this.BillNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GwoGST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HwoGST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KKwoGST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KKQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExtraTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSelectOne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTaxCat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShops)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSetShop_IdName)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.billIdDataGridViewTextBoxColumn,
            this.billDateDataGridViewTextBoxColumn,
            this.shopIdDataGridViewTextBoxColumn,
            this.shopNameDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.shopSalesInvoiceBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(6, 57);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 80;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(289, 244);
            this.dataGridView1.TabIndex = 0;
            // 
            // billIdDataGridViewTextBoxColumn
            // 
            this.billIdDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.billIdDataGridViewTextBoxColumn.DataPropertyName = "billId";
            this.billIdDataGridViewTextBoxColumn.HeaderText = "billId";
            this.billIdDataGridViewTextBoxColumn.Name = "billIdDataGridViewTextBoxColumn";
            this.billIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.billIdDataGridViewTextBoxColumn.Width = 53;
            // 
            // billDateDataGridViewTextBoxColumn
            // 
            this.billDateDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.billDateDataGridViewTextBoxColumn.DataPropertyName = "billDate";
            this.billDateDataGridViewTextBoxColumn.HeaderText = "billDate";
            this.billDateDataGridViewTextBoxColumn.Name = "billDateDataGridViewTextBoxColumn";
            this.billDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.billDateDataGridViewTextBoxColumn.Width = 67;
            // 
            // shopIdDataGridViewTextBoxColumn
            // 
            this.shopIdDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.shopIdDataGridViewTextBoxColumn.DataPropertyName = "shopId";
            this.shopIdDataGridViewTextBoxColumn.HeaderText = "shopId";
            this.shopIdDataGridViewTextBoxColumn.Name = "shopIdDataGridViewTextBoxColumn";
            this.shopIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.shopIdDataGridViewTextBoxColumn.Width = 64;
            // 
            // shopNameDataGridViewTextBoxColumn
            // 
            this.shopNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.shopNameDataGridViewTextBoxColumn.DataPropertyName = "shopName";
            this.shopNameDataGridViewTextBoxColumn.HeaderText = "shopName";
            this.shopNameDataGridViewTextBoxColumn.Name = "shopNameDataGridViewTextBoxColumn";
            this.shopNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.shopNameDataGridViewTextBoxColumn.Width = 83;
            // 
            // shopSalesInvoiceBindingSource
            // 
            this.shopSalesInvoiceBindingSource.DataMember = "ShopSalesInvoice";
            this.shopSalesInvoiceBindingSource.DataSource = this.atoZDatabaseDataSet14;
            // 
            // atoZDatabaseDataSet14
            // 
            this.atoZDatabaseDataSet14.DataSetName = "AtoZDatabaseDataSet14";
            this.atoZDatabaseDataSet14.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(78, 15);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(207, 20);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(386, 15);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(207, 20);
            this.dateTimePicker2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "From Date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(331, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "To Date:";
            // 
            // buttonFilter
            // 
            this.buttonFilter.Location = new System.Drawing.Point(622, 19);
            this.buttonFilter.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonFilter.Name = "buttonFilter";
            this.buttonFilter.Size = new System.Drawing.Size(108, 28);
            this.buttonFilter.TabIndex = 5;
            this.buttonFilter.Text = "Filter";
            this.buttonFilter.UseVisualStyleBackColor = true;
            this.buttonFilter.Click += new System.EventHandler(this.buttonFilter_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BillNo,
            this.BillDate,
            this.GwoGST,
            this.GQ,
            this.HwoGST,
            this.HQ,
            this.KKwoGST,
            this.KKQ,
            this.ExtraTax,
            this.Column4});
            this.dataGridView2.Location = new System.Drawing.Point(314, 57);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersWidth = 80;
            this.dataGridView2.RowTemplate.Height = 33;
            this.dataGridView2.Size = new System.Drawing.Size(276, 603);
            this.dataGridView2.TabIndex = 6;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // buttonPrint
            // 
            this.buttonPrint.Location = new System.Drawing.Point(742, 19);
            this.buttonPrint.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(60, 28);
            this.buttonPrint.TabIndex = 7;
            this.buttonPrint.Text = "Print";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // radioButtonNone
            // 
            this.radioButtonNone.AutoSize = true;
            this.radioButtonNone.Location = new System.Drawing.Point(36, 346);
            this.radioButtonNone.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioButtonNone.Name = "radioButtonNone";
            this.radioButtonNone.Size = new System.Drawing.Size(49, 17);
            this.radioButtonNone.TabIndex = 8;
            this.radioButtonNone.TabStop = true;
            this.radioButtonNone.Text = "none";
            this.radioButtonNone.UseVisualStyleBackColor = true;
            this.radioButtonNone.CheckedChanged += new System.EventHandler(this.radioButtonNone_CheckedChanged);
            // 
            // radioButtonGeneral
            // 
            this.radioButtonGeneral.AutoSize = true;
            this.radioButtonGeneral.Location = new System.Drawing.Point(36, 371);
            this.radioButtonGeneral.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioButtonGeneral.Name = "radioButtonGeneral";
            this.radioButtonGeneral.Size = new System.Drawing.Size(62, 17);
            this.radioButtonGeneral.TabIndex = 9;
            this.radioButtonGeneral.TabStop = true;
            this.radioButtonGeneral.Text = "General";
            this.radioButtonGeneral.UseVisualStyleBackColor = true;
            this.radioButtonGeneral.CheckedChanged += new System.EventHandler(this.radioButtonGeneral_CheckedChanged);
            // 
            // radioButtonHosiery
            // 
            this.radioButtonHosiery.AutoSize = true;
            this.radioButtonHosiery.Location = new System.Drawing.Point(36, 397);
            this.radioButtonHosiery.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioButtonHosiery.Name = "radioButtonHosiery";
            this.radioButtonHosiery.Size = new System.Drawing.Size(60, 17);
            this.radioButtonHosiery.TabIndex = 10;
            this.radioButtonHosiery.TabStop = true;
            this.radioButtonHosiery.Text = "Hosiery";
            this.radioButtonHosiery.UseVisualStyleBackColor = true;
            this.radioButtonHosiery.CheckedChanged += new System.EventHandler(this.radioButtonHosiery_CheckedChanged);
            // 
            // radioButtonKalaKola
            // 
            this.radioButtonKalaKola.AutoSize = true;
            this.radioButtonKalaKola.Location = new System.Drawing.Point(36, 422);
            this.radioButtonKalaKola.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioButtonKalaKola.Name = "radioButtonKalaKola";
            this.radioButtonKalaKola.Size = new System.Drawing.Size(70, 17);
            this.radioButtonKalaKola.TabIndex = 11;
            this.radioButtonKalaKola.TabStop = true;
            this.radioButtonKalaKola.Text = "Kala Kola";
            this.radioButtonKalaKola.UseVisualStyleBackColor = true;
            this.radioButtonKalaKola.CheckedChanged += new System.EventHandler(this.radioButtonKalaKola_CheckedChanged);
            // 
            // radioButtonExtraTax
            // 
            this.radioButtonExtraTax.AutoSize = true;
            this.radioButtonExtraTax.Location = new System.Drawing.Point(36, 446);
            this.radioButtonExtraTax.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioButtonExtraTax.Name = "radioButtonExtraTax";
            this.radioButtonExtraTax.Size = new System.Drawing.Size(70, 17);
            this.radioButtonExtraTax.TabIndex = 12;
            this.radioButtonExtraTax.TabStop = true;
            this.radioButtonExtraTax.Text = "Extra Tax";
            this.radioButtonExtraTax.UseVisualStyleBackColor = true;
            this.radioButtonExtraTax.CheckedChanged += new System.EventHandler(this.radioButtonExtraTax_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(12, 315);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(112, 159);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Type";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // dataGridViewSelectOne
            // 
            this.dataGridViewSelectOne.AllowUserToAddRows = false;
            this.dataGridViewSelectOne.AllowUserToDeleteRows = false;
            this.dataGridViewSelectOne.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSelectOne.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Quantity,
            this.Column5});
            this.dataGridViewSelectOne.Location = new System.Drawing.Point(314, 110);
            this.dataGridViewSelectOne.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridViewSelectOne.Name = "dataGridViewSelectOne";
            this.dataGridViewSelectOne.ReadOnly = true;
            this.dataGridViewSelectOne.RowHeadersWidth = 80;
            this.dataGridViewSelectOne.RowTemplate.Height = 33;
            this.dataGridViewSelectOne.Size = new System.Drawing.Size(276, 556);
            this.dataGridViewSelectOne.TabIndex = 14;
            this.dataGridViewSelectOne.Visible = false;
            // 
            // buttonDeliveryReport
            // 
            this.buttonDeliveryReport.Location = new System.Drawing.Point(174, 322);
            this.buttonDeliveryReport.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonDeliveryReport.Name = "buttonDeliveryReport";
            this.buttonDeliveryReport.Size = new System.Drawing.Size(128, 28);
            this.buttonDeliveryReport.TabIndex = 15;
            this.buttonDeliveryReport.Text = "Daily Delivery Report";
            this.buttonDeliveryReport.UseVisualStyleBackColor = true;
            this.buttonDeliveryReport.Click += new System.EventHandler(this.buttonDeliveryReport_Click);
            // 
            // buttonMonthlyDeliveryReport
            // 
            this.buttonMonthlyDeliveryReport.Location = new System.Drawing.Point(174, 359);
            this.buttonMonthlyDeliveryReport.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonMonthlyDeliveryReport.Name = "buttonMonthlyDeliveryReport";
            this.buttonMonthlyDeliveryReport.Size = new System.Drawing.Size(128, 28);
            this.buttonMonthlyDeliveryReport.TabIndex = 16;
            this.buttonMonthlyDeliveryReport.Text = "Monthly Delivery Report";
            this.buttonMonthlyDeliveryReport.UseVisualStyleBackColor = true;
            this.buttonMonthlyDeliveryReport.Click += new System.EventHandler(this.buttonMonthlyDeliveryReport_Click);
            // 
            // buttonMonthlyInAndOut
            // 
            this.buttonMonthlyInAndOut.Location = new System.Drawing.Point(174, 397);
            this.buttonMonthlyInAndOut.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonMonthlyInAndOut.Name = "buttonMonthlyInAndOut";
            this.buttonMonthlyInAndOut.Size = new System.Drawing.Size(128, 40);
            this.buttonMonthlyInAndOut.TabIndex = 17;
            this.buttonMonthlyInAndOut.Text = "Monthly Cash In and Out";
            this.buttonMonthlyInAndOut.UseVisualStyleBackColor = true;
            this.buttonMonthlyInAndOut.Click += new System.EventHandler(this.buttonMonthlyInAndOut_Click);
            // 
            // buttonTopShopsBySale
            // 
            this.buttonTopShopsBySale.Location = new System.Drawing.Point(174, 446);
            this.buttonTopShopsBySale.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonTopShopsBySale.Name = "buttonTopShopsBySale";
            this.buttonTopShopsBySale.Size = new System.Drawing.Size(128, 28);
            this.buttonTopShopsBySale.TabIndex = 18;
            this.buttonTopShopsBySale.Text = "Top Shops By Sale";
            this.buttonTopShopsBySale.UseVisualStyleBackColor = true;
            this.buttonTopShopsBySale.Click += new System.EventHandler(this.buttonTopShopsBySale_Click);
            // 
            // buttonTopItemsBySale
            // 
            this.buttonTopItemsBySale.Location = new System.Drawing.Point(174, 484);
            this.buttonTopItemsBySale.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonTopItemsBySale.Name = "buttonTopItemsBySale";
            this.buttonTopItemsBySale.Size = new System.Drawing.Size(128, 28);
            this.buttonTopItemsBySale.TabIndex = 19;
            this.buttonTopItemsBySale.Text = "Top Items By Sale";
            this.buttonTopItemsBySale.UseVisualStyleBackColor = true;
            this.buttonTopItemsBySale.Click += new System.EventHandler(this.buttonTopItemsBySale_Click);
            // 
            // buttonComputeDemand
            // 
            this.buttonComputeDemand.Location = new System.Drawing.Point(174, 560);
            this.buttonComputeDemand.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonComputeDemand.Name = "buttonComputeDemand";
            this.buttonComputeDemand.Size = new System.Drawing.Size(128, 28);
            this.buttonComputeDemand.TabIndex = 20;
            this.buttonComputeDemand.Text = "Compute Demand";
            this.buttonComputeDemand.UseVisualStyleBackColor = true;
            this.buttonComputeDemand.Click += new System.EventHandler(this.buttonComputeDemand_Click);
            // 
            // buttonVisualizeData
            // 
            this.buttonVisualizeData.Location = new System.Drawing.Point(174, 523);
            this.buttonVisualizeData.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonVisualizeData.Name = "buttonVisualizeData";
            this.buttonVisualizeData.Size = new System.Drawing.Size(128, 28);
            this.buttonVisualizeData.TabIndex = 21;
            this.buttonVisualizeData.Text = "Visualize Data";
            this.buttonVisualizeData.UseVisualStyleBackColor = true;
            this.buttonVisualizeData.Click += new System.EventHandler(this.buttonVisualizeData_Click);
            // 
            // shopSalesInvoiceTableAdapter
            // 
            this.shopSalesInvoiceTableAdapter.ClearBeforeFill = true;
            // 
            // buttonPrint2
            // 
            this.buttonPrint2.Location = new System.Drawing.Point(810, 18);
            this.buttonPrint2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonPrint2.Name = "buttonPrint2";
            this.buttonPrint2.Size = new System.Drawing.Size(60, 28);
            this.buttonPrint2.TabIndex = 22;
            this.buttonPrint2.Text = "Print2";
            this.buttonPrint2.UseVisualStyleBackColor = true;
            this.buttonPrint2.Click += new System.EventHandler(this.buttonPrint2_Click);
            // 
            // dataGridViewTaxCat
            // 
            this.dataGridViewTaxCat.AllowUserToAddRows = false;
            this.dataGridViewTaxCat.AllowUserToDeleteRows = false;
            this.dataGridViewTaxCat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTaxCat.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TaxCat});
            this.dataGridViewTaxCat.Location = new System.Drawing.Point(12, 477);
            this.dataGridViewTaxCat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridViewTaxCat.Name = "dataGridViewTaxCat";
            this.dataGridViewTaxCat.RowTemplate.Height = 33;
            this.dataGridViewTaxCat.Size = new System.Drawing.Size(160, 189);
            this.dataGridViewTaxCat.TabIndex = 23;
            this.dataGridViewTaxCat.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTaxCat_CellClick);
            this.dataGridViewTaxCat.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTaxCat_CellContentClick);
            // 
            // TaxCat
            // 
            this.TaxCat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.TaxCat.HeaderText = "Tax Cat";
            this.TaxCat.Name = "TaxCat";
            this.TaxCat.ReadOnly = true;
            this.TaxCat.Width = 69;
            // 
            // atoZDatabaseDataSet
            // 
            this.atoZDatabaseDataSet.DataSetName = "AtoZDatabaseDataSet";
            this.atoZDatabaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridViewShops
            // 
            this.dataGridViewShops.AutoGenerateColumns = false;
            this.dataGridViewShops.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewShops.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn});
            this.dataGridViewShops.DataSource = this.shopsBindingSource;
            this.dataGridViewShops.Location = new System.Drawing.Point(622, 57);
            this.dataGridViewShops.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridViewShops.Name = "dataGridViewShops";
            this.dataGridViewShops.RowTemplate.Height = 33;
            this.dataGridViewShops.Size = new System.Drawing.Size(254, 127);
            this.dataGridViewShops.TabIndex = 24;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // shopsBindingSource
            // 
            this.shopsBindingSource.DataMember = "shops";
            this.shopsBindingSource.DataSource = this.atoZDatabaseDataSetShop_IdName;
            // 
            // atoZDatabaseDataSetShop_IdName
            // 
            this.atoZDatabaseDataSetShop_IdName.DataSetName = "AtoZDatabaseDataSetShop_IdName";
            this.atoZDatabaseDataSetShop_IdName.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // shopsTableAdapter
            // 
            this.shopsTableAdapter.ClearBeforeFill = true;
            // 
            // BillNo
            // 
            this.BillNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BillNo.HeaderText = "Bill No";
            this.BillNo.Name = "BillNo";
            this.BillNo.ReadOnly = true;
            this.BillNo.Width = 62;
            // 
            // BillDate
            // 
            this.BillDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BillDate.HeaderText = "Bill Date";
            this.BillDate.Name = "BillDate";
            this.BillDate.ReadOnly = true;
            this.BillDate.Width = 71;
            // 
            // GwoGST
            // 
            this.GwoGST.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.GwoGST.HeaderText = "G w/o GST";
            this.GwoGST.Name = "GwoGST";
            this.GwoGST.ReadOnly = true;
            this.GwoGST.Width = 87;
            // 
            // GQ
            // 
            this.GQ.HeaderText = "G Quant";
            this.GQ.Name = "GQ";
            this.GQ.ReadOnly = true;
            // 
            // HwoGST
            // 
            this.HwoGST.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.HwoGST.HeaderText = "H w/o GST";
            this.HwoGST.Name = "HwoGST";
            this.HwoGST.ReadOnly = true;
            this.HwoGST.Width = 87;
            // 
            // HQ
            // 
            this.HQ.HeaderText = "H Quant";
            this.HQ.Name = "HQ";
            this.HQ.ReadOnly = true;
            // 
            // KKwoGST
            // 
            this.KKwoGST.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.KKwoGST.HeaderText = "KK w/o GST";
            this.KKwoGST.Name = "KKwoGST";
            this.KKwoGST.ReadOnly = true;
            this.KKwoGST.Width = 93;
            // 
            // KKQ
            // 
            this.KKQ.HeaderText = "KK Quant";
            this.KKQ.Name = "KKQ";
            this.KKQ.ReadOnly = true;
            // 
            // ExtraTax
            // 
            this.ExtraTax.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ExtraTax.HeaderText = "Extra Tax";
            this.ExtraTax.Name = "ExtraTax";
            this.ExtraTax.ReadOnly = true;
            this.ExtraTax.Width = 77;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.HeaderText = "Shop Name";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 88;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.HeaderText = "Bill No";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 62;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.HeaderText = "Bill Date";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 71;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.HeaderText = "Selected Type";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 101;
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column5.HeaderText = "Shop Name";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 88;
            // 
            // FormTaxSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 548);
            this.Controls.Add(this.dataGridViewShops);
            this.Controls.Add(this.dataGridViewTaxCat);
            this.Controls.Add(this.buttonPrint2);
            this.Controls.Add(this.buttonVisualizeData);
            this.Controls.Add(this.buttonComputeDemand);
            this.Controls.Add(this.buttonTopItemsBySale);
            this.Controls.Add(this.buttonTopShopsBySale);
            this.Controls.Add(this.buttonMonthlyInAndOut);
            this.Controls.Add(this.buttonMonthlyDeliveryReport);
            this.Controls.Add(this.buttonDeliveryReport);
            this.Controls.Add(this.dataGridViewSelectOne);
            this.Controls.Add(this.radioButtonExtraTax);
            this.Controls.Add(this.radioButtonKalaKola);
            this.Controls.Add(this.radioButtonHosiery);
            this.Controls.Add(this.radioButtonGeneral);
            this.Controls.Add(this.radioButtonNone);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.buttonFilter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormTaxSummary";
            this.Text = "FormTaxSummary";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormTaxSummary_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSelectOne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTaxCat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShops)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSetShop_IdName)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private AtoZDatabaseDataSet14 atoZDatabaseDataSet14;
        private System.Windows.Forms.BindingSource shopSalesInvoiceBindingSource;
        private AtoZ.datasets.AtoZDatabaseDataSet14TableAdapters.ShopSalesInvoiceTableAdapter shopSalesInvoiceTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn billIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn billDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shopIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn shopNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonFilter;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.RadioButton radioButtonNone;
        private System.Windows.Forms.RadioButton radioButtonGeneral;
        private System.Windows.Forms.RadioButton radioButtonHosiery;
        private System.Windows.Forms.RadioButton radioButtonKalaKola;
        private System.Windows.Forms.RadioButton radioButtonExtraTax;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewSelectOne;
        private System.Windows.Forms.Button buttonDeliveryReport;
        private System.Windows.Forms.Button buttonMonthlyDeliveryReport;
        private System.Windows.Forms.Button buttonMonthlyInAndOut;
        private System.Windows.Forms.Button buttonTopShopsBySale;
        private System.Windows.Forms.Button buttonTopItemsBySale;
        private System.Windows.Forms.Button buttonComputeDemand;
        private System.Windows.Forms.Button buttonVisualizeData;
        private System.Windows.Forms.Button buttonPrint2;
        private System.Windows.Forms.DataGridView dataGridViewTaxCat;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxCat;
        private datasets.AtoZDatabaseDataSet atoZDatabaseDataSet;
        private System.Windows.Forms.DataGridView dataGridViewShops;
        private AtoZDatabaseDataSetShop_IdName atoZDatabaseDataSetShop_IdName;
        private System.Windows.Forms.BindingSource shopsBindingSource;
        private AtoZDatabaseDataSetShop_IdNameTableAdapters.shopsTableAdapter shopsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn GwoGST;
        private System.Windows.Forms.DataGridViewTextBoxColumn GQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn HwoGST;
        private System.Windows.Forms.DataGridViewTextBoxColumn HQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn KKwoGST;
        private System.Windows.Forms.DataGridViewTextBoxColumn KKQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExtraTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
    }
}
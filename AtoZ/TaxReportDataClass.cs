﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtoZ
{
    public class TaxReportDataClass
    {
        public int billId { get;set;}

        public DateTime billDate { get; set; }

        public string shopName { get; set; }
        public string itemsDescription { get; set; }
        public TaxSummary taxSummary{ get;set;}
        public double hValueExcTax
        {
            get
            {
                return taxSummary.hValueExcTax;
            }
            
        }
        public double gValueExcTax
        {
            get
            {
                return taxSummary.gValueExcTax;
            }
        }
        public double kValueExcTax
        {
            get
            {
                return taxSummary.kValueExcTax;
            }
        }
        public double totalValueAllItemIncludeTaxes
        {
            get
            {
                return taxSummary.totalValueAllItemIncludeTaxes;
            }
        }

        public String perCatTaxSummaryInfo
        {
            get
            {
                String retStr = "";
                foreach(var kvp in taxSummary.perTaxCatTax_ValExcTax)
                {
                    retStr += kvp.Key + " = " + kvp.Value.ToString("0.00") + " , "; 
                }
                return retStr;
            }
        }
    }
}

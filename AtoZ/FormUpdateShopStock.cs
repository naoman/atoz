﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormUpdateShopStock : Form
    {
        private const int INDEX_BALANCE = 1;
        public FormUpdateShopStock()
        {
            InitializeComponent();

            
        }

        private void shopStatusBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                List<AtoZDatabaseDataSet_ShopStatus.shopStatusRow> rowsToBeDeleted = 
                    new List<AtoZDatabaseDataSet_ShopStatus.shopStatusRow>();
                    
                foreach(AtoZDatabaseDataSet_ShopStatus.shopStatusRow row in this.atoZDatabaseDataSet_ShopStatus.shopStatus.Rows)
                {
                    if(row.balance == 0)
                    {
                        rowsToBeDeleted.Add(row); 
                    }
                }

                //delete rows

                this.shopStatusDataGridView.DataSource = null;

                foreach(var row in rowsToBeDeleted)
                {
                    row.Delete();
                }
                this.shopStatusDataGridView.DataSource = this.shopStatusBindingSource;

                this.Validate();
                this.shopStatusBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.atoZDatabaseDataSet_ShopStatus);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Exception: " + ex.Message);
            }
        }

        private void FormUpdateShopStock_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet6.shops' table. You can move, or remove it, as needed.
            this.shopsTableAdapter.Fill(this.atoZDatabaseDataSet6.shops);
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet_ShopStatus.shopStatus' table. You can move, or remove it, as needed.
            this.shopStatusTableAdapter.Fill(this.atoZDatabaseDataSet_ShopStatus.shopStatus);

            // select first row of shop gridview
            this.dataGridView1.Rows[0].Selected = true;
            dataGridView1_RowHeaderMouseClick(null, (DataGridViewCellEventArgs)null);


        }


        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                int shopId = (int)dataGridView1.CurrentRow.Cells[0].Value;
                this.shopStatusTableAdapter.FillByShopId(this.atoZDatabaseDataSet_ShopStatus.shopStatus, shopId);
                this.labelShopSelected.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void buttonImportData_Click(object sender, EventArgs e)
        {
            if (this.labelShopSelected.Text.Equals("none"))
            {
                MessageBox.Show("Please select a shop");
                return;
            }

            FormImportShopStock formImportShopStock = new FormImportShopStock();
            formImportShopStock.shopStatus = this.atoZDatabaseDataSet_ShopStatus;
            formImportShopStock.shopId = (int)dataGridView1.CurrentRow.Cells[0].Value;
            formImportShopStock.Show();
            
        }

        private void buttonAddAllItems_Click(object sender, EventArgs e)
        {
            if (this.labelShopSelected.Text.Equals("none"))
            {
                MessageBox.Show("Please select a shop");
                return;
            }

            List<int> currentItemIds = new List<int>();
            foreach (AtoZDatabaseDataSet_ShopStatus.shopStatusRow
                row in this.atoZDatabaseDataSet_ShopStatus.shopStatus.Rows)
            {
                currentItemIds.Add(row.itemId);
            }

            
            foreach(var item in ItemDetailsHelper.getAllItems())
            {
                if (!currentItemIds.Contains(item.Key))
                {
                    AtoZDatabaseDataSet_ShopStatus.shopStatusRow newRow = this.atoZDatabaseDataSet_ShopStatus.shopStatus.NewshopStatusRow();

                    try
                    {
                        newRow.itemId = item.Key;
                        newRow.shopId = getShopId();
                        newRow.balance = 0;
                        this.atoZDatabaseDataSet_ShopStatus.shopStatus.Rows.Add(newRow);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }

        }

        private int getShopId()
        {
            return (int)dataGridView1.CurrentRow.Cells[0].Value;
        }
    }
}

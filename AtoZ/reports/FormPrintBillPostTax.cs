﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtoZ.reports;
using AtoZ.datasets;


namespace AtoZ
{
    public partial class FormPrintBillPostTax : Form
    {
        public FormPrintBillPostTax()
        {
            InitializeComponent();
        }

        public void setDataSource(AtoZDatabaseDataSet_ShopSalesInvoice dataSet)
        {
            this.CrystalReportBillPostTax1.SetDataSource(dataSet);

            CrystalDecisions.CrystalReports.Engine.TextObject textBillDate;
            textBillDate = this.CrystalReportBillPostTax1.ReportDefinition.ReportObjects["TextBillDate"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            textBillDate.Text = ((DateTime)dataSet.ShopSalesInvoice.Rows[0]["billDate"]).ToString("dd/MM/yyyy");

            CrystalDecisions.CrystalReports.Engine.TextObject txtShopName;
            txtShopName = this.CrystalReportBillPostTax1.ReportDefinition.ReportObjects["TextBuyerName"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            txtShopName.Text = ShopsDetailHelper.getShopName(int.Parse(dataSet.ShopSalesInvoice.Rows[0]["shopId"].ToString()));

            CrystalDecisions.CrystalReports.Engine.TextObject txtSalesINvoiceId;
            txtSalesINvoiceId = this.CrystalReportBillPostTax1.ReportDefinition.ReportObjects["TextSalesInvoiceNo"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            txtSalesINvoiceId.Text = dataSet.ShopSalesInvoice.Rows[0]["InvoiceId"].ToString();

            CrystalDecisions.CrystalReports.Engine.TextObject textSalesTaxRegNo;
            textSalesTaxRegNo = this.CrystalReportBillPostTax1.ReportDefinition.ReportObjects["TextSalesTaxRegNo"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            textSalesTaxRegNo.Text = dataSet.ShopSalesInvoice.Rows[0]["salesTaxRegNo"].ToString();

            CrystalDecisions.CrystalReports.Engine.TextObject serialNo;
            serialNo = this.CrystalReportBillPostTax1.ReportDefinition.ReportObjects["TextSerialNo"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            serialNo.Text = dataSet.ShopSalesInvoice.Rows[0]["billId"].ToString();

        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}

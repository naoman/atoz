﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormReportMonthlyDeliveryAndSales : Form
    {
        public FormReportMonthlyDeliveryAndSales()
        {
            InitializeComponent();
        }

        public void setDateSource(List<MonthlyDeliveryAndSalesData> data)
        {
            this.CrystalReportMonthlyDeliveriesAndSales2.SetDataSource(data);
        }
    }
}

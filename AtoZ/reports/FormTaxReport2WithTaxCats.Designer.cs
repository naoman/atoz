﻿using AtoZ.reports;
namespace AtoZ
{
    partial class FormTaxReport2WithTaxCats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CrystalReportTaxSummary1 = new AtoZ.reports.CrystalReportTaxSummary();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.CrystalReportTaxSummary2WithTaxCats1 = new AtoZ.reports.CrystalReportTaxSummary2WithTaxCats();
            this.SuspendLayout();
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = 0;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.ReportSource = this.CrystalReportTaxSummary2WithTaxCats1;
            this.crystalReportViewer1.Size = new System.Drawing.Size(1498, 917);
            this.crystalReportViewer1.TabIndex = 0;
            // 
            // FormTaxReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1498, 917);
            this.Controls.Add(this.crystalReportViewer1);
            this.Name = "FormTaxReport2WithTaxCats";
            this.Text = "FormTaxReport2WithTaxCats";
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalReportTaxSummary CrystalReportTaxSummary1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private CrystalReportTaxSummary2WithTaxCats CrystalReportTaxSummary2WithTaxCats1;
    }
}
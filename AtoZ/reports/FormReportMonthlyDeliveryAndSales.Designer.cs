﻿using AtoZ.reports;

namespace AtoZ
{
    partial class FormReportMonthlyDeliveryAndSales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.CrystalReportMonthlyDeliveriesAndSales1 = new AtoZ.reports.CrystalReportMonthlyDeliveriesAndSales();
            this.CrystalReportMonthlyDeliveriesAndSales2 = new AtoZ.reports.CrystalReportMonthlyDeliveriesAndSales();
            this.SuspendLayout();
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = 0;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.ReportSource = this.CrystalReportMonthlyDeliveriesAndSales2;
            this.crystalReportViewer1.Size = new System.Drawing.Size(1558, 1143);
            this.crystalReportViewer1.TabIndex = 0;
            // 
            // FormReportMonthlyDeliveryAndSales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1558, 1143);
            this.Controls.Add(this.crystalReportViewer1);
            this.Name = "FormReportMonthlyDeliveryAndSales";
            this.Text = "FormReportMonthlyDeliveryAndSales";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private CrystalReportMonthlyDeliveriesAndSales CrystalReportMonthlyDeliveriesAndSales1;
        private CrystalReportMonthlyDeliveriesAndSales CrystalReportMonthlyDeliveriesAndSales2;
    }
}
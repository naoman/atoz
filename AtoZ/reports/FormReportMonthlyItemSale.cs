﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ.reports
{
    public partial class FormReportMonthlyItemSale : Form
    {
        public FormReportMonthlyItemSale()
        {
            InitializeComponent();
        }

        public void setDataSource(List<MonthlyItemSaleData> data)
        {
            this.CrystalReportMonthlyItemSale1.SetDataSource(data);
        }

        public void setReportTitle(String title)
        {
            CrystalDecisions.CrystalReports.Engine.TextObject reportTitle;
            reportTitle = this.CrystalReportMonthlyItemSale1.ReportDefinition.ReportObjects["TextTitle"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            reportTitle.Text = title;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtoZ.reports;
using AtoZ.datasets;

namespace AtoZ
{
    public partial class FormChallanReport : Form
    {
        public FormChallanReport()
        {
            InitializeComponent();
/*
            AtoZDatabaseDataSet_DeliveryChallan_Report deliveryChallanDataset = new  AtoZDatabaseDataSet_DeliveryChallan_Report();
            AtoZDatabaseDataSet_DeliveryChallan_ReportTableAdapters.DeliveryChallanTableAdapter deliveryChallanTableAdapter =
                new AtoZDatabaseDataSet_DeliveryChallan_ReportTableAdapters.DeliveryChallanTableAdapter();


            deliveryChallanTableAdapter.Fill(deliveryChallanDataset.DeliveryChallan);

            this.CrystalReportChallan1.SetDataSource(deliveryChallanDataset);
 */ 
        }

        public void setDataSource(AtoZDatabaseDataSet_DeliveryChallan dataSet)
        {
            try
            {
                this.CrystalReportChallan1.SetDataSource(dataSet);

                CrystalDecisions.CrystalReports.Engine.TextObject txtDate;
                txtDate = this.CrystalReportChallan1.ReportDefinition.ReportObjects["textDate"] as CrystalDecisions.CrystalReports.Engine.TextObject;
                txtDate.Text = ((DateTime)dataSet.DeliveryChallan.Rows[0]["challanDate"]).ToString("dd/MM/yyyy");

                CrystalDecisions.CrystalReports.Engine.TextObject txtShopName;
                txtShopName = CrystalReportChallan1.ReportDefinition.ReportObjects["textShopName"] as CrystalDecisions.CrystalReports.Engine.TextObject;
                txtShopName.Text = ShopsDetailHelper.getShopName(int.Parse(dataSet.DeliveryChallan.Rows[0]["shopId"].ToString()));

                CrystalDecisions.CrystalReports.Engine.TextObject txtChallanID;
                txtChallanID = CrystalReportChallan1.ReportDefinition.ReportObjects["textChallanId"] as CrystalDecisions.CrystalReports.Engine.TextObject;
                txtChallanID.Text = dataSet.DeliveryChallan.Rows[0]["challanId"].ToString();

                CrystalDecisions.CrystalReports.Engine.TextObject textTotalItemCount;
                textTotalItemCount = CrystalReportChallan1.ReportDefinition.ReportObjects["textTotalItemCount"] as CrystalDecisions.CrystalReports.Engine.TextObject;
                textTotalItemCount.Text = getTotalItemCount(dataSet).ToString();

                CrystalDecisions.CrystalReports.Engine.TextObject textPoNo;
                textPoNo = CrystalReportChallan1.ReportDefinition.ReportObjects["TextPoNo"] as CrystalDecisions.CrystalReports.Engine.TextObject;
                textPoNo.Text = dataSet.DeliveryChallan.Rows[0]["orderno"].ToString();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private double getTotalItemCount(AtoZDatabaseDataSet_DeliveryChallan dataSet)
        {
            double totalCount = 0;
            foreach (AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow
               row in dataSet.DeliveryChallan.Rows)
            {
                totalCount += row.itemQuantity;
            }
            return totalCount;
        }
        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}

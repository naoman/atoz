﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ.reports
{
    public partial class FormReportMonthlyShopSale : Form
    {
        public FormReportMonthlyShopSale()
        {
            InitializeComponent();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }

        public void setDataSource(List<MonthlyShopSaleData> data)
        {
            this.CrystalReportMonthlyShopSale1.SetDataSource(data);
        }

        public void setTitle(String title)
        {
            CrystalDecisions.CrystalReports.Engine.TextObject reportTitle;
            reportTitle = this.CrystalReportMonthlyShopSale1.ReportDefinition.ReportObjects["TextTitle"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            reportTitle.Text = title;

        }
    }
}

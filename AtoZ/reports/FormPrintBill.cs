﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormPrintBill : Form
    {
        public FormPrintBill()
        {
            InitializeComponent();
        }

        public void setDataSource(AtoZDatabaseDataSet_ShopSalesInvoice dataSet)
        {
            this.CrystalReportBill21.SetDataSource(dataSet);

            CrystalDecisions.CrystalReports.Engine.TextObject textBillDate;
            textBillDate = this.CrystalReportBill21.ReportDefinition.ReportObjects["TextBillDate"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            textBillDate.Text = ((DateTime)dataSet.ShopSalesInvoice.Rows[0]["billDate"]).ToString("dd/MM/yyyy");

            CrystalDecisions.CrystalReports.Engine.TextObject txtShopName;
            txtShopName = this.CrystalReportBill21.ReportDefinition.ReportObjects["TextBuyerName"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            txtShopName.Text = ShopsDetailHelper.getShopName(int.Parse(dataSet.ShopSalesInvoice.Rows[0]["shopId"].ToString()));

            CrystalDecisions.CrystalReports.Engine.TextObject txtSalesINvoiceId;
            txtSalesINvoiceId = this.CrystalReportBill21.ReportDefinition.ReportObjects["TextSalesInvoiceNo"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            txtSalesINvoiceId.Text = dataSet.ShopSalesInvoice.Rows[0]["InvoiceId"].ToString();

            CrystalDecisions.CrystalReports.Engine.TextObject textSalesInvoiceDate;
            textSalesInvoiceDate = this.CrystalReportBill21.ReportDefinition.ReportObjects["TextSalesInvoiceDate"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            textSalesInvoiceDate.Text = ((DateTime)dataSet.ShopSalesInvoice.Rows[0]["invoiceDate"]).ToString("dd/MM/yyyy");

            CrystalDecisions.CrystalReports.Engine.TextObject serialNo;
            serialNo = this.CrystalReportBill21.ReportDefinition.ReportObjects["TextSerialNo"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            serialNo.Text = dataSet.ShopSalesInvoice.Rows[0]["billId"].ToString();

        }

        public void set5PercentString(String str)
        {
            CrystalDecisions.CrystalReports.Engine.TextObject obj;
            obj = this.CrystalReportBill21.ReportDefinition.ReportObjects["Text5Percent"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            obj.Text = str;
        }

        public void set17PercentString(String str)
        {
            CrystalDecisions.CrystalReports.Engine.TextObject obj;
            obj = this.CrystalReportBill21.ReportDefinition.ReportObjects["Text17Percent"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            obj.Text = str;
        }

        public void setKalaKolaPercentString(String str)
        {
            CrystalDecisions.CrystalReports.Engine.TextObject obj;
            obj = this.CrystalReportBill21.ReportDefinition.ReportObjects["TextKalaKola"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            obj.Text = str;
        }

        public void setPerTaxCatInfoString(String str)
        {
            CrystalDecisions.CrystalReports.Engine.TextObject obj;
            obj = this.CrystalReportBill21.ReportDefinition.ReportObjects["TextPerTaxCatInfo"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            obj.Text = str;

        }
    }
}

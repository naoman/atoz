﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace AtoZ.reports
{
    public partial class FormChart : Form
    {
        private Dictionary<string, ChartDataDetails> _data;
        public FormChart()
        {
            InitializeComponent();
            this.dataGridView1.Cursor = this.Cursor;
        }


        public void setDataSource(Dictionary<string, ChartDataDetails> data)
        {
            _data = data;
            foreach(var var in _data)
            {
                this.dataGridView1.Rows.Add(var.Key, ItemDetailsHelper.getItemName(int.Parse(var.Key)),
                    var.Value.average, var.Value.sd);
            }

            this.dataGridView1.PerformLayout();
                
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var series = new Series
            {
                Name = "Series",
                Color = System.Drawing.Color.Green,
                IsVisibleInLegend = false,
                IsXValueIndexed = false,
                ChartType = SeriesChartType.Column
            };

            chart1.Series.Clear();
            this.chart1.Series.Add(series);
            series.Points.Clear();
            foreach(var dataPoint in _data[this.dataGridView1.CurrentRow.Cells[0].Value.ToString()].dataPoints)
            {
                series.Points.Add(dataPoint);
            }

            /*
            series.Points.Add(new DataPoint(1, 10));
            series.Points.Add(new DataPoint(1000, 10000));
            series.Points.Add(new DataPoint(10, 100));
            */
            chart1.ChartAreas[0].AxisX.IsLogarithmic = true;
            chart1.ChartAreas[0].AxisX.Minimum = series.Points.Select(i => i.XValue).Min();
            chart1.Invalidate();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            this.dataGridView1_CellClick(null, null);
        }

        private void dataGridView1_KeyUp(object sender, KeyEventArgs e)
        {
            this.dataGridView1_CellClick(null, null);
        }

    }

    public class ChartDataDetails
    {
        public List<DataPoint> dataPoints { get; set; }
        public double average { get; set; }
        public double sd { get; set; }
    }

}

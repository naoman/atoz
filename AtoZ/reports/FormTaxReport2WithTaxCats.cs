﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormTaxReport2WithTaxCats : Form
    {
        public FormTaxReport2WithTaxCats()
        {
            InitializeComponent();
        }
        
        public void setSource(List<TaxReportDataClass> list)
        {
            this.CrystalReportTaxSummary2WithTaxCats1.SetDataSource(list);
        }

        public void setTotalTaxInfoString(String str)
        {
            CrystalDecisions.CrystalReports.Engine.TextObject taxInfoStr;
            taxInfoStr = this.CrystalReportTaxSummary2WithTaxCats1.ReportDefinition.ReportObjects["TextTotalTaxInfo"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            taxInfoStr.Text = str;

        }
    }
}

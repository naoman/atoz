﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtoZ.reports
{
    public class ClassDemandCalculation
    {
        private Dictionary<int, ItemSaleDataSummary> itemSaleDataForAllItems = null;
        private Dictionary<string, ShopMonthlySaleData> shopSaleDataForAllShops = null;
        private Dictionary<int, List<KeyValuePair<int, int>>> itemDeliveryCounts = null;        
        
        public Dictionary<int, ItemSaleDataSummary> getItemSaleDataHashSet(DateTime startDate, DateTime endDate)
        {
            Compute(startDate, endDate);
            return itemSaleDataForAllItems;
        }

        public Dictionary<string, ShopMonthlySaleData> getShopSaleDataForAllShops(DateTime startDate, DateTime endDate)
        {
            Compute(startDate, endDate);
            return shopSaleDataForAllShops;
        }

        public static int getMonthCount (int shopId,  Dictionary<string, ShopMonthlySaleData> shopSalesData)

        {
            List<string> months = new List<string>();
            foreach(var data in shopSalesData)
            {
                string[] strings = data.Key.Split('_');
                string id = strings[0];
                if(shopId.ToString().Equals(id))
                {
                    if(!months.Contains(strings[1]+"_"+strings[2]))
                    {
                        months.Add(strings[1] + "_" + strings[2]);
                    }
                }
            }
            return months.Count;
        }

        public static Dictionary<string,int> getBillMonthsCount(Dictionary<string, ShopMonthlySaleData> shopSalesData)
        {
            Dictionary<string, int> months = new Dictionary<string, int>();
            foreach (var data in shopSalesData)
            {
                string[] strings = data.Key.Split('_');
                string key = strings[1] + "_" + strings[2];
                if (!months.ContainsKey(key))
                {
                    months.Add(key,0);
                }
                months[key]++;
            }
            return months;
        }


        private void Compute(DateTime startDate, DateTime endDate)
        {
            itemSaleDataForAllItems = new Dictionary<int, ItemSaleDataSummary>();
            shopSaleDataForAllShops = new Dictionary<string, ShopMonthlySaleData>();

            AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapterBills = new
            datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();

            AtoZDatabaseDataSet_ShopSalesInvoice datasetBills = new AtoZDatabaseDataSet_ShopSalesInvoice();
            tableAdapterBills.FillByDateRange(datasetBills.ShopSalesInvoice,
                startDate.ToString("yyyy-MM-dd HH:mm:ss"),
                endDate.ToString("yyyy-MM-dd HH:mm:ss"));

            foreach (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow row in datasetBills.ShopSalesInvoice)
            {
                string key = getShopIdAndMonthKey(row.shopId, row.invoiceDate);
                // add shop total sate
                if (!shopSaleDataForAllShops.ContainsKey(key))
                {
                    shopSaleDataForAllShops.Add(key, new ShopMonthlySaleData(){ itemTypeCount =0, 
                         totalSale = 0});
                }

                shopSaleDataForAllShops[key].totalSale += row.itemPrice * row.itemQuantity;
                shopSaleDataForAllShops[key].itemTypeCount++;

                // add items sale
                if (!itemSaleDataForAllItems.ContainsKey(row.itemCatNo))
                {
                    itemSaleDataForAllItems.Add(row.itemCatNo, new ItemSaleDataSummary()
                    {
                        monthlySales = new Dictionary<string, ItemSaleDataForAShopMonth>(),
                        average = 0,
                        standartDeviation = 0,
                         shopSale = new Dictionary<int,double>()
                    });
                }

                ItemSaleDataSummary itemSaleDataSummary = itemSaleDataForAllItems[row.itemCatNo];

                // add monthly sale
                if (!itemSaleDataSummary.monthlySales.ContainsKey(key))
                {
                    itemSaleDataSummary.monthlySales.Add(key,
                        new ItemSaleDataForAShopMonth() { sale = 0, saleRatio = 0 });
                }

                ItemSaleDataForAShopMonth itemSaleDataForAShopMonth = itemSaleDataSummary.monthlySales[key];
                itemSaleDataForAShopMonth.sale += row.itemPrice * row.itemQuantity;

                // add sale to shop sale
                if(!itemSaleDataSummary.shopSale.ContainsKey(row.shopId))
                {
                    itemSaleDataSummary.shopSale.Add(row.shopId, 0);
                }

                itemSaleDataSummary.shopSale[row.shopId] += row.itemPrice * row.itemQuantity;
            }

            updateItemSaleRation();
            updateItemSaleSummary();

        }
        string getShopIdAndMonthKey(int shopId, DateTime date)
        {
            return shopId + "_" + date.Year + "_" +date.Month;
        }

        void updateItemSaleRation()
        {
            foreach(var item in itemSaleDataForAllItems)
            {
                foreach(KeyValuePair<string,ItemSaleDataForAShopMonth> monthlySale in item.Value.monthlySales)
                {
                    monthlySale.Value.saleRatio = 
                        monthlySale.Value.sale / shopSaleDataForAllShops[monthlySale.Key].totalSale * 100;
                  //  double salePerItemType = shopSaleDataForAllShops[monthlySale.Key].totalSale / 
                  //      shopSaleDataForAllShops[monthlySale.Key].itemTypeCount;
                  //  monthlySale.Value.saleRatio = monthlySale.Value.sale / salePerItemType * 100;
                }
            }
        }

        void updateItemSaleSummary()
        {
            foreach(var item in itemSaleDataForAllItems)
            {
                item.Value.average = item.Value.monthlySales.Values.Select( v => (double)v.saleRatio ).Average();
                item.Value.standartDeviation = CalculateStdDev(item.Value.monthlySales.Values.Select( v => (double)v.saleRatio ));
            }
        }

        private double CalculateStdDev(IEnumerable<double> values)
        {
            double ret = 0;
            if (values.Count() > 0)
            {
                //Compute the Average      
                double avg = values.Average();
                //Perform the Sum of (value-avg)_2_2      
                double sum = values.Sum(d => Math.Pow(d - avg, 2));
                //Put it all together      
                ret = Math.Sqrt((sum) / (values.Count() - 1));
            }
            return ret;
        }

        public Dictionary<int, List<KeyValuePair<int, int>>> getItemDeliveryCounts(DateTime dateTime1, DateTime dateTime2)
        {
            Dictionary<int, Dictionary<int, int>>  _itemDeliveryCounts = new Dictionary<int, Dictionary<int, int>>();

            AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter tableAdapter = new
                AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();

            AtoZDatabaseDataSet_DeliveryChallan dataset = new AtoZDatabaseDataSet_DeliveryChallan();
            tableAdapter.FillByDateRange(dataset.DeliveryChallan,
                dateTime1.ToString("yyyy-MM-dd HH:mm:ss"),
                dateTime2.ToString("yyyy-MM-dd HH:mm:ss"));

            foreach (AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow row in dataset.DeliveryChallan)
            {
                if(!_itemDeliveryCounts.ContainsKey(row.itemCatNo))
                {
                    _itemDeliveryCounts.Add(row.itemCatNo,new Dictionary<int,int>());
                }

                Dictionary<int,int> itemCount = _itemDeliveryCounts[row.itemCatNo];

                if(!itemCount.ContainsKey((int)row.itemQuantity))
                {
                    itemCount.Add((int)row.itemQuantity,0);
                }
                
                itemCount[(int)row.itemQuantity] ++;
 
            }

            // sort the quantities by their counts
            itemDeliveryCounts = new Dictionary<int,List<KeyValuePair<int,int>>>();
            foreach(KeyValuePair<int,Dictionary<int,int>> kvp in _itemDeliveryCounts)
            {
                itemDeliveryCounts.Add(kvp.Key, kvp.Value.ToList<KeyValuePair<int,int>>());
                itemDeliveryCounts[kvp.Key].Sort((firstPair,nextPair) =>
                    {
                        return nextPair.Value.CompareTo(firstPair.Value);
                    }
                );

                //trim
                itemDeliveryCounts[kvp.Key] = itemDeliveryCounts[kvp.Key].Take(10).ToList<KeyValuePair<int,int>>();
            }
            
            return itemDeliveryCounts;
        }

        public static List<MonthlyShopSaleData> GetSortedShopListBySale(DateTime start, DateTime end, Boolean convertSaleToAverage = false)
        {
            Dictionary<int, MonthlyShopSaleData> shopIdSaleHashset = new Dictionary<int, MonthlyShopSaleData>();

            AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapterBills = new
                 datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();

            AtoZDatabaseDataSet_ShopSalesInvoice datasetBills = new AtoZDatabaseDataSet_ShopSalesInvoice();
            tableAdapterBills.FillByDateRange(datasetBills.ShopSalesInvoice,
                start.ToString("yyyy-MM-dd HH:mm:ss"),
                end.ToString("yyyy-MM-dd HH:mm:ss"));

            foreach (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow row in datasetBills.ShopSalesInvoice)
            {
                if (!shopIdSaleHashset.ContainsKey(row.shopId))
                {
                    shopIdSaleHashset.Add(row.shopId, new MonthlyShopSaleData { shopId = row.shopId, shopName = ShopsDetailHelper.getShopName(row.shopId), sale = 0, months = new List<string>() });
                }

                MonthlyShopSaleData data = shopIdSaleHashset[row.shopId];

                data.sale += (int)(row.itemQuantity * row.itemPrice);
                string key = row.invoiceDate.Year.ToString()+"_"+row.invoiceDate.Month.ToString();
                if(!data.months.Contains(key))
                {
                    data.months.Add(key);
                }
            }

            // convery sale to average
            shopIdSaleHashset.Values.ToList<MonthlyShopSaleData>().ForEach(o => o.sale = o.sale / o.noOfMonths);

            // sort
            List<MonthlyShopSaleData> sortedList = shopIdSaleHashset.Values.OrderByDescending(o => o.sale).ToList<MonthlyShopSaleData>();

            int totalSale = 0;

            foreach (MonthlyShopSaleData i in sortedList)
            {
                totalSale += i.sale;
            }

            // calcualte percentage and running percentage

            double lastRunningPercentage = 0;
            foreach (MonthlyShopSaleData i in sortedList)
            {
                i.percentOfTotal = (((double)i.sale) / totalSale) * 100;
                i.runningPercentOfTotal = lastRunningPercentage + i.percentOfTotal;
                lastRunningPercentage = i.runningPercentOfTotal;
            }
            return sortedList;
        }
    }

    public class ItemSaleDataForAShopMonth
    {
        public double sale {get ; set; }
        public double saleRatio {get; set;}

    }

    public class ItemSaleDataSummary
    {
        public Dictionary<string, ItemSaleDataForAShopMonth> monthlySales { get; set; }

        // total sale amount of an item for a shop
        public Dictionary<int, double> shopSale { get; set; }
        public double average {get; set;}
        public double standartDeviation { get; set; }
    }

    public class ShopMonthlySaleData
    {
        public double totalSale { get; set; }
        public int itemTypeCount { get; set; }
    }
}

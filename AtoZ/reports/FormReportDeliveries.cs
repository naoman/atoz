﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormReportDailyDeliveries : Form
    {
        public FormReportDailyDeliveries()
        {
            InitializeComponent();
            /*
            List<DeliveryReportDataClass> list = new List<DeliveryReportDataClass>();
            DeliveryReportDataClass class1 = new DeliveryReportDataClass { date = new DateTime(2015, 1,1), noOfDeliveries = 5, noOfItems = 100, noOfItemTypes = 7, valueOfItems = 20000 };
            DeliveryReportDataClass class2 = new DeliveryReportDataClass { date = new DateTime(2015, 1, 3), noOfDeliveries = 7, noOfItems = 1000, noOfItemTypes = 50, valueOfItems = 30000 };
            DeliveryReportDataClass class3 = new DeliveryReportDataClass { date = new DateTime(2015, 1, 5), noOfDeliveries = 3, noOfItems = 200, noOfItemTypes = 5, valueOfItems = 50000 };
            DeliveryReportDataClass class4 = new DeliveryReportDataClass { date = new DateTime(2015, 1, 9), noOfDeliveries = 1, noOfItems = 500, noOfItemTypes = 12, valueOfItems = 10000 };
            DeliveryReportDataClass class5 = new DeliveryReportDataClass { date = new DateTime(2015, 1, 10), noOfDeliveries = 2, noOfItems = 10, noOfItemTypes = 19, valueOfItems = 200000 };
            DeliveryReportDataClass class6 = new DeliveryReportDataClass { date = new DateTime(2015, 1, 11), noOfDeliveries = 9, noOfItems = 100, noOfItemTypes = 2, valueOfItems = 40000 };
            DeliveryReportDataClass class7 = new DeliveryReportDataClass { date = new DateTime(2015, 1, 12), noOfDeliveries = 15, noOfItems = 200, noOfItemTypes = 8, valueOfItems = 20000 };
            DeliveryReportDataClass class8 = new DeliveryReportDataClass { date = new DateTime(2015, 1, 20), noOfDeliveries = 5, noOfItems = 800, noOfItemTypes = 15, valueOfItems = 2000 };

            list.Add(class1);
            list.Add(class2);
            list.Add(class3);
            list.Add(class4);
            list.Add(class5);
            list.Add(class6);
            list.Add(class7);
            list.Add(class8);

            this.CrystalReportDeliveries2.SetDataSource(list);
            */

        }

        public void setDataSource(List<DeliveryReportDataClass> data)
        {
            this.CrystalReportDeliveries2.SetDataSource(data);
        }
        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            
        }
    }
}

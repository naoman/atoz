﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtoZ
{
    public class DeliveryReportDataClass
    {
        public DateTime date {get; set;}
        public int noOfDeliveries { get { return challanIdsList.Count; } }
        public int noOfItems { get; set; }
        public int noOfItemTypes { get; set; }
        public int valueOfItems { get; set; }

        public List<int> challanIdsList = new List<int>();

    }

    public class MonthlyDeliveryAndSalesData
    {
        public DateTime date { get; set; }
        public string type { get; set; }
        public int count { get; set; }

        public List<int> challanOrBillIdList = new List<int>();
    }

    public class MonthlyShopSaleData
    {
        public int shopId { get; set; }
        public string shopName { get; set; }

        // total shop sale amount for the months given in the query
        public int sale { get; set; }

        public double percentOfTotal { get; set; }
        public double runningPercentOfTotal { get; set; }

        public int noOfMonths { get { return months.Count; } }

        public List<string> months  { get; set; } 

    }

    public class MonthlyItemSaleData
    {
        public int itemCatNo { get; set; }
        public string itemName { get; set; }
        public int itemCount { get; set; }
        public int itemValue { get; set; }

        public double percentOfTotal { get; set; }
        public double runningPercentOfTotal { get; set; }

    }
}

﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormShopSalesInvoiceDetails : Form
    {
        private Boolean isDataDirty = false;

        public FormShopSalesInvoiceDetails()
        {
            InitializeComponent();
        }

        private void shopSalesInvoiceBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(this.textBoxSalesInvoiceId.Text))
            {
                MessageBox.Show("please enter sales invoice ID");
                return;
            }

            int itemCount = 0;
            List<AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow> rowsToBeRemoved =
                new  List<AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow>();
            foreach (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow row in
                this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice.Rows)
            {
                if (row.itemQuantity == 0)
                {
                    rowsToBeRemoved.Add(row);
                }
                else
                {
                    row.invoiceId = this.textBoxSalesInvoiceId.Text;
                    row.invoiceDate = this.dateTimePickerSalesInvoiceDate.Value;
                    row.billDate = this.dateTimePickerBillDate.Value;
                    
                    if (!string.IsNullOrWhiteSpace(this.textBoxTaxRegNo.Text))
                    {
                        row.salesTaxRegNo = textBoxTaxRegNo.Text;
                    }
                    itemCount++;
                }

            }

            var response = MessageBox.Show("Are you sure you want to save changes? New item cout : " +
                itemCount, "", MessageBoxButtons.YesNo);
            if (response.Equals(DialogResult.No))
            {
                return;
            }

            //update inventory by adding it back to inventory stock first
            addInventory();

            this.shopSalesInvoiceDataGridView.DataSource = null;
            rowsToBeRemoved.ForEach(x => x.Delete());
            this.shopSalesInvoiceDataGridView.DataSource = this.shopSalesInvoiceBindingSource;
            
            this.Validate();
            this.shopSalesInvoiceBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.atoZDatabaseDataSet_ShopSalesInvoice);

            //update inventory stock
            ClassUpdateShopInventory updateInventory = new  ClassUpdateShopInventory();
            updateInventory.removeInventory(this.atoZDatabaseDataSet_ShopSalesInvoice);

            this.UpdateDetails();

            this.dataGridView2.CurrentRow.Cells[1].Value = dateTimePickerBillDate.Value.ToShortDateString();

            this.isDataDirty = false;
            this.textBoxSalesInvoiceId.BackColor = Color.White;
            this.textBoxStatus.BackColor = Color.White;
            this.textBoxTaxRegNo.BackColor = Color.White;
            
            this.dateTimePickerSalesInvoiceDate.Font = 
                new Font(dateTimePickerSalesInvoiceDate.Font.FontFamily, 8.25F);
            this.dateTimePickerBillDate.Font = 
                new Font(dateTimePickerBillDate.Font.FontFamily, 8.25F);
            
        }

        private void addInventory()
        {
            // read record set first
            AtoZDatabaseDataSet_ShopSalesInvoice tempDataset = new AtoZDatabaseDataSet_ShopSalesInvoice();
            this.shopSalesInvoiceTableAdapter.FillBy(tempDataset.ShopSalesInvoice, getBillId());
            
            //update inventory
            ClassUpdateShopInventory updateInventory = new  ClassUpdateShopInventory();
            updateInventory.addInventory(tempDataset);

        }

        private void FormShopSalesInvoiceDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet9.ShopSalesInvoice' table. You can move, or remove it, as needed.
            //this.shopSalesInvoiceTableAdapter1.Fill(this.atoZDatabaseDataSet9.ShopSalesInvoice);
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet8.shops' table. You can move, or remove it, as needed.
            this.shopsTableAdapter.Fill(this.atoZDatabaseDataSet8.shops);
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice' table. You can move, or remove it, as needed.
            // following line is required so that correct row id is picked for new records
            //PK logic moved to DB this.shopSalesInvoiceTableAdapter.FillByGetRowWithMaxRowId(this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice);

            dataGridView1_CellClick(null, null);

        }

        
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                UpdateBillListTable();
                dataGridView2_CellClick(null, null);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            
        }

        private void UpdateBillListTable()
        {
            int shopId = (int)dataGridView1.CurrentRow.Cells[0].Value;
            this.shopSalesInvoiceTableAdapter1.FillBy1(this.atoZDatabaseDataSet9.ShopSalesInvoice, shopId);
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dataGridView2.CurrentRow.Index >= 0)
            {
                UpdateDetails();
            }
        }

        private void UpdateDetails()
        {
            try
            {
                UpdateDetailTable();
                HelperMethods.setRowNumbers(this.shopSalesInvoiceDataGridView);
                AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow row =
                    (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow)
                    this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice.Rows[0];

                this.textBoxSalesInvoiceId.Text = row.invoiceId;
                this.dateTimePickerSalesInvoiceDate.Value = row.invoiceDate;
                this.dateTimePickerBillDate.Value = row.billDate;
                if (!row.IssalesTaxRegNoNull())
                {
                    this.textBoxTaxRegNo.Text = row.salesTaxRegNo;
                }
                else
                {
                    this.textBoxTaxRegNo.Text = "";
                }
                this.textBoxStatus.Text = row.status;

                this.resetTaxInfo();

                if(this.checkBoxShowAllBills.Checked)
                {
                    int shopId = row.shopId;
                    selectShopInDataGridView(shopId);
                }

            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void selectShopInDataGridView(int shopId)
        {
            foreach (DataGridViewRow r in dataGridView1.Rows)
            {
                if (int.Parse(r.Cells[0].Value.ToString()) == shopId)
                {
                    dataGridView1.CurrentCell = r.Cells[0];
                }
            }
        }

        private void UpdateDetailTable()
        {
            int billId = getBillId();
            this.shopSalesInvoiceTableAdapter.FillBy(this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice, billId);
          
        }

        private int getBillId()
        {
            int billId = int.Parse(dataGridView2.CurrentRow.Cells[0].Value.ToString());
            return billId;
        }

        

     

        private void buttonPrintBill_Click(object sender, EventArgs e)
        {
            if (isDataDirty)
            {
                MessageBox.Show("Please save changes before printing");
                return;
            }

            try
            {
                int shopId = (int)dataGridView1.CurrentRow.Cells[0].Value;
                if(ShopsDetailHelper.isPostTaxShop(shopId))
                {
                    FormPrintBillPostTax form = new FormPrintBillPostTax();
                    form.setDataSource(this.atoZDatabaseDataSet_ShopSalesInvoice);
                    form.Show();
                }
                else
                {
                    FormPrintBill form = new FormPrintBill();
                    form.setDataSource(this.atoZDatabaseDataSet_ShopSalesInvoice);
                    if(this.checkBoxTaxDetails.Checked)
                    {
                        form.set5PercentString(this.label5Percent.Text);
                        form.set17PercentString(this.label17Percent.Text);
                        form.setKalaKolaPercentString(this.labelKK.Text);
                        form.setPerTaxCatInfoString(this.richTextBoxTaxCatDetails.Text);
                    }
                    form.Show();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Got Exception " + ex.Message);
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            this.dataGridView1_CellClick(null, null);
        }

        private void dataGridView1_KeyUp(object sender, KeyEventArgs e)
        {
            this.dataGridView1_CellClick(null, null);
        }

        private void dataGridView2_KeyDown(object sender, KeyEventArgs e)
        {
            this.dataGridView2_CellClick(null, null);
        }

        private void dataGridView2_KeyUp(object sender, KeyEventArgs e)
        {
            this.dataGridView2_CellClick(null, null);
        }

        private void buttonDeleteBill_Click(object sender, EventArgs e)
        {
            if (isDataDirty)
            {
                MessageBox.Show("Please save changes before deleteing");
                return;
            }

            try
            {
                String shopName = this.dataGridView1.CurrentRow.Cells[1].Value.ToString();
                int billID = getBillId();
                int itemCount = this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice.Rows.Count;

                var result = MessageBox.Show("Are you sure you want to delete bill # " + billID + " of shop " + shopName + " with " + itemCount + " items",
                    "Confirm Delete",
                    MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    return;
                }
                else
                {
                    AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapter = new
                    AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();

                    //read dataset first to update stocks
                    AtoZDatabaseDataSet_ShopSalesInvoice tempDataset = new AtoZDatabaseDataSet_ShopSalesInvoice();
                    tableAdapter.FillBy(tempDataset.ShopSalesInvoice, billID);

                    int rows = tableAdapter.DeleteSalesInvoiceByBillId(billID);
                    this.atoZDatabaseDataSet_ShopSalesInvoice.Clear();

                    //updateInventory
                    ClassUpdateShopInventory updateShopInventory = new ClassUpdateShopInventory();
                    updateShopInventory.addInventory(tempDataset);
                    
                    MessageBox.Show(rows + " Row deleted");

                    dataGridView1_CellClick(null, null);

                    // clear show all flag
                    if (this.checkBoxShowAllBills.Checked)
                    {
                        this.ignoreNextEventForShowAllBillsCheckBox = true;
                        this.checkBoxShowAllBills.Checked = false;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
        private void buttonAddItems_Click(object sender, EventArgs e)
        {
            List<int> currentItemIds = new List<int>();
            foreach (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow
                row in this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice.Rows)
            {
                currentItemIds.Add(row.itemCatNo);
            }

            int shopId = int.Parse(this.dataGridView1.CurrentRow.Cells[0].Value.ToString());

            Dictionary<int, ItemDetailsData> items;
            if (ShopsDetailHelper.isPostTaxShop(shopId))
            {
                items = ItemDetailsHelper.getAllPostTaxItems();
            }
            else
            {
                items = ItemDetailsHelper.getAllItems();
            }

            foreach (var kvp in items)
            {
                if (!currentItemIds.Contains(kvp.Key))
                {

                    AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow newRow =
                        (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow)
                        this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice.NewRow();
                    newRow.itemCatNo = kvp.Key;
                    newRow.itemName = kvp.Value.name;
                    newRow.itemPrice = kvp.Value.price;
                    newRow.itemTax = kvp.Value.tax;
                    if (kvp.Value.actualTax != null)
                    {
                        newRow.custom1 = ((double)kvp.Value.actualTax).ToString("0.00"); 
                    }
                    newRow.itemQuantity = 0;

                    newRow.billId = getBillId();
                    newRow.billDate = new DateTime(); // this will be updated when the save button is clicked
                    
                    newRow.shopId = int.Parse(this.dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    //newRow.shopName = this.dataGridView1.CurrentRow.Cells[1].Value.ToString();

                    newRow.invoiceId = ""; // this will be updated when the save button is clicked
                    newRow.invoiceDate = new DateTime();// this will be updated when the save button is clicked

                    newRow.status = this.textBoxStatus.Text;    

                    this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice.AddShopSalesInvoiceRow(newRow);
                }
            }

            HelperMethods.setRowNumbers(this.shopSalesInvoiceDataGridView);
                
        }

        private void shopSalesInvoiceDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.shopSalesInvoiceDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Red;
            resetTaxInfo();
            this.isDataDirty = true;
        }

        private void resetTaxInfo()
        {
            this.label17Percent.Text = "G: ";
            this.label5Percent.Text = "H: ";
            this.labelKK.Text = "KK: ";
            this.checkBoxTaxDetails.Checked = false;
            this.richTextBoxTaxCatDetails.Text = "";
        }

        private void buttonCalTax_Click(object sender, EventArgs e)
        {
            if(isDataDirty)
            {
                MessageBox.Show("Please save the data first");
                return;
            }
            TaxSummary taxSummary;
            Dictionary<int, TaxCalItemized> taxItemizedList;
            calculateTax(this.atoZDatabaseDataSet_ShopSalesInvoice, 
                int.Parse(this.dataGridView1.CurrentRow.Cells[0].Value.ToString()),
                out taxSummary, out taxItemizedList);

            foreach (DataGridViewRow row
                in this.shopSalesInvoiceDataGridView.Rows)
            {
                int itemId = int.Parse(row.Cells[0].Value.ToString());

                row.Cells[7].Value = taxItemizedList[itemId].valueExcTax.ToString("0.00");
                row.Cells[8].Value = taxItemizedList[itemId].totalTax.ToString("0.00");
                row.Cells[9].Value = taxItemizedList[itemId].totalValueIncTax.ToString("0.00");

                row.Cells[10].Value = taxItemizedList[itemId].kkTaxPaid != null ? ((double)taxItemizedList[itemId].kkTaxPaid).ToString("0.00") : null;
                row.Cells[11].Value = taxItemizedList[itemId].kkTaxPerItem != null ? ((double)taxItemizedList[itemId].kkTaxPerItem).ToString("0.00") : null;
                   

            }

            this.label5Percent.Text =  "H: Val Exc Tax:   " + taxSummary.hValueExcTax.ToString("0.00");
            this.label17Percent.Text = "G: Val Exc Tax:   " + taxSummary.gValueExcTax.ToString("0.00");
            this.labelKK.Text =        "K: Val Exc Tax:   " + taxSummary.kValueExcTax.ToString("0.00")+ "       17% Tax: " + taxSummary.k17.ToString("0.00") + "  |  Actual Tax: " + taxSummary.kActual.ToString("0.00") + "  |  Extra Tax: " + taxSummary.kExtra.ToString("0.00");
            
            if(taxSummary.perTaxCatTax_ValExcTax != null)
            {
                String perTaxCatTaxInfo = "";
                foreach(var kvp in taxSummary.perTaxCatTax_ValExcTax)
                {
                    perTaxCatTaxInfo += kvp.Key + " = " + kvp.Value.ToString("0.00") + " , "; 
                }

                this.richTextBoxTaxCatDetails.Text = "Val Exc Tax : " + perTaxCatTaxInfo;
            }
        }

        public static void calculateTax(AtoZDatabaseDataSet_ShopSalesInvoice dataset,int shopId,
            out TaxSummary taxSummary, out Dictionary<int,TaxCalItemized> taxItemizedList)
        {
            if (ShopsDetailHelper.isPostTaxShop(shopId))
            {
                calculateTaxPostTax(dataset, out taxSummary, out taxItemizedList);
            }
            else
            {
                calculateTaxRegular(dataset, out taxSummary, out taxItemizedList);
            }
        }

        private static void calculateTaxRegular(AtoZDatabaseDataSet_ShopSalesInvoice dataset, out TaxSummary taxSummary, out Dictionary<int, TaxCalItemized> taxItemizedList)
        {
            double total5Perc = 0;
            double total17Perc = 0;
            double valExcTax5Perc = 0;
            double valExcTax17Perc = 0;
            double valExcTaxKK = 0;
            double totalTax5Perc = 0;
            double totalTax17Perc = 0;
            double kalaKolaItemsTaxPaid = 0;
            double kalaKolaItems17PercTax = 0;
            double gTotalQ = 0;
            double hTotalQ = 0;
            double kkTotalQ = 0;
            Dictionary<String, double> perTaxCatTax_ValExcTax = new Dictionary<string,double>(); // tax per tax category
            Dictionary<String, double> perTaxCatTax_Quantity = new Dictionary<string, double>(); // quantity per tax category

            taxItemizedList = new Dictionary<int, TaxCalItemized>();

            foreach (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow row
                in dataset.ShopSalesInvoice.Rows)
            {
                int itemId = row.itemCatNo;
                double itemQ = row.itemQuantity;
                double itemP = row.itemPrice;
                double tax = row.itemTax;
                double? actualTax = row.Iscustom1Null() ? (double?)null : double.Parse(row.custom1);

                double totalVal = itemQ * itemP;
                double valExcTax = totalVal / ((100 + tax) / 100);
                double totalTax = totalVal - valExcTax;



                TaxCalItemized taxCalItemized = new TaxCalItemized()
                {
                    totalTax = totalTax,
                    totalValueIncTax = totalVal,
                    valueExcTax = valExcTax
                };

                //check for KK items
                if (actualTax != null)
                {
                    double taxPaid = itemQ * (double)actualTax;
                    taxCalItemized.kkTaxPaid = taxPaid;

                    kalaKolaItemsTaxPaid += taxPaid;
                    kalaKolaItems17PercTax += totalTax;

                    taxCalItemized.kkTaxPerItem = (double)actualTax;

                    valExcTaxKK += valExcTax;
                    kkTotalQ += itemQ;
                }

                if (!taxItemizedList.ContainsKey(row.itemCatNo))
                {
                    taxItemizedList.Add(row.itemCatNo, taxCalItemized);
                }

                if (tax == 5 || tax == 6 || tax == 9 || row.itemCatNo.ToString().Substring(0,1).Equals("7"))
                {
                    // earlier hosiery tax was 5, 6, or 9. Now it can be 17. So adding check to cat number.
                    // Hosiery cat number starts with 7.
                    totalTax5Perc += totalTax;
                    valExcTax5Perc += valExcTax;
                    total5Perc += totalVal;
                    hTotalQ += itemQ;
                }
                else if (tax == 17)
                {
                    totalTax17Perc += totalTax;
                    valExcTax17Perc += valExcTax;
                    total17Perc += totalVal;
                    if (actualTax == null)
                    {
                        // not sure whats the best way to calculate general total quantity. Since KK is already computed, adding
                        // the above if condition so that its not counted twice.
                        gTotalQ += itemQ;
                    }
                }
                else
                {
                    MessageBox.Show("Unknown tax amount " + tax);
                }

                // caclulate Tax Per Tax Category
                string taxCategory = "UNKNOWN";
                if (!String.IsNullOrWhiteSpace(ItemDetailsHelper.getItemDetails(row.itemCatNo).custom3))
                {
                    taxCategory = ItemDetailsHelper.getItemDetails(row.itemCatNo).custom3;
                }

                if(perTaxCatTax_ValExcTax.ContainsKey(taxCategory))
                {
                    perTaxCatTax_ValExcTax[taxCategory] += valExcTax;
                    perTaxCatTax_Quantity[taxCategory] += itemQ;
                }
                else
                {
                    perTaxCatTax_ValExcTax[taxCategory] = valExcTax;
                    perTaxCatTax_Quantity[taxCategory] = itemQ;
                }
            }

            taxSummary = new TaxSummary()
            {
                gValueExcTax = (valExcTax17Perc - valExcTaxKK),
                gQuantity = gTotalQ,
                hValueExcTax = valExcTax5Perc,
                hQuantity = hTotalQ,
                kValueExcTax = valExcTaxKK,
                kQuantity = kkTotalQ,
                k17 = kalaKolaItems17PercTax,
                kActual = kalaKolaItemsTaxPaid,
                kExtra = (kalaKolaItemsTaxPaid - kalaKolaItems17PercTax),
                totalValueAllItemIncludeTaxes = total17Perc + total5Perc,
                perTaxCatTax_ValExcTax = perTaxCatTax_ValExcTax,
                perTaxCatTax_Quantity = perTaxCatTax_Quantity
            };
        }

        private static void calculateTaxPostTax(AtoZDatabaseDataSet_ShopSalesInvoice dataset, out TaxSummary taxSummary, out Dictionary<int, TaxCalItemized> taxItemizedList)
        {
            double total17Perc = 0;
            double valExcTax17Perc = 0;
            double totalTax17Perc = 0;
            double totalQ = 0;
            Dictionary<String, double> perTaxCatTax_ValExcTax = new Dictionary<string, double>(); // tax per tax category
            Dictionary<String, double> perTaxCatTax_Quantity = new Dictionary<string, double>(); // quantity per tax category

            
            taxItemizedList = new Dictionary<int, TaxCalItemized>();

            foreach (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow row
                in dataset.ShopSalesInvoice.Rows)
            {
                int itemId = row.itemCatNo;
                double itemQ = row.itemQuantity;
                double itemP = row.itemPrice;
                double tax = row.itemTax;

                double totalVal = (itemQ * itemP) * (tax/100 + 1);
                double valExcTax = (itemQ * itemP);
                double totalTax = totalVal - valExcTax;
             
                TaxCalItemized taxCalItemized = new TaxCalItemized()
                {
                    totalTax = totalTax,
                    totalValueIncTax = totalVal,
                    valueExcTax = valExcTax
                };

                if (!taxItemizedList.ContainsKey(row.itemCatNo))
                {
                    taxItemizedList.Add(row.itemCatNo, taxCalItemized);
                }

                if (tax == 17)
                {
                    totalTax17Perc += totalTax;
                    valExcTax17Perc += valExcTax;
                    total17Perc += totalVal;
                    totalQ += itemQ;
                }
                else
                {
                    MessageBox.Show("Unknown tax amount " + tax);
                }

                // caclulate Tax Per Tax Category
                string taxCategory = "UNKNOWN";
                if (!String.IsNullOrWhiteSpace(ItemDetailsHelper.getItemDetails(row.itemCatNo).custom3))
                {
                    taxCategory = ItemDetailsHelper.getItemDetails(row.itemCatNo).custom3;
                }

                if (perTaxCatTax_ValExcTax.ContainsKey(taxCategory))
                {
                    perTaxCatTax_ValExcTax[taxCategory] += valExcTax;
                    perTaxCatTax_Quantity[taxCategory] += itemQ;
                }
                else
                {
                    perTaxCatTax_ValExcTax[taxCategory] = valExcTax;
                    perTaxCatTax_Quantity[taxCategory] = itemQ;
                 }
            }

            taxSummary = new TaxSummary()
            {
                gValueExcTax = valExcTax17Perc,
                gQuantity = totalQ,
                totalValueAllItemIncludeTaxes = total17Perc,
                perTaxCatTax_ValExcTax = perTaxCatTax_ValExcTax,
                perTaxCatTax_Quantity = perTaxCatTax_Quantity
            };
        }

        Boolean ignoreNextEventForShowAllBillsCheckBox = false;
        private void checkBoxShowAllBills_CheckedChanged(object sender, EventArgs e)
        {


            if (ignoreNextEventForShowAllBillsCheckBox)
            {
                ignoreNextEventForShowAllBillsCheckBox = false;
                return;
            }

            if (isDataDirty)
            {
                var resp = MessageBox.Show("You have unsaved changed that will be lost. Are you sure you want to proceed?",
                    "", MessageBoxButtons.YesNo);
                if (resp.Equals(DialogResult.No))
                {
                    ignoreNextEventForShowAllBillsCheckBox = true;
                    checkBoxShowAllBills.Checked = !checkBoxShowAllBills.Checked;
                    return;
                }

            }

            int currentBillId = -1;

            if (this.dataGridView2.CurrentRow != null && this.dataGridView2.CurrentRow.Index >= 0)
            {
                currentBillId = int.Parse(this.dataGridView2.CurrentRow.Cells[0].Value.ToString());
            }

            if (checkBoxShowAllBills.Checked == true)
            {
                this.shopSalesInvoiceTableAdapter1.Fill(this.atoZDatabaseDataSet9.ShopSalesInvoice);
                this.dataGridView2.Sort(this.dataGridView2.Columns[0], ListSortDirection.Ascending);

                if (currentBillId != -1)
                {
                    HelperMethods.selectRowOnCol0(currentBillId, this.dataGridView2);
                }

                this.dataGridView2_CellClick(null, null);

                //this.shopSalesInvoiceDataGridView.Enabled = false;
                //this.shopSalesInvoiceBindingNavigatorSaveItem.Enabled = false;
                //this.buttonAddItems.Enabled = false;
                this.dataGridView1.Enabled = false;

            }
            else
            {

                UpdateBillListTable();

                if (currentBillId != -1)
                {
                    HelperMethods.selectRowOnCol0(currentBillId, this.dataGridView2);
                }

                dataGridView2_CellClick(null, null);

                //this.shopSalesInvoiceDataGridView.Enabled = true;
                //this.shopSalesInvoiceBindingNavigatorSaveItem.Enabled = true;
                //this.buttonAddItems.Enabled = true;
                this.dataGridView1.Enabled = true;
            }

            this.isDataDirty = false;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void FormShopSalesInvoiceDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isDataDirty)
            {
                var resp = MessageBox.Show("You have unsaved changed. Are you sure you want to close",
                    "", MessageBoxButtons.YesNo);
                if (resp.Equals(DialogResult.No))
                {
                    e.Cancel = true;
                }

            }
        }

        private void textBoxSalesInvoiceId_Enter(object sender, EventArgs e)
        {
            this.isDataDirty = true;
            this.textBoxSalesInvoiceId.BackColor = Color.Red;
        }

        private void textBoxTaxRegNo_Enter(object sender, EventArgs e)
        {
            this.isDataDirty = true;
            this.textBoxTaxRegNo.BackColor = Color.Red;
        }

        private void textBoxStatus_Enter(object sender, EventArgs e)
        {
            this.isDataDirty = true;
            this.textBoxStatus.BackColor = Color.Red;
        }

        private void dateTimePickerSalesInvoiceDate_ValueChanged(object sender, EventArgs e)
        {
            if(!((AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow)
                this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice.Rows[0]).invoiceDate.ToShortDateString()
                .Equals(dateTimePickerSalesInvoiceDate.Value.ToShortDateString()))
            {
                this.isDataDirty = true;
                this.dateTimePickerSalesInvoiceDate.Font = new Font(dateTimePickerSalesInvoiceDate.Font.FontFamily, 10.25F);
            }
        }

        private void dateTimePickerBillDate_ValueChanged(object sender, EventArgs e)
        {
            if (!((AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow)
                this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice.Rows[0]).billDate.ToShortDateString()
                .Equals(this.dateTimePickerBillDate.Value.ToShortDateString()))
            {
                this.isDataDirty = true;
                this.dateTimePickerBillDate.Font = new Font(dateTimePickerBillDate.Font.FontFamily, 10.25F);
            }
        }

        // function to add a duplication row. Triggered when user clicks the leftmost column
        private void shopSalesInvoiceDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // make sure column is leftmost
            if (e.ColumnIndex.Equals(-1) && e.RowIndex >= 0)
            {
                System.Text.StringBuilder messageBoxCS = new System.Text.StringBuilder();
                messageBoxCS.Append("Are you sure you want to duplicate ");
                messageBoxCS.AppendLine();
                messageBoxCS.AppendFormat("{0}", shopSalesInvoiceDataGridView[1, e.RowIndex].Value);
                messageBoxCS.Append("?");
                DialogResult dialogResult = MessageBox.Show(messageBoxCS.ToString(), "Confirming...", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    // go through all the rows in the datasource linked with the view
                    foreach (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow
                        dataRow in this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice.Rows)
                    {
                        // check if the item cat number is same as the one user selected. (there could be multiple matches, but we pick first)
                        if (dataRow.itemCatNo.Equals(shopSalesInvoiceDataGridView[0,e.RowIndex].Value))
                        {
                            // create a new row with the right schema
                            AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow newRow = 
                                this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice.NewRow() as AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow;
                            // save the unique row ID. It seemed to be -1, probabily if its -1 than a unique ID is assigned automatically
                            var rowId = newRow.rowId;
                            //MessageBox.Show(rowId.ToString());
                            // clone data from the row selected by user
                            newRow.ItemArray = dataRow.ItemArray.Clone() as object[];
                            // restore the unique ID
                            newRow.rowId = rowId;
                            // update the data source with the new row
                            this.atoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoice.AddShopSalesInvoiceRow(newRow);
                            
                            break;
                        }
                    }
                    // not sure if its needed, but just adding
                    HelperMethods.setRowNumbers(this.shopSalesInvoiceDataGridView);

                    // highlight row, next three lines copied from somewhere, not sure about exact working. Fixing row to last and column to 0
                    this.shopSalesInvoiceDataGridView.Rows[this.shopSalesInvoiceDataGridView.Rows.Count -1].Cells[0].Style.BackColor = Color.Red;
                    resetTaxInfo();
                    this.isDataDirty = true;
                }
            }   
        }

        public DataGridViewRow CloneWithValues(DataGridViewRow row)
        {
            DataGridViewRow clonedRow = (DataGridViewRow)row.Clone();
            for (Int32 index = 0; index < row.Cells.Count; index++)
            {
                clonedRow.Cells[index].Value = row.Cells[index].Value;
            }
            return clonedRow;
        }
    }

    public class TaxCalItemized
    {
        public double valueExcTax = 0;
        public double totalTax = 0;
        public double totalValueIncTax = 0;
        public double? kkTaxPaid = null;
        public double? kkTaxPerItem = null;
    }

    public class TaxSummary
    {
        public double hValueExcTax=0;
        public double hQuantity = 0;
        public double gValueExcTax=0;
        public double gQuantity = 0;
        public double kValueExcTax = 0;
        public double kQuantity = 0;
        public double k17 = 0;
        public double kActual=0;
        public double kExtra=0;
        public double totalValueAllItemIncludeTaxes=0;
        public Dictionary<String, double> perTaxCatTax_ValExcTax;
        public Dictionary<String, double> perTaxCatTax_Quantity;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtoZ.datasets;


namespace AtoZ
{
   
    class ClassUpdateInventory
    {
        AtoZ.datasets.AtoZDatabaseDataSet_ItemsTableAdapters.itemsTableAdapter adapter = new
            AtoZ.datasets.AtoZDatabaseDataSet_ItemsTableAdapters.itemsTableAdapter();
        AtoZDatabaseDataSet_Items itemsDataset = new AtoZDatabaseDataSet_Items();
        ClassUpdateShopInventory updateShopInventory = new ClassUpdateShopInventory();

        public void addInventory(AtoZDatabaseDataSet_DeliveryChallan deliveryChallan)
        {
            Boolean add = true;

            AddnSubInventory(DataSetToDictionary(deliveryChallan), add);

            // remove this inventory from shop
            updateShopInventory.removeInventory(deliveryChallan);
        }

        public void removeInventory(AtoZDatabaseDataSet_DeliveryChallan deliveryChallan)
        {
            Boolean add = false;

            AddnSubInventory(DataSetToDictionary(deliveryChallan), add);

            // add inventory to shop
            updateShopInventory.addInventory(deliveryChallan);
        }


        public void addInventory(AtoZDatabaseDataSet_ShipmentReceived shipmentReceived)
        {
            Boolean add = true;

            AddnSubInventory( DataSetToDictionary(shipmentReceived), add);

        }

        public void removeInventory(AtoZDatabaseDataSet_ShipmentReceived shipmentReceived)
        {
            Boolean add = false;

            AddnSubInventory(DataSetToDictionary(shipmentReceived), add);
        }


        private void AddnSubInventory( Dictionary<int, int> itemCounts, Boolean add)
        {
            this.itemsDataset.Clear();
            adapter.Fill(itemsDataset.items);
            foreach (AtoZDatabaseDataSet_Items.itemsRow row in itemsDataset.items.Rows)
            {
                if (itemCounts.ContainsKey(row.catNo))
                {
                    if(row.IsstockSizeNull())
                    {
                        row.stockSize = 0;
                    }

                    if (add)
                    {
                        row.stockSize += itemCounts[row.catNo];
                    }
                    else
                    {
                        row.stockSize -= itemCounts[row.catNo];
                    }
                }
            }

            adapter.Update(itemsDataset);
            itemsDataset.AcceptChanges();
        }

        private static Dictionary<int, int> DataSetToDictionary(AtoZDatabaseDataSet_ShipmentReceived shipmentReceived)
        {
            Dictionary<int, int> itemCounts = new Dictionary<int, int>();
            foreach (AtoZDatabaseDataSet_ShipmentReceived.ShipmentReceivedRow row in shipmentReceived.ShipmentReceived.Rows)
            {
                itemCounts.Add(row.itemId, (int)row.itemCount);
            }
            return itemCounts;
        }

        private static Dictionary<int, int> DataSetToDictionary(AtoZDatabaseDataSet_DeliveryChallan deliveryChallanDataset)
        {
            Dictionary<int, int> itemCounts = new Dictionary<int, int>();
            foreach (AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow row in deliveryChallanDataset.DeliveryChallan.Rows)
            {
                itemCounts.Add(row.itemCatNo, (int)row.itemQuantity);
            }
            return itemCounts;
        }

    }
}

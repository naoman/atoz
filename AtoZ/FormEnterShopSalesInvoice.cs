﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public enum RecordStatus
    {
        Unmerged = 0,
        Merged = 1
    }
    public partial class FormEnterShopSalesInvoice : Form
    {
        private const int INDEX_QUANTITY = 0; //update Rows.Add manually
        private const int INDEX_ITEM_ID = 1;
        private const int INDEX_ITEM_NAME = 2;
        private const int INDEX_BALANCE = 3;
        private const int INDEX_PRICE = 4;
        private const int INDEX_TAX = 5;
        private const int INDEX_ACTUAL_TAX = 6;

        public Boolean postTaxShops = false;

        public FormEnterShopSalesInvoice()
        {
            InitializeComponent();

           
        }

        private void FormEnterShopSalesInvoice_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet7.shops' table. You can move, or remove it, as needed.
            //replaced by logic below this.shopsTableAdapter.Fill(this.atoZDatabaseDataSet10.shops);

            if (postTaxShops)
            {
                this.textBoxSalesTaxRegNo.Text = "07 - 02 - 5111 - 002 - 28";
                this.shopsTableAdapter.GetPostTaxShops(this.atoZDatabaseDataSet10.shops);
            }
            else
            {
                this.shopsTableAdapter.GetRegularShops(this.atoZDatabaseDataSet10.shops);
            }

            
            //make sure user selects the shop explicitly selectCurrentShopFromList();

            this.textBoxBillId.Text = getNextBillId() + "";
            this.dateTimePicker1.Value = getLasShopSalesInvoiceDate();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selectCurrentShopFromList();
        }

        private void selectCurrentShopFromList()
        {
            try
            {
                if (this.labelShopId.Text.Equals("none"))
                {
                    InitializeItemsWithZeroCount();
                }

                int shopId = (int)dataGridView1.CurrentRow.Cells[0].Value;
                this.labelShopName.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                this.labelShopId.Text = shopId.ToString();
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dataGridView1_CellContentClick(sender, null);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1_CellContentClick(sender, null);
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
         //   dataGridView1_CellContentClick(sender, null);
        }

        private void buttonImportData_Click(object sender, EventArgs e)
        {
            FormImportShopSalesInvoice form = new FormImportShopSalesInvoice();
            form.itemsGridView = this.dataGridViewItems;
            form.Show();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if(labelShopId.Text.Equals("none"))
            {
                MessageBox.Show("Please select a shop");
                return;
            }
            if(string.IsNullOrEmpty(textBoxInvoiceId.Text))
            {
                MessageBox.Show("Please enter Terms of Sales");
                return;
            }
            try
            {
                AtoZDatabaseDataSet_ShopSalesInvoice dataSet = new AtoZDatabaseDataSet_ShopSalesInvoice();
                AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapter =
                    new AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();

                int rowCount = 0;

                foreach (DataGridViewRow row in this.dataGridViewItems.Rows)
                {
                    try
                    {
                        if (row.Cells != null && row.Cells[INDEX_ITEM_ID] != null 
                            && row.Cells[INDEX_ITEM_ID].Value != null
                            && !string.IsNullOrEmpty(row.Cells[INDEX_ITEM_ID].Value.ToString()) 
                            && double.Parse(row.Cells[INDEX_QUANTITY].Value.ToString()) > 0)
                        {
                            if (string.IsNullOrWhiteSpace(row.Cells[INDEX_PRICE].Value.ToString()))
                            {
                                MessageBox.Show("please provide price for the item " + row.Cells[INDEX_ITEM_ID].Value);
                                return;
                            }
                            AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow newRow =
                                   dataSet.ShopSalesInvoice.NewShopSalesInvoiceRow();
                            newRow.invoiceDate = this.dateTimePicker1.Value;
                            newRow.invoiceId = this.textBoxInvoiceId.Text;
                            newRow.shopId = int.Parse(this.labelShopId.Text);
                            //newRow.shopName = this.labelShopName.Text;
                            newRow.itemCatNo = int.Parse(row.Cells[INDEX_ITEM_ID].Value.ToString());
                            newRow.itemName = row.Cells[INDEX_ITEM_NAME].Value.ToString();
                            newRow.itemQuantity = double.Parse(row.Cells[INDEX_QUANTITY].Value.ToString());
                            newRow.status = RecordStatus.Unmerged.ToString();
                            newRow.itemPrice = double.Parse(row.Cells[INDEX_PRICE].Value.ToString());
                            newRow.itemTax = double.Parse(row.Cells[INDEX_TAX].Value.ToString());
                            
                            if (row.Cells[INDEX_BALANCE].Value != null && !string.IsNullOrEmpty(row.Cells[INDEX_BALANCE].Value.ToString()))
                            {
                                newRow.itemBalance = (int)float.Parse(row.Cells[INDEX_BALANCE].Value.ToString());
                            }
                            
                            if(row.Cells[INDEX_ACTUAL_TAX].Value != null && !string.IsNullOrWhiteSpace(row.Cells[INDEX_ACTUAL_TAX].Value.ToString()))
                            {
                                newRow.custom1 = double.Parse(row.Cells[INDEX_ACTUAL_TAX].Value.ToString()).ToString("0.00");
                            }
                            
                            newRow.billId = int.Parse(this.textBoxBillId.Text);
                            newRow.billDate = this.dateTimePicker2.Value;
                            if(!string.IsNullOrWhiteSpace(this.textBoxSalesTaxRegNo.Text))
                            {
                                newRow.salesTaxRegNo = this.textBoxSalesTaxRegNo.Text;
                            }
                            dataSet.ShopSalesInvoice.AddShopSalesInvoiceRow(newRow);
                            rowCount++;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Got Exception for row number: " + (row.Index + 1) + " " + ex.Message);
                        return;
                    }
                    
                }

                String msgString = "Are you sure you want to create a Sales Invoice with fillowing info:\n" +
                    "Shop Name: " + this.labelShopName.Text + "\n" +
                    "Bill ID: " + this.textBoxBillId.Text + "\n" +
                    "Item Count: " + rowCount;

                var resp = MessageBox.Show(msgString, "", MessageBoxButtons.YesNo);
                
                if(resp.Equals(DialogResult.No))
                {
                    return;
                }

                int count = tableAdapter.Update(dataSet);
                Console.WriteLine("Rows added " + count);
                Console.WriteLine("DataSet Rows " + dataSet.ShopSalesInvoice.Rows.Count);
                dataSet.ShopSalesInvoice.AcceptChanges();

                // update shop inventory
                ClassUpdateShopInventory updateShopInventory = new ClassUpdateShopInventory();
                updateShopInventory.removeInventory(dataSet);

                MessageBox.Show(count + " Rows added");
                this.Close();

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

        }

        private void dataGridViewItems_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            HelperMethods.setRowNumbers(this.dataGridViewItems);
        }

        private void dataGridViewItems_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            HelperMethods.setRowNumbers(this.dataGridViewItems);
        }

        private void dataGridViewItems_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            HelperMethods.setRowNumbers(this.dataGridViewItems);
        }


        private void InitializeItemsWithZeroCount()
        {
            int shopId = (int)dataGridView1.CurrentRow.Cells[0].Value;

            Dictionary<int, ItemDetailsData> items;
            if(ShopsDetailHelper.isPostTaxShop(shopId))
            {
                items = ItemDetailsHelper.getAllPostTaxItems();
            }
            else
            {
                items = ItemDetailsHelper.getAllItems();
            }

            foreach (var kvp in items)
            {
                this.dataGridViewItems.Rows.Add("0",kvp.Key, kvp.Value.name, "", kvp.Value.price, kvp.Value.tax,kvp.Value.actualTax);
            }
            this.dataGridViewItems.PerformLayout();
        }

        private void dataGridViewItems_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

     

        private static void SetItemPriceAndTax(DataGridViewRow row)
        {
            try
            {
                if (row.Cells != null && row.Cells[0] != null &&
                    row.Cells[0].Value != null && !string.IsNullOrEmpty(row.Cells[0].Value.ToString()) &&
                    (int)float.Parse(row.Cells[2].Value.ToString()) > 0)
                {
                    int itemId = int.Parse(row.Cells[0].Value.ToString());
                    double itemPrice = ItemDetailsHelper.getItemDetails(itemId).price;
                    row.Cells[4].Value = itemPrice.ToString("0.00");

                    double itemTax = ItemDetailsHelper.getItemDetails(itemId).tax;
                    row.Cells[5].Value = itemTax.ToString("0.00");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            selectCurrentShopFromList();
        }

        private void dataGridView1_KeyUp(object sender, KeyEventArgs e)
        {
            selectCurrentShopFromList();
        }

        private int getNextBillId()
        {
            try
            {
                AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapter =
                     new AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();
                return (int)tableAdapter.GetMaxBillId() + 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("unable to get last Max bill ID. Setting bill ID to 1 for now. " + ex.Message);
            }
            return 1;
            
        }

        private DateTime getLasShopSalesInvoiceDate()
        {
            try
            {
                AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapter =
                     new AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();
                return (DateTime)tableAdapter.GetLasShopSalesInvoiceDate();
            }
            catch (Exception ex)
            {
                MessageBox.Show("unable to get last Max bill ID. Setting bill ID to 1 for now. " + ex.Message);
            }
            return DateTime.Now;

        }

        private void dataGridViewItems_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            
            
            if (e.RowIndex >= 0 && (e.ColumnIndex == INDEX_QUANTITY || e.ColumnIndex ==INDEX_PRICE || e.ColumnIndex ==INDEX_TAX))
            {
                if((dataGridViewItems.Rows[e.RowIndex].Cells[e.ColumnIndex] == null) ||
                    (dataGridViewItems.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null) ||
                    (string.IsNullOrWhiteSpace(dataGridViewItems.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()))
                    )
                {
                    MessageBox.Show("cell value is empty");
                }
            }
             
        }
    }
}

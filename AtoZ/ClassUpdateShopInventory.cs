﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtoZ.datasets;


namespace AtoZ
{
    class ClassUpdateShopInventory
    {
        AtoZ.datasets.AtoZDatabaseDataSet_ShopStatusTableAdapters.shopStatusTableAdapter adapter = new
         AtoZ.datasets.AtoZDatabaseDataSet_ShopStatusTableAdapters.shopStatusTableAdapter();

        AtoZDatabaseDataSet_ShopStatus shopStatusDataset = new AtoZDatabaseDataSet_ShopStatus();

        public void addInventory(AtoZDatabaseDataSet_DeliveryChallan deliveryChallan)
        {
            Boolean add = true;
            AddnSubInventory(getShopId(deliveryChallan),DataSetToDictionary(deliveryChallan), add);
        }

        private static int getShopId(AtoZDatabaseDataSet_DeliveryChallan deliveryChallan)
        {
            int shopId = ((AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow)
                deliveryChallan.DeliveryChallan.Rows[0]).shopId;
            return shopId;
        }

        public void removeInventory(AtoZDatabaseDataSet_DeliveryChallan deliveryChallan)
        {
            Boolean add = false;

            AddnSubInventory(getShopId(deliveryChallan),DataSetToDictionary(deliveryChallan), add);
        }


        public void addInventory(AtoZDatabaseDataSet_ShopSalesInvoice salesInvoice)
        {
            Boolean add = true;
            AddnSubInventory(getShopId(salesInvoice),DataSetToDictionary(salesInvoice), add);

        }

        private static int getShopId(AtoZDatabaseDataSet_ShopSalesInvoice salesInvoice)
        {
            int shopId = ((AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow)
                salesInvoice.ShopSalesInvoice.Rows[0]).shopId;
            return shopId;
        }

        public void removeInventory(AtoZDatabaseDataSet_ShopSalesInvoice salesInvoice)
        {
            Boolean add = false;

            AddnSubInventory(getShopId(salesInvoice),DataSetToDictionary(salesInvoice), add);
        }


        private void AddnSubInventory(int shopId, Dictionary<int, int> itemCounts, Boolean add)
        {
            /* Working code. disabled for not. 
             * 
             * 
            this.shopStatusDataset.Clear();
            adapter.FillByShopId(this.shopStatusDataset.shopStatus,shopId);

            Dictionary<int, AtoZDatabaseDataSet_ShopStatus.shopStatusRow> shopStatusDict =
                new Dictionary<int, AtoZDatabaseDataSet_ShopStatus.shopStatusRow>();

            foreach (AtoZDatabaseDataSet_ShopStatus.shopStatusRow row in this.shopStatusDataset.shopStatus.Rows)
            {
                shopStatusDict.Add(row.itemId, row);
            }

            foreach(var item in itemCounts)
            { 
                if(!shopStatusDict.ContainsKey(item.Key))
                {
                    AtoZDatabaseDataSet_ShopStatus.shopStatusRow newRow = 
                        (AtoZDatabaseDataSet_ShopStatus.shopStatusRow)this.shopStatusDataset.shopStatus.NewRow();
                    newRow.shopId = shopId;
                    newRow.itemId = item.Key;
                    newRow.balance = 0;

                    this.shopStatusDataset.shopStatus.AddshopStatusRow(newRow);
                    shopStatusDict.Add(item.Key, newRow);
                }
                
                if (add)
                {
                    shopStatusDict[item.Key].balance += item.Value;
                }
                else
                {
                    shopStatusDict[item.Key].balance -= item.Value;
                }
            }

            adapter.Update(shopStatusDataset);
            shopStatusDataset.AcceptChanges();
             */ 
        }

        private static Dictionary<int, int> DataSetToDictionary(AtoZDatabaseDataSet_ShopSalesInvoice salesInvoice)
        {
            Dictionary<int, int> itemCounts = new Dictionary<int, int>();
            foreach (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow row in salesInvoice.ShopSalesInvoice.Rows)
            {
                if (itemCounts.ContainsKey(row.itemCatNo))
                {
                    int oldValue;
                    itemCounts.TryGetValue(row.itemCatNo, out oldValue);
                    itemCounts.Remove(row.itemCatNo);
                    itemCounts.Add(row.itemCatNo, (int)row.itemQuantity + oldValue);
                }
                else
                {
                    itemCounts.Add(row.itemCatNo, (int)row.itemQuantity);
                }
            }
            return itemCounts;
        }

        private static Dictionary<int, int> DataSetToDictionary(AtoZDatabaseDataSet_DeliveryChallan deliveryChallanDataset)
        {
            Dictionary<int, int> itemCounts = new Dictionary<int, int>();
            foreach (AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow row in deliveryChallanDataset.DeliveryChallan.Rows)
            {
                itemCounts.Add(row.itemCatNo, (int)row.itemQuantity);
            }
            return itemCounts;
        }

    }
}

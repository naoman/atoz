﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtoZ
{
    class ShopsDetailHelper
    {
        public const string CUSTOM_1_POST_TAX = "POST_TAX";

        private static Dictionary<int, ShopsDetailsData> shopsHashSet = null;
        public static void refreshData()
        {
            AtoZDatabaseDataSet_Shops dataSet = new AtoZDatabaseDataSet_Shops();
            AtoZ.datasets.AtoZDatabaseDataSet_ShopsTableAdapters.shopsTableAdapter tableAdapter =
                new AtoZ.datasets.AtoZDatabaseDataSet_ShopsTableAdapters.shopsTableAdapter();


            int count = tableAdapter.Fill(dataSet.shops);

            shopsHashSet = new Dictionary<int, ShopsDetailsData>();
            foreach (var shop in dataSet.shops)
            {
                shopsHashSet.Add(shop.Id, new ShopsDetailsData() { 
                    name = shop.name, 
                    custom1 = shop.Iscustom1Null()?"":shop.custom1 });
            }
            return;
        }

        public static string getShopName(int shopId)
        {
            checkAndRefresh();
            return shopsHashSet[shopId].name;
        }

        public static ShopsDetailsData getShopDetails(int shopId)
        {
            checkAndRefresh();
            return shopsHashSet[shopId];
        }

        public static bool isPostTaxShop(int shopId)
        {
            checkAndRefresh();

            return shopsHashSet[shopId].custom1.Equals(ShopsDetailHelper.CUSTOM_1_POST_TAX);
        }

        private static void checkAndRefresh()
        {
            if (shopsHashSet == null)
            {
                refreshData();
            }
        }

        public static Dictionary<int, ShopsDetailsData>  getShopsHashset()
        {
            checkAndRefresh();
            return shopsHashSet;
        }
    }

    class ShopsDetailsData
    {
        public string name;
        public string custom1;
    }
}

﻿using AtoZ.datasets;
using AtoZ.reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace AtoZ
{
    public partial class FormTaxSummary : Form
    {
        public FormTaxSummary()
        {
            InitializeComponent();
        }

        List<TaxReportDataClass> taxReportDataClassList;
        private void FormTaxSummary_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'atoZDatabaseDataSetShop_IdName.shops' table. You can move, or remove it, as needed.
            this.shopsTableAdapter.Fill(this.atoZDatabaseDataSetShop_IdName.shops);
            // TODO: This line of code loads data into the 'atoZDatabaseDataSet14.ShopSalesInvoice' table. You can move, or remove it, as needed.
            this.shopSalesInvoiceTableAdapter.Fill(this.atoZDatabaseDataSet14.ShopSalesInvoice);
            //this.
            this.radioButtonNone.Checked = true;

        }

        private void buttonFilter_Click(object sender, EventArgs e)
        {
            this.shopSalesInvoiceTableAdapter.FillByDate(this.atoZDatabaseDataSet14.ShopSalesInvoice,
                this.dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                this.dateTimePicker2.Value.ToString("yyyy-MM-dd HH:mm:ss"));

            this.dataGridView2.Rows.Clear();
            taxReportDataClassList = new List<TaxReportDataClass>();

            foreach(AtoZDatabaseDataSet14.ShopSalesInvoiceRow row in this.atoZDatabaseDataSet14.ShopSalesInvoice)
            {
                int? billId = row.billId;
                AtoZDatabaseDataSet_ShopSalesInvoice dataSet = new AtoZDatabaseDataSet_ShopSalesInvoice();
                AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapter = new AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();
                tableAdapter.FillBy(dataSet.ShopSalesInvoice, billId);

                TaxSummary taxSummary;
                Dictionary<int,TaxCalItemized> taxItemizedList;

                FormShopSalesInvoiceDetails.calculateTax(dataSet, row.shopId, out taxSummary, out taxItemizedList);

                this.dataGridView2.Rows.Add(billId, row.billDate.ToString("dd/MM/yyyy"), 
                    taxSummary.gValueExcTax.ToString("0.00"), taxSummary.gQuantity.ToString("0"),
                    taxSummary.hValueExcTax.ToString("0.00"), taxSummary.hQuantity.ToString("0"),
                    taxSummary.kValueExcTax.ToString("0.00"), taxSummary.kQuantity.ToString("0"),
                    taxSummary.kExtra.ToString("0.00"),ShopsDetailHelper.getShopName(row.shopId));
                
                String itemDescriptionString = "";
                if(taxSummary.gValueExcTax > 0)
                {
                    itemDescriptionString += "G ";
                }
                if(taxSummary.hValueExcTax > 0)
                {
                    itemDescriptionString += "H ";
                }
                if(taxSummary.kValueExcTax > 0)
                {
                    itemDescriptionString += "3rd";
                }
                TaxReportDataClass taxReportDataClass = new TaxReportDataClass()
                {
                    taxSummary = taxSummary,
                    shopName = ShopsDetailHelper.getShopName(row.shopId),
                    billDate = row.billDate,
                    billId = row.billId,
                    itemsDescription = itemDescriptionString
                };
                taxReportDataClassList.Add(taxReportDataClass);
            }
            this.dataGridView2.PerformLayout();
            
            
            HelperMethods.setRowNumbers(this.dataGridView1);
            HelperMethods.setRowNumbers(this.dataGridView2);

            updateTaxCatDataGrid();
        }

        private void updateTaxCatDataGrid()
        {
            HashSet<string> taxCats = new HashSet<string>();
            foreach(var v in taxReportDataClassList)
            {
                foreach(var v2 in v.taxSummary.perTaxCatTax_ValExcTax)
                {
                    if(!taxCats.Contains(v2.Key))
                    {
                        taxCats.Add(v2.Key);
                    }
                }
            }

            this.dataGridViewTaxCat.Rows.Clear();
            foreach(var s in taxCats)
            {
                this.dataGridViewTaxCat.Rows.Add(s);
            }
        }
        private void buttonPrint_Click(object sender, EventArgs e)
        {
            FormTaxReport taxReport = new FormTaxReport();
            taxReport.setSource(taxReportDataClassList);
            taxReport.Show();
        }

        private void buttonPrint2_Click(object sender, EventArgs e)
        {
            FormTaxReport2WithTaxCats taxReport = new FormTaxReport2WithTaxCats();
            taxReport.setSource(taxReportDataClassList);

            // compute totals for all tax cats across all bills
            String taxInfoTotaledStr = "";
            Dictionary<String, double> total = new Dictionary<string, double>();
            foreach(var taxReportData in taxReportDataClassList)
            {
                foreach (var kvp in taxReportData.taxSummary.perTaxCatTax_ValExcTax)
                {
                    if (total.ContainsKey(kvp.Key))
                    {
                        total[kvp.Key] += kvp.Value;
                    }
                    else
                    {
                        total[kvp.Key] = kvp.Value;
                    }
                }
            }

            foreach (var kvp in total)
            {
                taxInfoTotaledStr += kvp.Key + " = " + kvp.Value.ToString("0.00") + " , ";
            }

            taxReport.setTotalTaxInfoString(taxInfoTotaledStr);

            taxReport.Show();
        }

        private void radioButtonNone_CheckedChanged(object sender, EventArgs e)
        {
            this.dataGridView2.Visible = true;
            this.dataGridViewSelectOne.Visible = false;
        }

        private void radioButtonGeneral_CheckedChanged(object sender, EventArgs e)
        {
            this.dataGridView2.Visible = false;
            this.dataGridViewSelectOne.Visible = true;

            int requiredColumn = 2;
            UpdateSelectOneTable(requiredColumn);
        }

        private void UpdateSelectOneTable(int requiredColumn)
        {
            this.dataGridViewSelectOne.Rows.Clear();
            foreach (DataGridViewRow row in this.dataGridView2.Rows)
            {
                if (double.Parse(row.Cells[requiredColumn].Value.ToString()) != 0)
                {
                    this.dataGridViewSelectOne.Rows.Add(row.Cells[0].Value, row.Cells[1].Value, row.Cells[requiredColumn].Value,row.Cells[6].Value);
                }
            }

            HelperMethods.setRowNumbers(this.dataGridViewSelectOne);
        }

        private void radioButtonHosiery_CheckedChanged(object sender, EventArgs e)
        {
            this.dataGridView2.Visible = false;
            this.dataGridViewSelectOne.Visible = true;

            int requiredColumn = 3;
            UpdateSelectOneTable(requiredColumn);
        }

        private void radioButtonKalaKola_CheckedChanged(object sender, EventArgs e)
        {
            this.dataGridView2.Visible = false;
            this.dataGridViewSelectOne.Visible = true;

            int requiredColumn = 4;
            UpdateSelectOneTable(requiredColumn);
        }

        private void radioButtonExtraTax_CheckedChanged(object sender, EventArgs e)
        {
            this.dataGridView2.Visible = false;
            this.dataGridViewSelectOne.Visible = true;

            int requiredColumn = 5;
            UpdateSelectOneTable(requiredColumn);
        }

        private void buttonDeliveryReport_Click(object sender, EventArgs e)
        {
            Dictionary<DateTime, DeliveryReportDataClass> list = new  Dictionary<DateTime,DeliveryReportDataClass>();
            AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter tableAdapter = new 
                AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();

            AtoZDatabaseDataSet_DeliveryChallan dataset = new AtoZDatabaseDataSet_DeliveryChallan();
            tableAdapter.FillByDateRange(dataset.DeliveryChallan,
                this.dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                this.dateTimePicker2.Value.ToString("yyyy-MM-dd HH:mm:ss"));

            foreach (AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow row in dataset.DeliveryChallan)
            {
                if(!list.ContainsKey(row.challanDate))
                {
                    list.Add(row.challanDate,new DeliveryReportDataClass(){ date = row.challanDate});
                }

                DeliveryReportDataClass data = list[row.challanDate];
                data.noOfItems += (int)row.itemQuantity;
                data.noOfItemTypes ++;
                data.valueOfItems += (int) (row.itemQuantity * ItemDetailsHelper.getItemDetails(row.itemCatNo).price);
                if(!data.challanIdsList.Contains(row.challanId))
                {
                    data.challanIdsList.Add(row.challanId);
                }
                
            }
            
            FormReportDailyDeliveries form = new FormReportDailyDeliveries();
            form.setDataSource(list.Values.ToList());
            form.Show();
        }

        private void buttonMonthlyDeliveryReport_Click(object sender, EventArgs e)
        {
            // get delivery Challan data
            Dictionary<DateTime, MonthlyDeliveryAndSalesData> listDelivery = getMonthlyDeliveryData(false);

            // get bills data
            Dictionary<DateTime, MonthlyDeliveryAndSalesData> listBills = getMonthlySalesData(false);

            List<MonthlyDeliveryAndSalesData> finalList = listDelivery.Values.ToList();
            
            FormReportMonthlyDeliveryAndSales form = new FormReportMonthlyDeliveryAndSales();
            form.setDateSource(finalList.Concat(listBills.Values.ToList()).ToList<MonthlyDeliveryAndSalesData>());
            form.Show();
        }

        private static Dictionary<DateTime, MonthlyDeliveryAndSalesData> getMonthlySalesData(Boolean valueOrCount)
        {
            Dictionary<DateTime, MonthlyDeliveryAndSalesData> listBills = new Dictionary<DateTime, MonthlyDeliveryAndSalesData>();
            AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapterBills = new
                 datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();

            AtoZDatabaseDataSet_ShopSalesInvoice datasetBills = new AtoZDatabaseDataSet_ShopSalesInvoice();
            tableAdapterBills.Fill(datasetBills.ShopSalesInvoice);

            foreach (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow row in datasetBills.ShopSalesInvoice)
            {
                DateTime month = new DateTime(row.billDate.Year, row.billDate.Month, 1); // keep day fixed to get monthly stats
                if (!listBills.ContainsKey(month))
                {
                    listBills.Add(month, new MonthlyDeliveryAndSalesData() { date = month, type = "sales" });
                }

                MonthlyDeliveryAndSalesData data = listBills[month];
                
                if (valueOrCount)
                {
                    data.count += (int)(row.itemQuantity * row.itemPrice);
                }
                else
                {
                    if (!data.challanOrBillIdList.Contains(row.billId))
                    {
                        data.challanOrBillIdList.Add(row.billId);
                        data.count++;
                    }
                }

            }
            return listBills;
        }

        private static Dictionary<DateTime, MonthlyDeliveryAndSalesData> getMonthlyDeliveryData(Boolean valueOrCount)
        {
            Dictionary<DateTime, MonthlyDeliveryAndSalesData> list = new Dictionary<DateTime, MonthlyDeliveryAndSalesData>();
            AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter tableAdapter = new
                AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();

            AtoZDatabaseDataSet_DeliveryChallan dataset = new AtoZDatabaseDataSet_DeliveryChallan();
            tableAdapter.Fill(dataset.DeliveryChallan);

            foreach (AtoZDatabaseDataSet_DeliveryChallan.DeliveryChallanRow row in dataset.DeliveryChallan)
            {
                DateTime month = new DateTime(row.challanDate.Year, row.challanDate.Month, 1); // keep day fixed to get monthly stats
                if (!list.ContainsKey(month))
                {
                    list.Add(month, new MonthlyDeliveryAndSalesData() { date = month, type = "delivery" });
                }

                MonthlyDeliveryAndSalesData data = list[month];

                if (valueOrCount)
                {
                    data.count += (int)(row.itemQuantity * ItemDetailsHelper.getItemDetails(row.itemCatNo).price);
                }
                else
                {
                    if (!data.challanOrBillIdList.Contains(row.challanId))
                    {
                        data.challanOrBillIdList.Add(row.challanId);
                        data.count++;
                    }
                }

            }
            return list;
        }

        private void buttonMonthlyInAndOut_Click(object sender, EventArgs e)
        {
            // get delivery Challan data
            Dictionary<DateTime, MonthlyDeliveryAndSalesData> listDelivery = getMonthlyDeliveryData(true);

            // get bills data
            Dictionary<DateTime, MonthlyDeliveryAndSalesData> listBills = getMonthlySalesData(true);

            List<MonthlyDeliveryAndSalesData> finalList = listDelivery.Values.ToList();

            FormReportMonthlyDeliveryAndSales form = new FormReportMonthlyDeliveryAndSales();
            form.setDateSource(finalList.Concat(listBills.Values.ToList()).ToList<MonthlyDeliveryAndSalesData>());
            form.Show();
        }

        private void buttonTopShopsBySale_Click(object sender, EventArgs e)
        {
            List<MonthlyShopSaleData> sortedList = ClassDemandCalculation.GetSortedShopListBySale(this.dateTimePicker1.Value, this.dateTimePicker2.Value,true);


            FormReportMonthlyShopSale form = new FormReportMonthlyShopSale();
            form.setTitle("Shop Sale Range:  " + this.dateTimePicker1.Value.ToString("dd-MM-yyy") +
                "  " +  this.dateTimePicker2.Value.ToString("dd-MM-yyy"));
            // set month count with shop name
            sortedList.ForEach(o => o.shopName = "(" + o.noOfMonths + ") " + o.shopName);
            form.setDataSource(sortedList);
            form.Show();

        }

       

        private void buttonTopItemsBySale_Click(object sender, EventArgs e)
        {
            Dictionary<int, MonthlyItemSaleData> itemIdSaleDataHashSet = new Dictionary<int, MonthlyItemSaleData>();

            AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter tableAdapterBills = new
                datasets.AtoZDatabaseDataSet_ShopSalesInvoiceTableAdapters.ShopSalesInvoiceTableAdapter();

            AtoZDatabaseDataSet_ShopSalesInvoice datasetBills = new AtoZDatabaseDataSet_ShopSalesInvoice();
            tableAdapterBills.FillByDateRange(datasetBills.ShopSalesInvoice,
                this.dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                this.dateTimePicker2.Value.ToString("yyyy-MM-dd HH:mm:ss"));

            int shopId = int.Parse(this.dataGridViewShops.CurrentRow.Cells[0].Value.ToString());
            String shopName = this.dataGridViewShops.CurrentRow.Cells[1].Value.ToString();
            foreach (AtoZDatabaseDataSet_ShopSalesInvoice.ShopSalesInvoiceRow row in datasetBills.ShopSalesInvoice)
            {
                if (row.shopId == shopId)
                {
                    if (!itemIdSaleDataHashSet.ContainsKey(row.itemCatNo))
                    {
                        itemIdSaleDataHashSet.Add(row.itemCatNo, new MonthlyItemSaleData()
                            {
                                itemCatNo = row.itemCatNo,
                                itemName = ItemDetailsHelper.getItemName(row.itemCatNo),
                                itemCount = 0,
                                itemValue = 0
                            });

                    }

                    MonthlyItemSaleData data = itemIdSaleDataHashSet[row.itemCatNo];

                    data.itemValue += (int)(row.itemQuantity * row.itemPrice);
                    data.itemCount += (int)row.itemQuantity;
                }
            }

            List<MonthlyItemSaleData> sortedList = itemIdSaleDataHashSet.Values.OrderByDescending(o => o.itemValue).ToList<MonthlyItemSaleData>();
            //List<MonthlyItemSaleData> sortedList = itemIdSaleDataHashSet.Values.OrderByDescending(o => o.itemCatNo).ToList<MonthlyItemSaleData>();

            int totalSale = 0;

            foreach (MonthlyItemSaleData i in sortedList)
            {
                totalSale += i.itemValue;
            }

            // calcualte percentage and running percentage

            double lastRunningPercentage = 0;
            foreach (MonthlyItemSaleData i in sortedList)
            {
                i.percentOfTotal = (((double)i.itemValue) / totalSale) * 100;
                i.runningPercentOfTotal = lastRunningPercentage + i.percentOfTotal;
                lastRunningPercentage = i.runningPercentOfTotal;
            }

            FormReportMonthlyItemSale form = new FormReportMonthlyItemSale();
            form.setReportTitle(shopName +": " + this.dateTimePicker1.Value.ToString("dd-MM-yy") +
                "  " + this.dateTimePicker2.Value.ToString("dd-MM-yy"));
            form.setDataSource(sortedList);
            form.Show();
        }

        private void buttonComputeDemand_Click(object sender, EventArgs e)
        {
            

        }

        private void buttonVisualizeData_Click(object sender, EventArgs e)
        {
            ClassDemandCalculation demandCalculator = new ClassDemandCalculation();
            //var itemSized = demandCalculator.getItemDeliveryCounts(this.dateTimePicker1.Value, this.dateTimePicker2.Value);
            var itemsData = demandCalculator.getItemSaleDataHashSet(this.dateTimePicker1.Value, this.dateTimePicker2.Value);
            var shopsData = demandCalculator.getShopSaleDataForAllShops(this.dateTimePicker1.Value, this.dateTimePicker2.Value);

            FormChart formChart = new FormChart();

            // convert data for chart
            Dictionary<string, ChartDataDetails> data = new Dictionary<string, ChartDataDetails>();
            foreach (var itemData in itemsData)
            {
                ChartDataDetails dataDetails = new ChartDataDetails();
                dataDetails.dataPoints = itemData.Value.monthlySales.Select(i => new DataPoint(shopsData[i.Key].totalSale, i.Value.saleRatio)).ToList();
                dataDetails.average = itemData.Value.average;
                dataDetails.sd = itemData.Value.standartDeviation;
                data.Add(itemData.Key.ToString(), dataDetails);
            }

            formChart.setDataSource(data);
            formChart.Show();
        }

        private void dataGridViewTaxCat_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string selectedCat = dataGridViewTaxCat.CurrentRow.Cells[0].Value.ToString();

            this.dataGridView2.Visible = false;
            this.dataGridViewSelectOne.Visible = true;

            this.dataGridViewSelectOne.Rows.Clear();
            foreach(var v in taxReportDataClassList)
            {
                if(v.taxSummary.perTaxCatTax_ValExcTax.ContainsKey(selectedCat))
                {
                    this.dataGridViewSelectOne.Rows.Add(
                        v.billId, v.billDate.ToString("dd/MM/yyyy"),
                        v.taxSummary.perTaxCatTax_ValExcTax[selectedCat].ToString("0.00"),
                        v.taxSummary.perTaxCatTax_Quantity[selectedCat].ToString("0"),
                        v.shopName);
                }
            }
            HelperMethods.setRowNumbers(this.dataGridViewSelectOne);

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridViewTaxCat_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


    }

}

﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public enum ShipmentReceivedFormMode
    {
        ENTER_NEW,
        EDIT
    }
    public partial class FormEnterShipmentReceived : Form
    {
        private const int INDEX_NEW_DEMAND = 0; // modify .Rows.Add manually
        private const int INDEX_ITEM_ID = 1;
        private const int INDEX_ITEM_NAME = 2;
        
        private ShipmentReceivedFormMode _mode = ShipmentReceivedFormMode.ENTER_NEW;
        public FormEnterShipmentReceived(ShipmentReceivedFormMode mode)
        {
            this._mode = mode;

            InitializeComponent();

        }

        private void FormEnterShipmentReceived_Load(object sender, EventArgs e)
        {
            if (_mode == ShipmentReceivedFormMode.ENTER_NEW)
            {
                this.buttonSave.Visible = false;
                this.buttonAddMoreItems.Visible = false;
                this.buttonDelete.Visible = false;
                this.dataGridViewShipmentsList.Visible = false;

                InitializeEnterNewShipment();
                
            }
            else if (_mode == ShipmentReceivedFormMode.EDIT)
            {

                this.textBoxShipmentId.Enabled = false;
                this.buttonCreate.Visible = false;

                InitializeEditShipment();

            }
        }


        private void InitializeEditShipment()
        {
            AtoZ.datasets.AtoZDatabaseDataSet_ShipmentReceived_IdsTableAdapters.ShipmentReceivedTableAdapter adapter = new
                 AtoZ.datasets.AtoZDatabaseDataSet_ShipmentReceived_IdsTableAdapters.ShipmentReceivedTableAdapter();
            AtoZDatabaseDataSet_ShipmentReceived_Ids dataset = new
                 AtoZDatabaseDataSet_ShipmentReceived_Ids();

            adapter.Fill(dataset.ShipmentReceived);

            this.dataGridViewShipmentsList.Rows.Clear();

            foreach (DataRow row in dataset.ShipmentReceived.Rows)
            {
                this.dataGridViewShipmentsList.Rows.Add(row[0], ((DateTime)row[1]).ToShortDateString());
            }

            this.dataGridViewShipmentsList_CellClick(null, null);
        }

        private void InitializeEnterNewShipment()
        {
            // initialize shipment ID
            int? currentMaxShipmentId;
            AtoZ.datasets.AtoZDatabaseDataSet_ShipmentReceivedTableAdapters.ShipmentReceivedTableAdapter tableAdapter =
                new AtoZ.datasets.AtoZDatabaseDataSet_ShipmentReceivedTableAdapters.ShipmentReceivedTableAdapter();

            currentMaxShipmentId = tableAdapter.GetMaxShipmentId();
            if (currentMaxShipmentId == null)
            {
                currentMaxShipmentId = 0;
            }

            currentMaxShipmentId++;

            this.textBoxShipmentId.Text = (currentMaxShipmentId).ToString();

            AddItemsToTable();
        }

        private void AddItemsToTable()
        {
            List<int> currentItemIds = new List<int>();
            foreach (DataGridViewRow row in this.dataGridViewItemList.Rows)
            {
                currentItemIds.Add(int.Parse(row.Cells[INDEX_ITEM_ID].Value.ToString()));
            }

            Dictionary<int, ItemDetailsData> regularItems = ItemDetailsHelper.getAllItems();
            Dictionary<int, ItemDetailsData> postTaxItems = ItemDetailsHelper.getAllPostTaxItems();

            foreach (var kvp in regularItems)
            {
                if ((kvp.Value.category == ItemCategory.DeliveryChallanOnly) && !currentItemIds.Contains(kvp.Key))
                {
                    this.dataGridViewItemList.Rows.Add("0", kvp.Key, kvp.Value.name);
                }
            }

            foreach (var kvp in postTaxItems)
            {
                if ((kvp.Value.category == ItemCategory.DeliveryChallanOnly) && !currentItemIds.Contains(kvp.Key))
                {
                    this.dataGridViewItemList.Rows.Add("0", kvp.Key, kvp.Value.name);
                }
            }

            this.dataGridViewItemList.PerformLayout();

            HelperMethods.setRowNumbers(dataGridViewItemList);
        }

        private void dataGridViewItemList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            if (createShipment(false))
            {
                this.Close();
            }

        }

        private Boolean createShipment(Boolean deleteFirst)
        {
            try
            {
                
                AtoZDatabaseDataSet_ShipmentReceived shipmentDataset = new AtoZDatabaseDataSet_ShipmentReceived();
                AtoZ.datasets.AtoZDatabaseDataSet_ShipmentReceivedTableAdapters.ShipmentReceivedTableAdapter shipmentAdapter =
                    new AtoZ.datasets.AtoZDatabaseDataSet_ShipmentReceivedTableAdapters.ShipmentReceivedTableAdapter();


                int count = 0;

                foreach (DataGridViewRow row in this.dataGridViewItemList.Rows)
                {
                    double quantity;
                    if (row.Cells[INDEX_NEW_DEMAND].Value != null
                        && !string.IsNullOrEmpty(row.Cells[INDEX_NEW_DEMAND].Value.ToString())
                        && double.TryParse(row.Cells[INDEX_NEW_DEMAND].Value.ToString(), out quantity))
                    {
                        if (quantity != 0)
                        {
                            AtoZDatabaseDataSet_ShipmentReceived.ShipmentReceivedRow newRow =
                               (AtoZDatabaseDataSet_ShipmentReceived.ShipmentReceivedRow)
                               shipmentDataset.ShipmentReceived.NewRow();

                            newRow.shipmentId = int.Parse(this.textBoxShipmentId.Text);
                            newRow.date = this.dateTimePickerReceivedDate.Value;
                            newRow.itemId = int.Parse(row.Cells[INDEX_ITEM_ID].Value.ToString());
                            newRow.itemCount = double.Parse(row.Cells[INDEX_NEW_DEMAND].Value.ToString());


                            if (!string.IsNullOrWhiteSpace(this.textBoxOrderId.Text))
                            {
                                newRow.orderNo = this.textBoxOrderId.Text;
                            }

                            if (!string.IsNullOrWhiteSpace(this.textBoxRemarks.Text))
                            {
                                newRow.remarks = this.textBoxRemarks.Text;
                            }

                            if (!string.IsNullOrWhiteSpace(this.textBoxVenderId.Text))
                            {
                                newRow.venderId = int.Parse(this.textBoxVenderId.Text);
                            }

                            shipmentDataset.ShipmentReceived.AddShipmentReceivedRow(newRow);
                            count++;
                        }
                    }


                }

                var response = MessageBox.Show("Do you want to create shipment with " + count + " item(s)", "", MessageBoxButtons.YesNo);

                if (response.Equals(DialogResult.No))
                {
                    return false;
                }

                if(deleteFirst)
                {
                    DeleteShipment(getShipmentId());
                }

                int recordsAdded = shipmentAdapter.Update(shipmentDataset);
                Console.WriteLine("Rows added " + recordsAdded);
                shipmentDataset.ShipmentReceived.AcceptChanges();

                //update inventory
                ClassUpdateInventory updateInventory = new ClassUpdateInventory();
                
                updateInventory.addInventory(shipmentDataset);
 
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

            return true;
        }

        private void dataGridViewShipmentsList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int shipmentId = getShipmentId();
            AtoZ.datasets.AtoZDatabaseDataSet_ShipmentReceivedTableAdapters.ShipmentReceivedTableAdapter adapter = new
             AtoZ.datasets.AtoZDatabaseDataSet_ShipmentReceivedTableAdapters.ShipmentReceivedTableAdapter();
            AtoZDatabaseDataSet_ShipmentReceived dataset = new AtoZDatabaseDataSet_ShipmentReceived();
            adapter.FillByShipmentId(dataset.ShipmentReceived,shipmentId);

            this.dataGridViewItemList.Rows.Clear();
            this.textBoxRemarks.Text = "";
            this.textBoxOrderId.Text = "";
            this.textBoxVenderId.Text = "";

            AtoZDatabaseDataSet_ShipmentReceived.ShipmentReceivedRow row0 = 
                (AtoZDatabaseDataSet_ShipmentReceived.ShipmentReceivedRow)dataset.ShipmentReceived.Rows[0];
            this.textBoxShipmentId.Text = row0.shipmentId.ToString();
            this.dateTimePickerReceivedDate.Value = row0.date;
           
            if (!row0.IsorderNoNull())
            {
                this.textBoxOrderId.Text = row0.orderNo;
            }

            if (!row0.IsremarksNull())
            {
                this.textBoxRemarks.Text = row0.remarks;
            }

            if (!row0.IsvenderIdNull())
            {
                this.textBoxVenderId.Text = row0.venderId.ToString();
            }


            foreach (AtoZDatabaseDataSet_ShipmentReceived.ShipmentReceivedRow row in dataset.ShipmentReceived.Rows)
            {
                this.dataGridViewItemList.Rows.Add(row.itemCount,row.itemId,ItemDetailsHelper.getItemName(row.itemId));
            }



            this.dataGridViewItemList.PerformLayout();

            HelperMethods.setRowNumbers(dataGridViewItemList);

        }

        private int getShipmentId()
        {
            int shipmentId = int.Parse(this.dataGridViewShipmentsList.CurrentRow.Cells[0].Value.ToString());

            return shipmentId;
        }

        private void dataGridViewShipmentsList_KeyDown(object sender, KeyEventArgs e)
        {
            this.dataGridViewShipmentsList_CellClick(null, null);
        }

        private void dataGridViewShipmentsList_KeyUp(object sender, KeyEventArgs e)
        {
            this.dataGridViewShipmentsList_CellClick(null, null);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            createShipment(true);
            this.dataGridViewShipmentsList_CellClick(null, null);

        }

        private void buttonAddMoreItems_Click(object sender, EventArgs e)
        {
            AddItemsToTable();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            var resp = MessageBox.Show("Are you sure you want to delete record with shipment ID " + getShipmentId(),
                "", MessageBoxButtons.YesNo);

            if (resp.Equals(DialogResult.No))
            {
                return;
            }

            int rowCount = DeleteShipment(getShipmentId());

            MessageBox.Show(rowCount + " rows deleted");
            
            InitializeEditShipment();
           
        }

        private int DeleteShipment(int shipmentId)
        {
            AtoZ.datasets.AtoZDatabaseDataSet_ShipmentReceivedTableAdapters.ShipmentReceivedTableAdapter shipmentAdapter =
                    new AtoZ.datasets.AtoZDatabaseDataSet_ShipmentReceivedTableAdapters.ShipmentReceivedTableAdapter();

            // read record set first
            AtoZDatabaseDataSet_ShipmentReceived tempDataset = new AtoZDatabaseDataSet_ShipmentReceived();
            shipmentAdapter.FillByShipmentId(tempDataset.ShipmentReceived, shipmentId);

            // delete dataset from table
            int rowsDeleted = shipmentAdapter.DeleteQuery(shipmentId);
            Console.WriteLine("Rows Deleted:" + rowsDeleted);

            //update inventory
            ClassUpdateInventory updateInventory = new ClassUpdateInventory();
            updateInventory.removeInventory(tempDataset);

            return rowsDeleted;
        }

        private void dataGridViewItemList_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.dataGridViewItemList.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Red;
        }

    }
}

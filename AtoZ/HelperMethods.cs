﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    class HelperMethods
    {
        public static void setRowNumbers(DataGridView dgv)
        {
            foreach (DataGridViewRow row in dgv.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }
        }

        public static void selectRowOnCol0(int colVal, DataGridView dgv)
        {
            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (int.Parse(row.Cells[0].Value.ToString()) == colVal)
                {
                    dgv.CurrentCell = row.Cells[0];
                }
            }
        }
    }
}

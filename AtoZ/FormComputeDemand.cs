﻿using AtoZ.reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    public partial class FormComputeDemand : Form
    {
        public FormComputeDemand()
        {
            InitializeComponent();
            foreach(var shop in ShopsDetailHelper.getShopsHashset())
            {
                this.dataGridView1.Rows.Add(shop.Key, shop.Value.name);
            }

        }

        private void buttonCompute_Click(object sender, EventArgs e)
        {
            this.dataGridView2.Rows.Clear();
            this.dataGridView3.Rows.Clear();

            int shopId = int.Parse(this.dataGridView1.Rows[this.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString());
            List<MonthlyShopSaleData> sortedList = ClassDemandCalculation.GetSortedShopListBySale(this.dateTimePicker1.Value, this.dateTimePicker2.Value);
            MonthlyShopSaleData shopSaleData = sortedList.Find(shop => shop.shopId == shopId);
            int shopRank = sortedList.IndexOf(shopSaleData) + 1;
            if(shopRank == 0)
            {
                MessageBox.Show("No data avaiable for this shop");
                return;
            }
            int totalShops = sortedList.Count;

            this.labelAverageSale.Text = "Total Sale: " + shopSaleData.sale;
            this.labelShopRank.Text = "Shop Rank: " + shopRank + "/" + totalShops;

            ClassDemandCalculation demandCalculator = new ClassDemandCalculation();
            
            Dictionary<int, List<KeyValuePair<int, int>>> itemSized = 
                demandCalculator.getItemDeliveryCounts(this.dateTimePicker1.Value, this.dateTimePicker2.Value);
            
            Dictionary<int, ItemSaleDataSummary> itemsData = 
                demandCalculator.getItemSaleDataHashSet(this.dateTimePicker1.Value, this.dateTimePicker2.Value);

            Dictionary<string, ShopMonthlySaleData>
                shopsData = demandCalculator.getShopSaleDataForAllShops(this.dateTimePicker1.Value, this.dateTimePicker2.Value);

            int numberOfMonths = ClassDemandCalculation.getMonthCount(shopId, shopsData);
            this.labelNoOfMonths.Text = "No of months (sale data) : " + numberOfMonths;

            var monthsCount = ClassDemandCalculation.getBillMonthsCount(shopsData);
            foreach(var v in monthsCount)
            {
                dataGridView3.Rows.Add(v.Key, v.Value);
            }
            // COMPUTE TableData
            Dictionary<int, TableRowData> tableData = new Dictionary<int, TableRowData>();
            

            foreach(var itemData in itemsData)
            {
                TableRowData rowData = new TableRowData();
                rowData.itemId = itemData.Key;
                rowData.name = ItemDetailsHelper.getItemName(rowData.itemId);
                if (itemData.Value.shopSale.ContainsKey(shopId))
                {
                    rowData.shopAvg = (itemData.Value.shopSale[shopId] / ItemDetailsHelper.getItemDetails(rowData.itemId).price)/ numberOfMonths;
                }
                else
                {
                    rowData.shopAvg = 0;
                }

                rowData.computedAvgForThisShop =
                    (itemData.Value.average * (shopSaleData.sale / numberOfMonths) / 100) / ItemDetailsHelper.getItemDetails(rowData.itemId).price;
                rowData.sd =
                    (itemData.Value.standartDeviation * (shopSaleData.sale / numberOfMonths) / 100) / ItemDetailsHelper.getItemDetails(rowData.itemId).price;

                rowData.sizes = new List<string>();
                if(itemSized.ContainsKey(rowData.itemId))
                {
                    foreach(var v in itemSized[rowData.itemId])
                    {
                        rowData.sizes.Add(v.Key + ":" + v.Value);
                    }
                }

                //fill remaining
                for(int i = rowData.sizes.Count ; i < 5; i ++)
                {
                    rowData.sizes.Add("");
                }

                tableData.Add(rowData.itemId, rowData);
            }
            // UPDATE TABLE
            foreach(var row in tableData.Values)
            {
                this.dataGridView2.Rows.Add(row.itemId, row.name, row.shopAvg.ToString("0.00"), row.computedAvgForThisShop.ToString("0.00"), row.sd.ToString("0.00"),
                    row.sizes[0], row.sizes[1], row.sizes[2], row.sizes[3], row.sizes[4]);
            }
        }

    }

    class TableRowData
    {
        public int itemId {get; set;}
        public string name { get; set; }
        public double shopAvg { get; set; }
        public double computedAvgForThisShop { get; set; }
        public double sd { get; set; }
        public List<string> sizes { get; set; }
       
       
    }
}

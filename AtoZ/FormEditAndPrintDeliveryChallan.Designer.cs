﻿using AtoZ.datasets;
namespace AtoZ
{
    partial class FormEditAndPrintDeliveryChallan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditAndPrintDeliveryChallan));
            this.deliveryChallanBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.deliveryChallanBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet_DeliveryChallan = new AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallan();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.deliveryChallanBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.deliveryChallanDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopsDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet10 = new AtoZ.datasets.AtoZDatabaseDataSet10();
            this.shopsTableAdapter = new AtoZ.datasets.AtoZDatabaseDataSet10TableAdapters.shopsTableAdapter();
            this.tableAdapterManager1 = new AtoZ.datasets.AtoZDatabaseDataSet10TableAdapters.TableAdapterManager();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.challanIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.challanDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deliveryChallanBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet13 = new AtoZ.datasets.AtoZDatabaseDataSet13();
            this.deliveryChallanTableAdapter1 = new AtoZ.datasets.AtoZDatabaseDataSet13TableAdapters.DeliveryChallanTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonMergeChallan = new System.Windows.Forms.Button();
            this.buttonUnmergeChallan = new System.Windows.Forms.Button();
            this.buttonDeleteChallan = new System.Windows.Forms.Button();
            this.buttonAddMoreItems = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.deliveryChallanTableAdapter = new AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter();
            this.tableAdapterManager = new AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.TableAdapterManager();
            this.textBoxNewOrderId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxShowAllChallans = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryChallanBindingNavigator)).BeginInit();
            this.deliveryChallanBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryChallanBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet_DeliveryChallan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryChallanDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryChallanBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet13)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // deliveryChallanBindingNavigator
            // 
            this.deliveryChallanBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.deliveryChallanBindingNavigator.BindingSource = this.deliveryChallanBindingSource;
            this.deliveryChallanBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.deliveryChallanBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.deliveryChallanBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.deliveryChallanBindingNavigatorSaveItem});
            this.deliveryChallanBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.deliveryChallanBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.deliveryChallanBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.deliveryChallanBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.deliveryChallanBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.deliveryChallanBindingNavigator.Name = "deliveryChallanBindingNavigator";
            this.deliveryChallanBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.deliveryChallanBindingNavigator.Size = new System.Drawing.Size(2234, 39);
            this.deliveryChallanBindingNavigator.TabIndex = 0;
            this.deliveryChallanBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Enabled = false;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // deliveryChallanBindingSource
            // 
            this.deliveryChallanBindingSource.DataMember = "DeliveryChallan";
            this.deliveryChallanBindingSource.DataSource = this.atoZDatabaseDataSet_DeliveryChallan;
            // 
            // atoZDatabaseDataSet_DeliveryChallan
            // 
            this.atoZDatabaseDataSet_DeliveryChallan.DataSetName = "AtoZDatabaseDataSet_DeliveryChallan";
            this.atoZDatabaseDataSet_DeliveryChallan.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(71, 36);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Enabled = false;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 39);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 39);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // deliveryChallanBindingNavigatorSaveItem
            // 
            this.deliveryChallanBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deliveryChallanBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("deliveryChallanBindingNavigatorSaveItem.Image")));
            this.deliveryChallanBindingNavigatorSaveItem.Name = "deliveryChallanBindingNavigatorSaveItem";
            this.deliveryChallanBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 36);
            this.deliveryChallanBindingNavigatorSaveItem.Text = "Save Data";
            this.deliveryChallanBindingNavigatorSaveItem.Click += new System.EventHandler(this.deliveryChallanBindingNavigatorSaveItem_Click);
            // 
            // deliveryChallanDataGridView
            // 
            this.deliveryChallanDataGridView.AllowUserToAddRows = false;
            this.deliveryChallanDataGridView.AutoGenerateColumns = false;
            this.deliveryChallanDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.deliveryChallanDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn10});
            this.tableLayoutPanel1.SetColumnSpan(this.deliveryChallanDataGridView, 3);
            this.deliveryChallanDataGridView.DataSource = this.deliveryChallanBindingSource;
            this.deliveryChallanDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deliveryChallanDataGridView.Location = new System.Drawing.Point(3, 484);
            this.deliveryChallanDataGridView.Name = "deliveryChallanDataGridView";
            this.deliveryChallanDataGridView.RowHeadersWidth = 65;
            this.deliveryChallanDataGridView.RowTemplate.Height = 33;
            this.deliveryChallanDataGridView.Size = new System.Drawing.Size(2228, 475);
            this.deliveryChallanDataGridView.TabIndex = 1;
            this.deliveryChallanDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.deliveryChallanDataGridView_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "itemCatNo";
            this.dataGridViewTextBoxColumn2.HeaderText = "itemCatNo";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 137;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "itemName";
            this.dataGridViewTextBoxColumn4.HeaderText = "itemName";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 133;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "itemQuantity";
            this.dataGridViewTextBoxColumn3.HeaderText = "itemQuantity";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 157;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn10.DataPropertyName = "itemRemarks";
            this.dataGridViewTextBoxColumn10.HeaderText = "itemRemarks";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 162;
            // 
            // shopsDataGridView
            // 
            this.shopsDataGridView.AllowUserToAddRows = false;
            this.shopsDataGridView.AllowUserToDeleteRows = false;
            this.shopsDataGridView.AutoGenerateColumns = false;
            this.shopsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.shopsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16});
            this.shopsDataGridView.DataSource = this.shopsBindingSource;
            this.shopsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shopsDataGridView.Location = new System.Drawing.Point(3, 3);
            this.shopsDataGridView.Name = "shopsDataGridView";
            this.shopsDataGridView.ReadOnly = true;
            this.shopsDataGridView.RowTemplate.Height = 33;
            this.shopsDataGridView.Size = new System.Drawing.Size(664, 475);
            this.shopsDataGridView.TabIndex = 0;
            this.shopsDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.shopsDataGridView_CellClick);
            this.shopsDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.shopsDataGridView_KeyDown);
            this.shopsDataGridView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.shopsDataGridView_KeyPress);
            this.shopsDataGridView.KeyUp += new System.Windows.Forms.KeyEventHandler(this.shopsDataGridView_KeyUp);
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn15.HeaderText = "Id";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 54;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn16.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn16.HeaderText = "name";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 90;
            // 
            // shopsBindingSource
            // 
            this.shopsBindingSource.DataMember = "shops";
            this.shopsBindingSource.DataSource = this.atoZDatabaseDataSet10;
            // 
            // atoZDatabaseDataSet10
            // 
            this.atoZDatabaseDataSet10.DataSetName = "AtoZDatabaseDataSet10";
            this.atoZDatabaseDataSet10.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // shopsTableAdapter
            // 
            this.shopsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.shopsTableAdapter = this.shopsTableAdapter;
            this.tableAdapterManager1.UpdateOrder = AtoZ.datasets.AtoZDatabaseDataSet10TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.challanIdDataGridViewTextBoxColumn,
            this.challanDateDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.deliveryChallanBindingSource1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(673, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(664, 475);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            this.dataGridView1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridView1_KeyPress);
            this.dataGridView1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyUp);
            // 
            // challanIdDataGridViewTextBoxColumn
            // 
            this.challanIdDataGridViewTextBoxColumn.DataPropertyName = "challanId";
            this.challanIdDataGridViewTextBoxColumn.HeaderText = "challanId";
            this.challanIdDataGridViewTextBoxColumn.Name = "challanIdDataGridViewTextBoxColumn";
            this.challanIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // challanDateDataGridViewTextBoxColumn
            // 
            this.challanDateDataGridViewTextBoxColumn.DataPropertyName = "challanDate";
            this.challanDateDataGridViewTextBoxColumn.HeaderText = "challanDate";
            this.challanDateDataGridViewTextBoxColumn.Name = "challanDateDataGridViewTextBoxColumn";
            this.challanDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // deliveryChallanBindingSource1
            // 
            this.deliveryChallanBindingSource1.DataMember = "DeliveryChallan";
            this.deliveryChallanBindingSource1.DataSource = this.atoZDatabaseDataSet13;
            // 
            // atoZDatabaseDataSet13
            // 
            this.atoZDatabaseDataSet13.DataSetName = "AtoZDatabaseDataSet13";
            this.atoZDatabaseDataSet13.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // deliveryChallanTableAdapter1
            // 
            this.deliveryChallanTableAdapter1.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(435, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(271, 69);
            this.button1.TabIndex = 15;
            this.button1.Text = "Print";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonMergeChallan
            // 
            this.buttonMergeChallan.Location = new System.Drawing.Point(435, 125);
            this.buttonMergeChallan.Name = "buttonMergeChallan";
            this.buttonMergeChallan.Size = new System.Drawing.Size(271, 69);
            this.buttonMergeChallan.TabIndex = 16;
            this.buttonMergeChallan.Text = "Remove from Local Inventory";
            this.buttonMergeChallan.UseVisualStyleBackColor = true;
            this.buttonMergeChallan.Click += new System.EventHandler(this.buttonMergeChallan_Click);
            // 
            // buttonUnmergeChallan
            // 
            this.buttonUnmergeChallan.Location = new System.Drawing.Point(435, 221);
            this.buttonUnmergeChallan.Name = "buttonUnmergeChallan";
            this.buttonUnmergeChallan.Size = new System.Drawing.Size(271, 69);
            this.buttonUnmergeChallan.TabIndex = 17;
            this.buttonUnmergeChallan.Text = "Add to Local Inventory";
            this.buttonUnmergeChallan.UseVisualStyleBackColor = true;
            this.buttonUnmergeChallan.Click += new System.EventHandler(this.buttonUnmergeChallan_Click);
            // 
            // buttonDeleteChallan
            // 
            this.buttonDeleteChallan.Location = new System.Drawing.Point(27, 323);
            this.buttonDeleteChallan.Name = "buttonDeleteChallan";
            this.buttonDeleteChallan.Size = new System.Drawing.Size(295, 69);
            this.buttonDeleteChallan.TabIndex = 18;
            this.buttonDeleteChallan.Text = "Delete Challan";
            this.buttonDeleteChallan.UseVisualStyleBackColor = true;
            this.buttonDeleteChallan.Click += new System.EventHandler(this.buttonDeleteChallan_Click);
            // 
            // buttonAddMoreItems
            // 
            this.buttonAddMoreItems.Location = new System.Drawing.Point(27, 233);
            this.buttonAddMoreItems.Name = "buttonAddMoreItems";
            this.buttonAddMoreItems.Size = new System.Drawing.Size(295, 69);
            this.buttonAddMoreItems.TabIndex = 19;
            this.buttonAddMoreItems.Text = "Add More Items";
            this.buttonAddMoreItems.UseVisualStyleBackColor = true;
            this.buttonAddMoreItems.Click += new System.EventHandler(this.buttonAddMoreItems_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(27, 83);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(402, 31);
            this.dateTimePicker1.TabIndex = 20;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // deliveryChallanTableAdapter
            // 
            this.deliveryChallanTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DeliveryChallanTableAdapter = this.deliveryChallanTableAdapter;
            this.tableAdapterManager.UpdateOrder = AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // textBoxNewOrderId
            // 
            this.textBoxNewOrderId.Location = new System.Drawing.Point(27, 135);
            this.textBoxNewOrderId.Name = "textBoxNewOrderId";
            this.textBoxNewOrderId.Size = new System.Drawing.Size(274, 31);
            this.textBoxNewOrderId.TabIndex = 22;
            this.textBoxNewOrderId.Enter += new System.EventHandler(this.textBoxNewOrderId_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 25);
            this.label4.TabIndex = 21;
            this.label4.Text = "Order ID: ";
            // 
            // checkBoxShowAllChallans
            // 
            this.checkBoxShowAllChallans.AutoSize = true;
            this.checkBoxShowAllChallans.Location = new System.Drawing.Point(27, 33);
            this.checkBoxShowAllChallans.Name = "checkBoxShowAllChallans";
            this.checkBoxShowAllChallans.Size = new System.Drawing.Size(217, 29);
            this.checkBoxShowAllChallans.TabIndex = 23;
            this.checkBoxShowAllChallans.Text = "Show All Challans";
            this.checkBoxShowAllChallans.UseVisualStyleBackColor = true;
            this.checkBoxShowAllChallans.CheckedChanged += new System.EventHandler(this.checkBoxShowAllChallans_CheckedChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.shopsDataGridView, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.deliveryChallanDataGridView, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 39);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(2234, 962);
            this.tableLayoutPanel1.TabIndex = 24;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.buttonDeleteChallan);
            this.panel1.Controls.Add(this.buttonAddMoreItems);
            this.panel1.Controls.Add(this.checkBoxShowAllChallans);
            this.panel1.Controls.Add(this.textBoxNewOrderId);
            this.panel1.Controls.Add(this.buttonUnmergeChallan);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.buttonMergeChallan);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(1343, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(888, 475);
            this.panel1.TabIndex = 25;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // FormEditAndPrintDeliveryChallan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2234, 1001);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.deliveryChallanBindingNavigator);
            this.Name = "FormEditAndPrintDeliveryChallan";
            this.Text = "FormEditAndPrintDeliveryChallan";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormEditAndPrintDeliveryChallan_FormClosing);
            this.Load += new System.EventHandler(this.FormEditAndPrintDeliveryChallan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.deliveryChallanBindingNavigator)).EndInit();
            this.deliveryChallanBindingNavigator.ResumeLayout(false);
            this.deliveryChallanBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryChallanBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet_DeliveryChallan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryChallanDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryChallanBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet13)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AtoZDatabaseDataSet_DeliveryChallan atoZDatabaseDataSet_DeliveryChallan;
        private System.Windows.Forms.BindingSource deliveryChallanBindingSource;
        private AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.DeliveryChallanTableAdapter deliveryChallanTableAdapter;
        private AtoZ.datasets.AtoZDatabaseDataSet_DeliveryChallanTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator deliveryChallanBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton deliveryChallanBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView deliveryChallanDataGridView;
        private AtoZDatabaseDataSet10 atoZDatabaseDataSet10;
        private System.Windows.Forms.BindingSource shopsBindingSource;
        private AtoZ.datasets.AtoZDatabaseDataSet10TableAdapters.shopsTableAdapter shopsTableAdapter;
        private AtoZ.datasets.AtoZDatabaseDataSet10TableAdapters.TableAdapterManager tableAdapterManager1;
        private System.Windows.Forms.DataGridView shopsDataGridView;
        private System.Windows.Forms.DataGridView dataGridView1;
        private AtoZDatabaseDataSet13 atoZDatabaseDataSet13;
        private System.Windows.Forms.BindingSource deliveryChallanBindingSource1;
        private AtoZ.datasets.AtoZDatabaseDataSet13TableAdapters.DeliveryChallanTableAdapter deliveryChallanTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn challanIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn challanDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonMergeChallan;
        private System.Windows.Forms.Button buttonUnmergeChallan;
        private System.Windows.Forms.Button buttonDeleteChallan;
        private System.Windows.Forms.Button buttonAddMoreItems;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox textBoxNewOrderId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.CheckBox checkBoxShowAllChallans;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
    }
}
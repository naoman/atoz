﻿using AtoZ.reports;
using AtoZ.datasets;

namespace AtoZ
{
    partial class FormUpdateShopStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUpdateShopStock));
            this.shopStatusBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.shopStatusBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.shopStatusDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.shopsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet6 = new AtoZDatabaseDataSet6();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonImportData = new System.Windows.Forms.Button();
            this.atoZDatabaseDataSet_Shops = new AtoZDatabaseDataSet_Shops();
            this.atoZDatabaseDataSetShopsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSetShopsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.shopsTableAdapter = new AtoZ.datasets.AtoZDatabaseDataSet6TableAdapters.shopsTableAdapter();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonAddAllItems = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.labelShopSelected = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet_ShopStatus = new AtoZDatabaseDataSet_ShopStatus();
            this.shopStatusTableAdapter = new AtoZ.datasets.AtoZDatabaseDataSet_ShopStatusTableAdapters.shopStatusTableAdapter();
            this.tableAdapterManager = new AtoZ.datasets.AtoZDatabaseDataSet_ShopStatusTableAdapters.TableAdapterManager();
            ((System.ComponentModel.ISupportInitialize)(this.shopStatusBindingNavigator)).BeginInit();
            this.shopStatusBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopStatusDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet_Shops)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSetShopsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSetShopsBindingSource1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet_ShopStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // shopStatusBindingNavigator
            // 
            this.shopStatusBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.shopStatusBindingNavigator.BindingSource = this.shopStatusBindingSource;
            this.shopStatusBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.shopStatusBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.shopStatusBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.shopStatusBindingNavigatorSaveItem});
            this.shopStatusBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.shopStatusBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.shopStatusBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.shopStatusBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.shopStatusBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.shopStatusBindingNavigator.Name = "shopStatusBindingNavigator";
            this.shopStatusBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.shopStatusBindingNavigator.Size = new System.Drawing.Size(1668, 39);
            this.shopStatusBindingNavigator.TabIndex = 0;
            this.shopStatusBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Enabled = false;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(71, 36);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Enabled = false;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 39);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 39);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 36);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // shopStatusBindingNavigatorSaveItem
            // 
            this.shopStatusBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.shopStatusBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("shopStatusBindingNavigatorSaveItem.Image")));
            this.shopStatusBindingNavigatorSaveItem.Name = "shopStatusBindingNavigatorSaveItem";
            this.shopStatusBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 36);
            this.shopStatusBindingNavigatorSaveItem.Text = "Save Data";
            this.shopStatusBindingNavigatorSaveItem.Click += new System.EventHandler(this.shopStatusBindingNavigatorSaveItem_Click);
            // 
            // shopStatusDataGridView
            // 
            this.shopStatusDataGridView.AllowUserToAddRows = false;
            this.shopStatusDataGridView.AllowUserToDeleteRows = false;
            this.shopStatusDataGridView.AutoGenerateColumns = false;
            this.shopStatusDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.shopStatusDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.shopStatusDataGridView.DataSource = this.shopStatusBindingSource;
            this.shopStatusDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shopStatusDataGridView.Location = new System.Drawing.Point(503, 319);
            this.shopStatusDataGridView.Name = "shopStatusDataGridView";
            this.shopStatusDataGridView.RowTemplate.Height = 33;
            this.shopStatusDataGridView.Size = new System.Drawing.Size(1162, 734);
            this.shopStatusDataGridView.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.shopsBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 319);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(494, 734);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowHeaderMouseClick);
            this.dataGridView1.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_RowHeaderMouseClick);
            // 
            // shopsBindingSource
            // 
            this.shopsBindingSource.DataMember = "shops";
            this.shopsBindingSource.DataSource = this.atoZDatabaseDataSet6;
            // 
            // atoZDatabaseDataSet6
            // 
            this.atoZDatabaseDataSet6.DataSetName = "AtoZDatabaseDataSet6";
            this.atoZDatabaseDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 225);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Shop Selected:";
            // 
            // buttonImportData
            // 
            this.buttonImportData.Location = new System.Drawing.Point(42, 92);
            this.buttonImportData.Name = "buttonImportData";
            this.buttonImportData.Size = new System.Drawing.Size(271, 57);
            this.buttonImportData.TabIndex = 6;
            this.buttonImportData.Text = "Import Data";
            this.buttonImportData.UseVisualStyleBackColor = true;
            this.buttonImportData.Click += new System.EventHandler(this.buttonImportData_Click);
            // 
            // atoZDatabaseDataSet_Shops
            // 
            this.atoZDatabaseDataSet_Shops.DataSetName = "AtoZDatabaseDataSet_Shops";
            this.atoZDatabaseDataSet_Shops.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // atoZDatabaseDataSetShopsBindingSource
            // 
            this.atoZDatabaseDataSetShopsBindingSource.DataSource = this.atoZDatabaseDataSet_Shops;
            this.atoZDatabaseDataSetShopsBindingSource.Position = 0;
            // 
            // atoZDatabaseDataSetShopsBindingSource1
            // 
            this.atoZDatabaseDataSetShopsBindingSource1.DataSource = this.atoZDatabaseDataSet_Shops;
            this.atoZDatabaseDataSetShopsBindingSource1.Position = 0;
            // 
            // shopsTableAdapter
            // 
            this.shopsTableAdapter.ClearBeforeFill = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.shopStatusDataGridView, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 39);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1668, 1056);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // buttonAddAllItems
            // 
            this.buttonAddAllItems.Location = new System.Drawing.Point(42, 20);
            this.buttonAddAllItems.Name = "buttonAddAllItems";
            this.buttonAddAllItems.Size = new System.Drawing.Size(271, 51);
            this.buttonAddAllItems.TabIndex = 7;
            this.buttonAddAllItems.Text = "Add All Items";
            this.buttonAddAllItems.UseVisualStyleBackColor = true;
            this.buttonAddAllItems.Click += new System.EventHandler(this.buttonAddAllItems_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelShopSelected);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.buttonAddAllItems);
            this.panel1.Controls.Add(this.buttonImportData);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(503, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1162, 310);
            this.panel1.TabIndex = 8;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Width = 54;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Width = 90;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 209);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 25);
            this.label3.TabIndex = 9;
            this.label3.Text = "Shop Selected: ";
            // 
            // labelShopSelected
            // 
            this.labelShopSelected.AutoSize = true;
            this.labelShopSelected.Location = new System.Drawing.Point(222, 209);
            this.labelShopSelected.Name = "labelShopSelected";
            this.labelShopSelected.Size = new System.Drawing.Size(60, 25);
            this.labelShopSelected.TabIndex = 10;
            this.labelShopSelected.Text = "none";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "itemId";
            this.dataGridViewTextBoxColumn3.HeaderText = "itemId";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 94;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "balance";
            this.dataGridViewTextBoxColumn4.HeaderText = "balance";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 113;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "custom1";
            this.dataGridViewTextBoxColumn5.HeaderText = "custom1";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 118;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "custom2";
            this.dataGridViewTextBoxColumn6.HeaderText = "custom2";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 118;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "custom3";
            this.dataGridViewTextBoxColumn7.HeaderText = "custom3";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 118;
            // 
            // shopStatusBindingSource
            // 
            this.shopStatusBindingSource.DataMember = "shopStatus";
            this.shopStatusBindingSource.DataSource = this.atoZDatabaseDataSet_ShopStatus;
            // 
            // atoZDatabaseDataSet_ShopStatus
            // 
            this.atoZDatabaseDataSet_ShopStatus.DataSetName = "AtoZDatabaseDataSet_ShopStatus";
            this.atoZDatabaseDataSet_ShopStatus.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // shopStatusTableAdapter
            // 
            this.shopStatusTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.shopStatusTableAdapter = this.shopStatusTableAdapter;
            this.tableAdapterManager.UpdateOrder = AtoZ.datasets.AtoZDatabaseDataSet_ShopStatusTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // FormUpdateShopStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1668, 1095);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.shopStatusBindingNavigator);
            this.Name = "FormUpdateShopStock";
            this.Text = "FormUpdateShopStock";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormUpdateShopStock_Load);
            ((System.ComponentModel.ISupportInitialize)(this.shopStatusBindingNavigator)).EndInit();
            this.shopStatusBindingNavigator.ResumeLayout(false);
            this.shopStatusBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopStatusDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet_Shops)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSetShopsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSetShopsBindingSource1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet_ShopStatus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AtoZDatabaseDataSet_ShopStatus atoZDatabaseDataSet_ShopStatus;
        private System.Windows.Forms.BindingSource shopStatusBindingSource;
        private AtoZ.datasets.AtoZDatabaseDataSet_ShopStatusTableAdapters.shopStatusTableAdapter shopStatusTableAdapter;
        private AtoZ.datasets.AtoZDatabaseDataSet_ShopStatusTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator shopStatusBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton shopStatusBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView shopStatusDataGridView;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource atoZDatabaseDataSetShopsBindingSource;
        private AtoZDatabaseDataSet_Shops atoZDatabaseDataSet_Shops;
        private System.Windows.Forms.BindingSource atoZDatabaseDataSetShopsBindingSource1;
        private AtoZDatabaseDataSet6 atoZDatabaseDataSet6;
        private System.Windows.Forms.BindingSource shopsBindingSource;
        private AtoZ.datasets.AtoZDatabaseDataSet6TableAdapters.shopsTableAdapter shopsTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonImportData;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonAddAllItems;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label labelShopSelected;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
    }
}
﻿namespace AtoZ
{
    partial class FormEnterShipmentReceived
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.textBoxRemarks = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxVenderId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxOrderId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxShipmentId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePickerReceivedDate = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridViewItemList = new System.Windows.Forms.DataGridView();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewShipmentsList = new System.Windows.Forms.DataGridView();
            this.ShipmentId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShipmentDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonAddMoreItems = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItemList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShipmentsList)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonDelete);
            this.panel1.Controls.Add(this.buttonAddMoreItems);
            this.panel1.Controls.Add(this.buttonSave);
            this.panel1.Controls.Add(this.buttonCreate);
            this.panel1.Controls.Add(this.textBoxRemarks);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBoxVenderId);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBoxOrderId);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxShipmentId);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.dateTimePickerReceivedDate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(561, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1298, 451);
            this.panel1.TabIndex = 0;
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(716, 32);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(247, 53);
            this.buttonCreate.TabIndex = 10;
            this.buttonCreate.Text = "Create";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // textBoxRemarks
            // 
            this.textBoxRemarks.Location = new System.Drawing.Point(216, 284);
            this.textBoxRemarks.Name = "textBoxRemarks";
            this.textBoxRemarks.Size = new System.Drawing.Size(410, 31);
            this.textBoxRemarks.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 284);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 25);
            this.label5.TabIndex = 8;
            this.label5.Text = "Remarks";
            // 
            // textBoxVenderId
            // 
            this.textBoxVenderId.Location = new System.Drawing.Point(216, 224);
            this.textBoxVenderId.Name = "textBoxVenderId";
            this.textBoxVenderId.Size = new System.Drawing.Size(410, 31);
            this.textBoxVenderId.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 224);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Vender Id";
            // 
            // textBoxOrderId
            // 
            this.textBoxOrderId.Location = new System.Drawing.Point(216, 162);
            this.textBoxOrderId.Name = "textBoxOrderId";
            this.textBoxOrderId.Size = new System.Drawing.Size(410, 31);
            this.textBoxOrderId.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Order Id";
            // 
            // textBoxShipmentId
            // 
            this.textBoxShipmentId.Location = new System.Drawing.Point(216, 99);
            this.textBoxShipmentId.Name = "textBoxShipmentId";
            this.textBoxShipmentId.Size = new System.Drawing.Size(410, 31);
            this.textBoxShipmentId.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Shipment Id";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Date Received";
            // 
            // dateTimePickerReceivedDate
            // 
            this.dateTimePickerReceivedDate.Location = new System.Drawing.Point(216, 32);
            this.dateTimePickerReceivedDate.Name = "dateTimePickerReceivedDate";
            this.dateTimePickerReceivedDate.Size = new System.Drawing.Size(410, 31);
            this.dateTimePickerReceivedDate.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewItemList, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewShipmentsList, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1862, 1143);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // dataGridViewItemList
            // 
            this.dataGridViewItemList.AllowUserToAddRows = false;
            this.dataGridViewItemList.AllowUserToDeleteRows = false;
            this.dataGridViewItemList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewItemList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.quantity,
            this.itemId,
            this.ItemName});
            this.tableLayoutPanel1.SetColumnSpan(this.dataGridViewItemList, 2);
            this.dataGridViewItemList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewItemList.Location = new System.Drawing.Point(3, 460);
            this.dataGridViewItemList.Name = "dataGridViewItemList";
            this.dataGridViewItemList.RowHeadersWidth = 60;
            this.dataGridViewItemList.RowTemplate.Height = 33;
            this.dataGridViewItemList.Size = new System.Drawing.Size(1856, 680);
            this.dataGridViewItemList.TabIndex = 2;
            this.dataGridViewItemList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewItemList_CellContentClick);
            this.dataGridViewItemList.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewItemList_CellEndEdit);
            // 
            // quantity
            // 
            this.quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.quantity.HeaderText = "Quantity";
            this.quantity.Name = "quantity";
            this.quantity.Width = 117;
            // 
            // itemId
            // 
            this.itemId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.itemId.HeaderText = "Cat No";
            this.itemId.Name = "itemId";
            this.itemId.ReadOnly = true;
            this.itemId.Width = 103;
            // 
            // ItemName
            // 
            this.ItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ItemName.HeaderText = "Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            this.ItemName.Width = 93;
            // 
            // dataGridViewShipmentsList
            // 
            this.dataGridViewShipmentsList.AllowUserToAddRows = false;
            this.dataGridViewShipmentsList.AllowUserToDeleteRows = false;
            this.dataGridViewShipmentsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewShipmentsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ShipmentId,
            this.ShipmentDate});
            this.dataGridViewShipmentsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewShipmentsList.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewShipmentsList.Name = "dataGridViewShipmentsList";
            this.dataGridViewShipmentsList.ReadOnly = true;
            this.dataGridViewShipmentsList.RowTemplate.Height = 33;
            this.dataGridViewShipmentsList.Size = new System.Drawing.Size(552, 451);
            this.dataGridViewShipmentsList.TabIndex = 3;
            this.dataGridViewShipmentsList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewShipmentsList_CellClick);
            this.dataGridViewShipmentsList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridViewShipmentsList_KeyDown);
            this.dataGridViewShipmentsList.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridViewShipmentsList_KeyUp);
            // 
            // ShipmentId
            // 
            this.ShipmentId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ShipmentId.HeaderText = "ID";
            this.ShipmentId.Name = "ShipmentId";
            this.ShipmentId.ReadOnly = true;
            this.ShipmentId.Width = 57;
            // 
            // ShipmentDate
            // 
            this.ShipmentDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ShipmentDate.HeaderText = "Date";
            this.ShipmentDate.Name = "ShipmentDate";
            this.ShipmentDate.ReadOnly = true;
            this.ShipmentDate.Width = 82;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(716, 99);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(247, 53);
            this.buttonSave.TabIndex = 11;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonAddMoreItems
            // 
            this.buttonAddMoreItems.Location = new System.Drawing.Point(716, 172);
            this.buttonAddMoreItems.Name = "buttonAddMoreItems";
            this.buttonAddMoreItems.Size = new System.Drawing.Size(247, 53);
            this.buttonAddMoreItems.TabIndex = 12;
            this.buttonAddMoreItems.Text = "Add More Items";
            this.buttonAddMoreItems.UseVisualStyleBackColor = true;
            this.buttonAddMoreItems.Click += new System.EventHandler(this.buttonAddMoreItems_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(716, 245);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(247, 53);
            this.buttonDelete.TabIndex = 13;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // FormEnterShipmentReceived
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1862, 1143);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FormEnterShipmentReceived";
            this.Text = "FormEnterShipmentReceived";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormEnterShipmentReceived_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewItemList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShipmentsList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dataGridViewItemList;
        private System.Windows.Forms.Button buttonCreate;
        private System.Windows.Forms.TextBox textBoxRemarks;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxVenderId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxOrderId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxShipmentId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePickerReceivedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridView dataGridViewShipmentsList;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipmentId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShipmentDate;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonAddMoreItems;
        private System.Windows.Forms.Button buttonDelete;
    }
}
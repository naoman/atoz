﻿namespace AtoZ
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.inventoryManagement = new System.Windows.Forms.Button();
            this.buttonShopManagement = new System.Windows.Forms.Button();
            this.buttonUpdateShopStock = new System.Windows.Forms.Button();
            this.buttonEnterShopSalesReceipt = new System.Windows.Forms.Button();
            this.buttonFormShopSalesInvoiceDetails = new System.Windows.Forms.Button();
            this.buttonCreateDeliveryChallan = new System.Windows.Forms.Button();
            this.buttonEditAndPrintDeliveryChallan = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonComputeDemand = new System.Windows.Forms.Button();
            this.buttonCreateDeliveryChallanKohinoor = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonEnterShopSalesInvoiceKohinoor = new System.Windows.Forms.Button();
            this.buttonTaxSummary = new System.Windows.Forms.Button();
            this.buttonDebugShopSalesInvoice = new System.Windows.Forms.Button();
            this.buttonDebugDeliveryChallan = new System.Windows.Forms.Button();
            this.buttonBackupDatabase = new System.Windows.Forms.Button();
            this.buttonRestoreDatabase = new System.Windows.Forms.Button();
            this.buttonDebugShipmentReceived = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonCreateShipmentReceived = new System.Windows.Forms.Button();
            this.buttonEditShipmentReceived = new System.Windows.Forms.Button();
            this.buttonDebugShopStatus = new System.Windows.Forms.Button();
            this.buttonEvaluateLocalStock = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // inventoryManagement
            // 
            this.inventoryManagement.Location = new System.Drawing.Point(31, 183);
            this.inventoryManagement.Name = "inventoryManagement";
            this.inventoryManagement.Size = new System.Drawing.Size(477, 109);
            this.inventoryManagement.TabIndex = 0;
            this.inventoryManagement.Text = "Add New Items";
            this.inventoryManagement.UseVisualStyleBackColor = true;
            this.inventoryManagement.Click += new System.EventHandler(this.inventoryManagement_Click);
            // 
            // buttonShopManagement
            // 
            this.buttonShopManagement.Location = new System.Drawing.Point(31, 47);
            this.buttonShopManagement.Name = "buttonShopManagement";
            this.buttonShopManagement.Size = new System.Drawing.Size(477, 109);
            this.buttonShopManagement.TabIndex = 3;
            this.buttonShopManagement.Text = "Add New Shop";
            this.buttonShopManagement.UseVisualStyleBackColor = true;
            this.buttonShopManagement.Click += new System.EventHandler(this.buttonShopManagement_Click);
            // 
            // buttonUpdateShopStock
            // 
            this.buttonUpdateShopStock.Location = new System.Drawing.Point(31, 323);
            this.buttonUpdateShopStock.Name = "buttonUpdateShopStock";
            this.buttonUpdateShopStock.Size = new System.Drawing.Size(477, 109);
            this.buttonUpdateShopStock.TabIndex = 4;
            this.buttonUpdateShopStock.Text = "Update Shop Stock";
            this.buttonUpdateShopStock.UseVisualStyleBackColor = true;
            this.buttonUpdateShopStock.Click += new System.EventHandler(this.buttonUpdateShopStock_Click);
            // 
            // buttonEnterShopSalesReceipt
            // 
            this.buttonEnterShopSalesReceipt.Location = new System.Drawing.Point(30, 47);
            this.buttonEnterShopSalesReceipt.Name = "buttonEnterShopSalesReceipt";
            this.buttonEnterShopSalesReceipt.Size = new System.Drawing.Size(477, 109);
            this.buttonEnterShopSalesReceipt.TabIndex = 5;
            this.buttonEnterShopSalesReceipt.Text = "Enter Shop Sales Invoice";
            this.buttonEnterShopSalesReceipt.UseVisualStyleBackColor = true;
            this.buttonEnterShopSalesReceipt.Click += new System.EventHandler(this.buttonEnterShopSalesReceipt_Click);
            // 
            // buttonFormShopSalesInvoiceDetails
            // 
            this.buttonFormShopSalesInvoiceDetails.Location = new System.Drawing.Point(30, 323);
            this.buttonFormShopSalesInvoiceDetails.Name = "buttonFormShopSalesInvoiceDetails";
            this.buttonFormShopSalesInvoiceDetails.Size = new System.Drawing.Size(477, 109);
            this.buttonFormShopSalesInvoiceDetails.TabIndex = 6;
            this.buttonFormShopSalesInvoiceDetails.Text = "Edit and Print Sales Invoice";
            this.buttonFormShopSalesInvoiceDetails.UseVisualStyleBackColor = true;
            this.buttonFormShopSalesInvoiceDetails.Click += new System.EventHandler(this.buttonFormShopSalesInvoiceDetails_Click);
            // 
            // buttonCreateDeliveryChallan
            // 
            this.buttonCreateDeliveryChallan.Location = new System.Drawing.Point(74, 75);
            this.buttonCreateDeliveryChallan.Name = "buttonCreateDeliveryChallan";
            this.buttonCreateDeliveryChallan.Size = new System.Drawing.Size(477, 109);
            this.buttonCreateDeliveryChallan.TabIndex = 7;
            this.buttonCreateDeliveryChallan.Text = "Create Delivery Challan";
            this.buttonCreateDeliveryChallan.UseVisualStyleBackColor = true;
            this.buttonCreateDeliveryChallan.Click += new System.EventHandler(this.buttonCreateDeliveryChallan_Click);
            // 
            // buttonEditAndPrintDeliveryChallan
            // 
            this.buttonEditAndPrintDeliveryChallan.Location = new System.Drawing.Point(51, 323);
            this.buttonEditAndPrintDeliveryChallan.Name = "buttonEditAndPrintDeliveryChallan";
            this.buttonEditAndPrintDeliveryChallan.Size = new System.Drawing.Size(477, 109);
            this.buttonEditAndPrintDeliveryChallan.TabIndex = 8;
            this.buttonEditAndPrintDeliveryChallan.Text = "Edit and Print Delivery Challan";
            this.buttonEditAndPrintDeliveryChallan.UseVisualStyleBackColor = true;
            this.buttonEditAndPrintDeliveryChallan.Click += new System.EventHandler(this.buttonEditAndPrintDeliveryChallan_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonEvaluateLocalStock);
            this.groupBox1.Controls.Add(this.inventoryManagement);
            this.groupBox1.Controls.Add(this.buttonShopManagement);
            this.groupBox1.Controls.Add(this.buttonUpdateShopStock);
            this.groupBox1.Location = new System.Drawing.Point(1287, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(577, 623);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Management";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonComputeDemand);
            this.groupBox2.Controls.Add(this.buttonCreateDeliveryChallanKohinoor);
            this.groupBox2.Controls.Add(this.buttonEditAndPrintDeliveryChallan);
            this.groupBox2.Location = new System.Drawing.Point(23, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(588, 623);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Delivery Challan";
            // 
            // buttonComputeDemand
            // 
            this.buttonComputeDemand.Location = new System.Drawing.Point(51, 467);
            this.buttonComputeDemand.Name = "buttonComputeDemand";
            this.buttonComputeDemand.Size = new System.Drawing.Size(477, 109);
            this.buttonComputeDemand.TabIndex = 16;
            this.buttonComputeDemand.Text = "Compute Demand";
            this.buttonComputeDemand.UseVisualStyleBackColor = true;
            this.buttonComputeDemand.Click += new System.EventHandler(this.buttonComputeDemand_Click);
            // 
            // buttonCreateDeliveryChallanKohinoor
            // 
            this.buttonCreateDeliveryChallanKohinoor.Location = new System.Drawing.Point(51, 183);
            this.buttonCreateDeliveryChallanKohinoor.Name = "buttonCreateDeliveryChallanKohinoor";
            this.buttonCreateDeliveryChallanKohinoor.Size = new System.Drawing.Size(477, 109);
            this.buttonCreateDeliveryChallanKohinoor.TabIndex = 15;
            this.buttonCreateDeliveryChallanKohinoor.Text = "Create Delivery Challan Koh-i-noor ";
            this.buttonCreateDeliveryChallanKohinoor.UseVisualStyleBackColor = true;
            this.buttonCreateDeliveryChallanKohinoor.Click += new System.EventHandler(this.buttonCreateDeliveryChallanKohinoor_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonEnterShopSalesInvoiceKohinoor);
            this.groupBox3.Controls.Add(this.buttonTaxSummary);
            this.groupBox3.Controls.Add(this.buttonEnterShopSalesReceipt);
            this.groupBox3.Controls.Add(this.buttonFormShopSalesInvoiceDetails);
            this.groupBox3.Location = new System.Drawing.Point(663, 28);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(588, 623);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Sales Invoice";
            // 
            // buttonEnterShopSalesInvoiceKohinoor
            // 
            this.buttonEnterShopSalesInvoiceKohinoor.Location = new System.Drawing.Point(30, 179);
            this.buttonEnterShopSalesInvoiceKohinoor.Name = "buttonEnterShopSalesInvoiceKohinoor";
            this.buttonEnterShopSalesInvoiceKohinoor.Size = new System.Drawing.Size(477, 109);
            this.buttonEnterShopSalesInvoiceKohinoor.TabIndex = 8;
            this.buttonEnterShopSalesInvoiceKohinoor.Text = "Enter Koh-i-noor Sales Invoice";
            this.buttonEnterShopSalesInvoiceKohinoor.UseVisualStyleBackColor = true;
            this.buttonEnterShopSalesInvoiceKohinoor.Click += new System.EventHandler(this.buttonEnterShopSalesInvoiceKohinoor_Click);
            // 
            // buttonTaxSummary
            // 
            this.buttonTaxSummary.Location = new System.Drawing.Point(30, 457);
            this.buttonTaxSummary.Name = "buttonTaxSummary";
            this.buttonTaxSummary.Size = new System.Drawing.Size(477, 109);
            this.buttonTaxSummary.TabIndex = 7;
            this.buttonTaxSummary.Text = "Tax Summary";
            this.buttonTaxSummary.UseVisualStyleBackColor = true;
            this.buttonTaxSummary.Click += new System.EventHandler(this.buttonTaxSummary_Click);
            // 
            // buttonDebugShopSalesInvoice
            // 
            this.buttonDebugShopSalesInvoice.Location = new System.Drawing.Point(788, 771);
            this.buttonDebugShopSalesInvoice.Name = "buttonDebugShopSalesInvoice";
            this.buttonDebugShopSalesInvoice.Size = new System.Drawing.Size(343, 61);
            this.buttonDebugShopSalesInvoice.TabIndex = 5;
            this.buttonDebugShopSalesInvoice.Text = "Debug Shop Sales Invoice";
            this.buttonDebugShopSalesInvoice.UseVisualStyleBackColor = true;
            this.buttonDebugShopSalesInvoice.Click += new System.EventHandler(this.buttonDebugShopSalesInvoice_Click);
            // 
            // buttonDebugDeliveryChallan
            // 
            this.buttonDebugDeliveryChallan.Location = new System.Drawing.Point(788, 693);
            this.buttonDebugDeliveryChallan.Name = "buttonDebugDeliveryChallan";
            this.buttonDebugDeliveryChallan.Size = new System.Drawing.Size(343, 57);
            this.buttonDebugDeliveryChallan.TabIndex = 12;
            this.buttonDebugDeliveryChallan.Text = "Debug Deliery Challan";
            this.buttonDebugDeliveryChallan.UseVisualStyleBackColor = true;
            this.buttonDebugDeliveryChallan.Click += new System.EventHandler(this.buttonDebugDeliveryChallan_Click);
            // 
            // buttonBackupDatabase
            // 
            this.buttonBackupDatabase.Location = new System.Drawing.Point(1232, 693);
            this.buttonBackupDatabase.Name = "buttonBackupDatabase";
            this.buttonBackupDatabase.Size = new System.Drawing.Size(343, 57);
            this.buttonBackupDatabase.TabIndex = 13;
            this.buttonBackupDatabase.Text = "Backup Database";
            this.buttonBackupDatabase.UseVisualStyleBackColor = true;
            this.buttonBackupDatabase.Click += new System.EventHandler(this.buttonBackupDatabase_Click);
            // 
            // buttonRestoreDatabase
            // 
            this.buttonRestoreDatabase.Location = new System.Drawing.Point(1232, 773);
            this.buttonRestoreDatabase.Name = "buttonRestoreDatabase";
            this.buttonRestoreDatabase.Size = new System.Drawing.Size(343, 57);
            this.buttonRestoreDatabase.TabIndex = 14;
            this.buttonRestoreDatabase.Text = "Restore Database";
            this.buttonRestoreDatabase.UseVisualStyleBackColor = true;
            this.buttonRestoreDatabase.Click += new System.EventHandler(this.buttonRestoreDatabase_Click);
            // 
            // buttonDebugShipmentReceived
            // 
            this.buttonDebugShipmentReceived.Location = new System.Drawing.Point(788, 851);
            this.buttonDebugShipmentReceived.Name = "buttonDebugShipmentReceived";
            this.buttonDebugShipmentReceived.Size = new System.Drawing.Size(343, 57);
            this.buttonDebugShipmentReceived.TabIndex = 15;
            this.buttonDebugShipmentReceived.Text = "Debug Shipment Received";
            this.buttonDebugShipmentReceived.UseVisualStyleBackColor = true;
            this.buttonDebugShipmentReceived.Click += new System.EventHandler(this.buttonDebugShipmentReceived_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonCreateShipmentReceived);
            this.groupBox4.Controls.Add(this.buttonEditShipmentReceived);
            this.groupBox4.Location = new System.Drawing.Point(53, 693);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(588, 495);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Shipment Received";
            // 
            // buttonCreateShipmentReceived
            // 
            this.buttonCreateShipmentReceived.Location = new System.Drawing.Point(37, 69);
            this.buttonCreateShipmentReceived.Name = "buttonCreateShipmentReceived";
            this.buttonCreateShipmentReceived.Size = new System.Drawing.Size(477, 109);
            this.buttonCreateShipmentReceived.TabIndex = 15;
            this.buttonCreateShipmentReceived.Text = "Enter Shipment Received";
            this.buttonCreateShipmentReceived.UseVisualStyleBackColor = true;
            this.buttonCreateShipmentReceived.Click += new System.EventHandler(this.buttonCreateShipmentReceived_Click);
            // 
            // buttonEditShipmentReceived
            // 
            this.buttonEditShipmentReceived.Location = new System.Drawing.Point(37, 210);
            this.buttonEditShipmentReceived.Name = "buttonEditShipmentReceived";
            this.buttonEditShipmentReceived.Size = new System.Drawing.Size(477, 109);
            this.buttonEditShipmentReceived.TabIndex = 8;
            this.buttonEditShipmentReceived.Text = "Edit Shipment Received";
            this.buttonEditShipmentReceived.UseVisualStyleBackColor = true;
            this.buttonEditShipmentReceived.Click += new System.EventHandler(this.buttonEditShipmentReceived_Click);
            // 
            // buttonDebugShopStatus
            // 
            this.buttonDebugShopStatus.Location = new System.Drawing.Point(788, 931);
            this.buttonDebugShopStatus.Name = "buttonDebugShopStatus";
            this.buttonDebugShopStatus.Size = new System.Drawing.Size(343, 57);
            this.buttonDebugShopStatus.TabIndex = 17;
            this.buttonDebugShopStatus.Text = "Debug Shop Status";
            this.buttonDebugShopStatus.UseVisualStyleBackColor = true;
            this.buttonDebugShopStatus.Click += new System.EventHandler(this.buttonDebugShopStatus_Click);
            // 
            // buttonEvaluateLocalStock
            // 
            this.buttonEvaluateLocalStock.Location = new System.Drawing.Point(31, 457);
            this.buttonEvaluateLocalStock.Name = "buttonEvaluateLocalStock";
            this.buttonEvaluateLocalStock.Size = new System.Drawing.Size(477, 109);
            this.buttonEvaluateLocalStock.TabIndex = 5;
            this.buttonEvaluateLocalStock.Text = "Evaluate Local Stock";
            this.buttonEvaluateLocalStock.UseVisualStyleBackColor = true;
            this.buttonEvaluateLocalStock.Click += new System.EventHandler(this.buttonEvaluateLocalStock_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(2444, 1559);
            this.Controls.Add(this.buttonDebugShopStatus);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.buttonDebugShipmentReceived);
            this.Controls.Add(this.buttonRestoreDatabase);
            this.Controls.Add(this.buttonBackupDatabase);
            this.Controls.Add(this.buttonDebugDeliveryChallan);
            this.Controls.Add(this.buttonDebugShopSalesInvoice);
            this.Controls.Add(this.buttonCreateDeliveryChallan);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button inventoryManagement;
        private System.Windows.Forms.Button buttonShopManagement;
        private System.Windows.Forms.Button buttonUpdateShopStock;
        private System.Windows.Forms.Button buttonEnterShopSalesReceipt;
        private System.Windows.Forms.Button buttonFormShopSalesInvoiceDetails;
        private System.Windows.Forms.Button buttonCreateDeliveryChallan;
        private System.Windows.Forms.Button buttonEditAndPrintDeliveryChallan;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonDebugShopSalesInvoice;
        private System.Windows.Forms.Button buttonDebugDeliveryChallan;
        private System.Windows.Forms.Button buttonBackupDatabase;
        private System.Windows.Forms.Button buttonRestoreDatabase;
        private System.Windows.Forms.Button buttonTaxSummary;
        private System.Windows.Forms.Button buttonCreateDeliveryChallanKohinoor;
        private System.Windows.Forms.Button buttonEnterShopSalesInvoiceKohinoor;
        private System.Windows.Forms.Button buttonDebugShipmentReceived;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonCreateShipmentReceived;
        private System.Windows.Forms.Button buttonEditShipmentReceived;
        private System.Windows.Forms.Button buttonDebugShopStatus;
        private System.Windows.Forms.Button buttonComputeDemand;
        private System.Windows.Forms.Button buttonEvaluateLocalStock;
    }
}


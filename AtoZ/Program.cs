﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtoZ
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            LoadTest loadTest = new LoadTest();
            //loadTest.addDeliveryChallan(41, 500);
 //           loadTest.addSalesInvoice(0, 500);
            Application.Run(new Form1());
           // loadTest.deleteDeliveryChallan(1, 200);
        }
    }
}

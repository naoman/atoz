﻿using AtoZ.reports;
using AtoZ.datasets;

namespace AtoZ
{
    partial class FormCreateDeliveryChallan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.shopsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet10 = new AtoZ.datasets.AtoZDatabaseDataSet10();
            this.atoZDatabaseDataSet7 = new AtoZ.datasets.AtoZDatabaseDataSet7();
            this.atoZDatabaseDataSet7BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet7BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.shopsTableAdapter = new AtoZ.datasets.AtoZDatabaseDataSet10TableAdapters.shopsTableAdapter();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.invoiceIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invoiceDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopSalesInvoiceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet11 = new AtoZ.datasets.AtoZDatabaseDataSet11();
            this.atoZDatabaseDataSet_ShopSalesInvoice = new AtoZ.datasets.AtoZDatabaseDataSet_ShopSalesInvoice();
            this.atoZDatabaseDataSetShopSalesInvoiceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fillByToolStrip = new System.Windows.Forms.ToolStrip();
            this.shopIdToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.shopIdToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.fillByToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.shopSalesInvoiceTableAdapter = new AtoZ.datasets.AtoZDatabaseDataSet11TableAdapters.ShopSalesInvoiceTableAdapter();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.challanIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.challanDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deliveryChallanBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atoZDatabaseDataSet12 = new AtoZ.datasets.AtoZDatabaseDataSet12();
            this.fillByToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.shopIdToolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.shopIdToolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.fillByToolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.deliveryChallanTableAdapter = new AtoZ.datasets.AtoZDatabaseDataSet12TableAdapters.DeliveryChallanTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.labelShopId = new System.Windows.Forms.Label();
            this.labelLastInvoice2 = new System.Windows.Forms.Label();
            this.labelInvoiceID = new System.Windows.Forms.Label();
            this.labelLastDeliveryChallanId2 = new System.Windows.Forms.Label();
            this.labelChallanID = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonCreateChallan = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxNewOrderId = new System.Windows.Forms.TextBox();
            this.textBoxNewChallanId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonLoadAllItems = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.newDemand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.catNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentStock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastSale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastDelivery = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet7BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet7BindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet_ShopSalesInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSetShopSalesInvoiceBindingSource)).BeginInit();
            this.fillByToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryChallanBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet12)).BeginInit();
            this.fillByToolStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.shopsBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.tableLayoutPanel1.SetRowSpan(this.dataGridView1, 2);
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(511, 384);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            this.dataGridView1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridView1_KeyDown);
            this.dataGridView1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyUp);
            // 
            // shopsBindingSource
            // 
            this.shopsBindingSource.DataMember = "shops";
            this.shopsBindingSource.DataSource = this.atoZDatabaseDataSet10;
            // 
            // atoZDatabaseDataSet10
            // 
            this.atoZDatabaseDataSet10.DataSetName = "AtoZDatabaseDataSet10";
            this.atoZDatabaseDataSet10.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // atoZDatabaseDataSet7
            // 
            this.atoZDatabaseDataSet7.DataSetName = "AtoZDatabaseDataSet7";
            this.atoZDatabaseDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // atoZDatabaseDataSet7BindingSource
            // 
            this.atoZDatabaseDataSet7BindingSource.DataSource = this.atoZDatabaseDataSet7;
            this.atoZDatabaseDataSet7BindingSource.Position = 0;
            // 
            // atoZDatabaseDataSet7BindingSource1
            // 
            this.atoZDatabaseDataSet7BindingSource1.DataSource = this.atoZDatabaseDataSet7;
            this.atoZDatabaseDataSet7BindingSource1.Position = 0;
            // 
            // shopsTableAdapter
            // 
            this.shopsTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.invoiceIdDataGridViewTextBoxColumn,
            this.invoiceDateDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.shopSalesInvoiceBindingSource;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 393);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.tableLayoutPanel1.SetRowSpan(this.dataGridView2, 2);
            this.dataGridView2.RowTemplate.Height = 33;
            this.dataGridView2.Size = new System.Drawing.Size(511, 384);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            this.dataGridView2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView2_KeyDown);
            this.dataGridView2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridView2_KeyPress);
            this.dataGridView2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridView2_KeyUp);
            // 
            // invoiceIdDataGridViewTextBoxColumn
            // 
            this.invoiceIdDataGridViewTextBoxColumn.DataPropertyName = "invoiceId";
            this.invoiceIdDataGridViewTextBoxColumn.HeaderText = "invoiceId";
            this.invoiceIdDataGridViewTextBoxColumn.Name = "invoiceIdDataGridViewTextBoxColumn";
            this.invoiceIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.invoiceIdDataGridViewTextBoxColumn.Width = 122;
            // 
            // invoiceDateDataGridViewTextBoxColumn
            // 
            this.invoiceDateDataGridViewTextBoxColumn.DataPropertyName = "invoiceDate";
            this.invoiceDateDataGridViewTextBoxColumn.HeaderText = "invoiceDate";
            this.invoiceDateDataGridViewTextBoxColumn.Name = "invoiceDateDataGridViewTextBoxColumn";
            this.invoiceDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.invoiceDateDataGridViewTextBoxColumn.Width = 150;
            // 
            // shopSalesInvoiceBindingSource
            // 
            this.shopSalesInvoiceBindingSource.DataMember = "ShopSalesInvoice";
            this.shopSalesInvoiceBindingSource.DataSource = this.atoZDatabaseDataSet11;
            // 
            // atoZDatabaseDataSet11
            // 
            this.atoZDatabaseDataSet11.DataSetName = "AtoZDatabaseDataSet11";
            this.atoZDatabaseDataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // atoZDatabaseDataSet_ShopSalesInvoice
            // 
            this.atoZDatabaseDataSet_ShopSalesInvoice.DataSetName = "AtoZDatabaseDataSet_ShopSalesInvoice";
            this.atoZDatabaseDataSet_ShopSalesInvoice.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // atoZDatabaseDataSetShopSalesInvoiceBindingSource
            // 
            this.atoZDatabaseDataSetShopSalesInvoiceBindingSource.DataSource = this.atoZDatabaseDataSet_ShopSalesInvoice;
            this.atoZDatabaseDataSetShopSalesInvoiceBindingSource.Position = 0;
            // 
            // fillByToolStrip
            // 
            this.fillByToolStrip.Enabled = false;
            this.fillByToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shopIdToolStripLabel,
            this.shopIdToolStripTextBox,
            this.fillByToolStripButton});
            this.fillByToolStrip.Location = new System.Drawing.Point(0, 0);
            this.fillByToolStrip.Name = "fillByToolStrip";
            this.fillByToolStrip.Size = new System.Drawing.Size(1908, 39);
            this.fillByToolStrip.TabIndex = 11;
            this.fillByToolStrip.Text = "fillByToolStrip";
            // 
            // shopIdToolStripLabel
            // 
            this.shopIdToolStripLabel.Name = "shopIdToolStripLabel";
            this.shopIdToolStripLabel.Size = new System.Drawing.Size(92, 36);
            this.shopIdToolStripLabel.Text = "shopId:";
            // 
            // shopIdToolStripTextBox
            // 
            this.shopIdToolStripTextBox.Name = "shopIdToolStripTextBox";
            this.shopIdToolStripTextBox.Size = new System.Drawing.Size(100, 39);
            // 
            // fillByToolStripButton
            // 
            this.fillByToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fillByToolStripButton.Name = "fillByToolStripButton";
            this.fillByToolStripButton.Size = new System.Drawing.Size(75, 36);
            this.fillByToolStripButton.Text = "FillBy";
            this.fillByToolStripButton.Click += new System.EventHandler(this.fillByToolStripButton_Click);
            // 
            // shopSalesInvoiceTableAdapter
            // 
            this.shopSalesInvoiceTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.challanIdDataGridViewTextBoxColumn,
            this.challanDateDataGridViewTextBoxColumn});
            this.dataGridView3.DataSource = this.deliveryChallanBindingSource;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(3, 783);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.tableLayoutPanel1.SetRowSpan(this.dataGridView3, 2);
            this.dataGridView3.RowTemplate.Height = 33;
            this.dataGridView3.Size = new System.Drawing.Size(511, 519);
            this.dataGridView3.TabIndex = 0;
            this.dataGridView3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellClick);
            this.dataGridView3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView3_KeyDown);
            this.dataGridView3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridView3_KeyPress);
            this.dataGridView3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridView3_KeyUp);
            // 
            // challanIdDataGridViewTextBoxColumn
            // 
            this.challanIdDataGridViewTextBoxColumn.DataPropertyName = "challanId";
            this.challanIdDataGridViewTextBoxColumn.HeaderText = "challanId";
            this.challanIdDataGridViewTextBoxColumn.Name = "challanIdDataGridViewTextBoxColumn";
            this.challanIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.challanIdDataGridViewTextBoxColumn.Width = 123;
            // 
            // challanDateDataGridViewTextBoxColumn
            // 
            this.challanDateDataGridViewTextBoxColumn.DataPropertyName = "challanDate";
            this.challanDateDataGridViewTextBoxColumn.HeaderText = "challanDate";
            this.challanDateDataGridViewTextBoxColumn.Name = "challanDateDataGridViewTextBoxColumn";
            this.challanDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.challanDateDataGridViewTextBoxColumn.Width = 151;
            // 
            // deliveryChallanBindingSource
            // 
            this.deliveryChallanBindingSource.DataMember = "DeliveryChallan";
            this.deliveryChallanBindingSource.DataSource = this.atoZDatabaseDataSet12;
            // 
            // atoZDatabaseDataSet12
            // 
            this.atoZDatabaseDataSet12.DataSetName = "AtoZDatabaseDataSet12";
            this.atoZDatabaseDataSet12.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fillByToolStrip1
            // 
            this.fillByToolStrip1.Enabled = false;
            this.fillByToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shopIdToolStripLabel1,
            this.shopIdToolStripTextBox1,
            this.fillByToolStripButton1});
            this.fillByToolStrip1.Location = new System.Drawing.Point(0, 39);
            this.fillByToolStrip1.Name = "fillByToolStrip1";
            this.fillByToolStrip1.Size = new System.Drawing.Size(1908, 39);
            this.fillByToolStrip1.TabIndex = 12;
            this.fillByToolStrip1.Text = "fillByToolStrip1";
            // 
            // shopIdToolStripLabel1
            // 
            this.shopIdToolStripLabel1.Name = "shopIdToolStripLabel1";
            this.shopIdToolStripLabel1.Size = new System.Drawing.Size(92, 36);
            this.shopIdToolStripLabel1.Text = "shopId:";
            // 
            // shopIdToolStripTextBox1
            // 
            this.shopIdToolStripTextBox1.Name = "shopIdToolStripTextBox1";
            this.shopIdToolStripTextBox1.Size = new System.Drawing.Size(100, 39);
            // 
            // fillByToolStripButton1
            // 
            this.fillByToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fillByToolStripButton1.Name = "fillByToolStripButton1";
            this.fillByToolStripButton1.Size = new System.Drawing.Size(75, 36);
            this.fillByToolStripButton1.Text = "FillBy";
            this.fillByToolStripButton1.Click += new System.EventHandler(this.fillByToolStripButton1_Click);
            // 
            // deliveryChallanTableAdapter
            // 
            this.deliveryChallanTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 25);
            this.label1.TabIndex = 13;
            this.label1.Text = "Shop: ";
            // 
            // labelShopId
            // 
            this.labelShopId.AutoSize = true;
            this.labelShopId.Location = new System.Drawing.Point(89, 143);
            this.labelShopId.Name = "labelShopId";
            this.labelShopId.Size = new System.Drawing.Size(352, 25);
            this.labelShopId.TabIndex = 14;
            this.labelShopId.Text = "Shop Name                                      ";
            // 
            // labelLastInvoice2
            // 
            this.labelLastInvoice2.AutoSize = true;
            this.labelLastInvoice2.Location = new System.Drawing.Point(21, 187);
            this.labelLastInvoice2.Name = "labelLastInvoice2";
            this.labelLastInvoice2.Size = new System.Drawing.Size(219, 25);
            this.labelLastInvoice2.TabIndex = 15;
            this.labelLastInvoice2.Text = "Last Sales Invoice ID:";
            // 
            // labelInvoiceID
            // 
            this.labelInvoiceID.AutoSize = true;
            this.labelInvoiceID.Location = new System.Drawing.Point(212, 187);
            this.labelInvoiceID.Name = "labelInvoiceID";
            this.labelInvoiceID.Size = new System.Drawing.Size(0, 25);
            this.labelInvoiceID.TabIndex = 16;
            // 
            // labelLastDeliveryChallanId2
            // 
            this.labelLastDeliveryChallanId2.AutoSize = true;
            this.labelLastDeliveryChallanId2.Location = new System.Drawing.Point(21, 253);
            this.labelLastDeliveryChallanId2.Name = "labelLastDeliveryChallanId2";
            this.labelLastDeliveryChallanId2.Size = new System.Drawing.Size(248, 25);
            this.labelLastDeliveryChallanId2.TabIndex = 17;
            this.labelLastDeliveryChallanId2.Text = "Last Delivery Challan ID:";
            // 
            // labelChallanID
            // 
            this.labelChallanID.AutoSize = true;
            this.labelChallanID.Location = new System.Drawing.Point(659, 187);
            this.labelChallanID.Name = "labelChallanID";
            this.labelChallanID.Size = new System.Drawing.Size(0, 25);
            this.labelChallanID.TabIndex = 18;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonCreateChallan);
            this.groupBox4.Controls.Add(this.dateTimePicker1);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.textBoxNewOrderId);
            this.groupBox4.Controls.Add(this.textBoxNewChallanId);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Location = new System.Drawing.Point(623, 47);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(642, 182);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Create New Challan";
            // 
            // buttonCreateChallan
            // 
            this.buttonCreateChallan.Location = new System.Drawing.Point(452, 91);
            this.buttonCreateChallan.Name = "buttonCreateChallan";
            this.buttonCreateChallan.Size = new System.Drawing.Size(177, 61);
            this.buttonCreateChallan.TabIndex = 6;
            this.buttonCreateChallan.Text = "Create";
            this.buttonCreateChallan.UseVisualStyleBackColor = true;
            this.buttonCreateChallan.Click += new System.EventHandler(this.buttonCreateChallan_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(154, 141);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(274, 31);
            this.dateTimePicker1.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 25);
            this.label6.TabIndex = 4;
            this.label6.Text = "Date";
            // 
            // textBoxNewOrderId
            // 
            this.textBoxNewOrderId.Location = new System.Drawing.Point(154, 85);
            this.textBoxNewOrderId.Name = "textBoxNewOrderId";
            this.textBoxNewOrderId.Size = new System.Drawing.Size(274, 31);
            this.textBoxNewOrderId.TabIndex = 3;
            // 
            // textBoxNewChallanId
            // 
            this.textBoxNewChallanId.Location = new System.Drawing.Point(154, 40);
            this.textBoxNewChallanId.Name = "textBoxNewChallanId";
            this.textBoxNewChallanId.Size = new System.Drawing.Size(274, 31);
            this.textBoxNewChallanId.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 25);
            this.label4.TabIndex = 1;
            this.label4.Text = "PO No:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Challan ID: ";
            // 
            // buttonLoadAllItems
            // 
            this.buttonLoadAllItems.Location = new System.Drawing.Point(118, 47);
            this.buttonLoadAllItems.Name = "buttonLoadAllItems";
            this.buttonLoadAllItems.Size = new System.Drawing.Size(256, 58);
            this.buttonLoadAllItems.TabIndex = 22;
            this.buttonLoadAllItems.Text = "Load All Items";
            this.buttonLoadAllItems.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.buttonLoadAllItems.UseVisualStyleBackColor = true;
            this.buttonLoadAllItems.Visible = false;
            this.buttonLoadAllItems.Click += new System.EventHandler(this.buttonLoadAllItems_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelShopId);
            this.panel1.Controls.Add(this.buttonLoadAllItems);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.labelLastInvoice2);
            this.panel1.Controls.Add(this.labelInvoiceID);
            this.panel1.Controls.Add(this.labelLastDeliveryChallanId2);
            this.panel1.Controls.Add(this.labelChallanID);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(520, 3);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(1385, 384);
            this.panel1.TabIndex = 23;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.10049F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.89951F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridView3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView4, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 78);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1908, 1305);
            this.tableLayoutPanel1.TabIndex = 24;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AllowUserToDeleteRows = false;
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.newDemand,
            this.catNo,
            this.itemName,
            this.currentStock,
            this.lastSale,
            this.lastDelivery});
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.Enabled = false;
            this.dataGridView4.Location = new System.Drawing.Point(520, 393);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.RowHeadersWidth = 60;
            this.tableLayoutPanel1.SetRowSpan(this.dataGridView4, 4);
            this.dataGridView4.RowTemplate.Height = 33;
            this.dataGridView4.Size = new System.Drawing.Size(1385, 909);
            this.dataGridView4.TabIndex = 25;
            this.dataGridView4.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellValueChanged);
            // 
            // newDemand
            // 
            this.newDemand.HeaderText = "New Demand";
            this.newDemand.Name = "newDemand";
            this.newDemand.Width = 165;
            // 
            // catNo
            // 
            this.catNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.catNo.HeaderText = "Cat No";
            this.catNo.Name = "catNo";
            this.catNo.ReadOnly = true;
            this.catNo.Width = 103;
            // 
            // itemName
            // 
            this.itemName.HeaderText = "Item Name";
            this.itemName.Name = "itemName";
            this.itemName.ReadOnly = true;
            this.itemName.Width = 139;
            // 
            // currentStock
            // 
            this.currentStock.HeaderText = "Current Stock";
            this.currentStock.Name = "currentStock";
            this.currentStock.ReadOnly = true;
            this.currentStock.Width = 168;
            // 
            // lastSale
            // 
            this.lastSale.HeaderText = "Last Sale";
            this.lastSale.Name = "lastSale";
            this.lastSale.ReadOnly = true;
            this.lastSale.Width = 127;
            // 
            // lastDelivery
            // 
            this.lastDelivery.HeaderText = "Last Delivery";
            this.lastDelivery.Name = "lastDelivery";
            this.lastDelivery.ReadOnly = true;
            this.lastDelivery.Width = 162;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Width = 54;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Width = 90;
            // 
            // FormCreateDeliveryChallan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1908, 1383);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.fillByToolStrip1);
            this.Controls.Add(this.fillByToolStrip);
            this.Name = "FormCreateDeliveryChallan";
            this.Text = "FormCreateDeliveryChallan";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormCreateDeliveryChallan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet7BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet7BindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopSalesInvoiceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet_ShopSalesInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSetShopSalesInvoiceBindingSource)).EndInit();
            this.fillByToolStrip.ResumeLayout(false);
            this.fillByToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryChallanBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atoZDatabaseDataSet12)).EndInit();
            this.fillByToolStrip1.ResumeLayout(false);
            this.fillByToolStrip1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource atoZDatabaseDataSet7BindingSource1;
        private AtoZDatabaseDataSet7 atoZDatabaseDataSet7;
        private System.Windows.Forms.BindingSource atoZDatabaseDataSet7BindingSource;
        private AtoZDatabaseDataSet10 atoZDatabaseDataSet10;
        private System.Windows.Forms.BindingSource shopsBindingSource;
        private AtoZ.datasets.AtoZDatabaseDataSet10TableAdapters.shopsTableAdapter shopsTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView2;
        private AtoZDatabaseDataSet_ShopSalesInvoice atoZDatabaseDataSet_ShopSalesInvoice;
        private System.Windows.Forms.BindingSource atoZDatabaseDataSetShopSalesInvoiceBindingSource;
        private AtoZDatabaseDataSet11 atoZDatabaseDataSet11;
        private System.Windows.Forms.BindingSource shopSalesInvoiceBindingSource;
        private AtoZ.datasets.AtoZDatabaseDataSet11TableAdapters.ShopSalesInvoiceTableAdapter shopSalesInvoiceTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn invoiceIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn invoiceDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStrip fillByToolStrip;
        private System.Windows.Forms.ToolStripLabel shopIdToolStripLabel;
        private System.Windows.Forms.ToolStripTextBox shopIdToolStripTextBox;
        private System.Windows.Forms.ToolStripButton fillByToolStripButton;
        private System.Windows.Forms.DataGridView dataGridView3;
        private AtoZDatabaseDataSet12 atoZDatabaseDataSet12;
        private System.Windows.Forms.BindingSource deliveryChallanBindingSource;
        private AtoZ.datasets.AtoZDatabaseDataSet12TableAdapters.DeliveryChallanTableAdapter deliveryChallanTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn challanIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn challanDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStrip fillByToolStrip1;
        private System.Windows.Forms.ToolStripLabel shopIdToolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox shopIdToolStripTextBox1;
        private System.Windows.Forms.ToolStripButton fillByToolStripButton1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelShopId;
        private System.Windows.Forms.Label labelLastInvoice2;
        private System.Windows.Forms.Label labelInvoiceID;
        private System.Windows.Forms.Label labelLastDeliveryChallanId2;
        private System.Windows.Forms.Label labelChallanID;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxNewOrderId;
        private System.Windows.Forms.TextBox textBoxNewChallanId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonCreateChallan;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonLoadAllItems;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridViewTextBoxColumn newDemand;
        private System.Windows.Forms.DataGridViewTextBoxColumn catNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentStock;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastSale;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastDelivery;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
    }
}
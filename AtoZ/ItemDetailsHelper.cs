﻿using AtoZ.datasets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtoZ
{
    public class ItemDetailsHelper
    {
        public const string CUSTOM_2_POST_TAX = "POST_TAX";

        private static Dictionary<int, ItemDetailsData> itemsHashSet = null;
        public static void refreshData()
        {
            AtoZDatabaseDataSet_Items dataSet = new AtoZDatabaseDataSet_Items();
            AtoZ.datasets.AtoZDatabaseDataSet_ItemsTableAdapters.itemsTableAdapter tableAdapter =
                new AtoZ.datasets.AtoZDatabaseDataSet_ItemsTableAdapters.itemsTableAdapter();


            int count = tableAdapter.Fill(dataSet.items);

            itemsHashSet = new Dictionary<int,ItemDetailsData>();
            foreach(var item in dataSet.items)
            {
                ItemCategory itemCategory = ItemCategory.None;

                if (!item.IscategoryNull() &&
                    !String.IsNullOrWhiteSpace(item.category))
                {
                    itemCategory = (ItemCategory)int.Parse(item.category);
                }

                double? specTax = null;
                if (!item.Iscustom1Null() &&
                    !String.IsNullOrWhiteSpace(item.custom1))
                {
                    specTax = double.Parse(item.custom1);
                }

                itemsHashSet.Add(item.catNo, new ItemDetailsData() { 
                    name = item.name,
                    price = item.price,
                    tax = item.tax,
                    category = itemCategory,
                    actualTax = specTax,
                    custom2 = item.Iscustom2Null()?"":item.custom2,
                    stockSize = item.IsstockSizeNull()?0:item.stockSize,
                    custom3 = item.Iscustom3Null()?"":item.custom3
                });
            }
            return;
        }

        public static string getItemName(int itemId)
        {
            if (itemsHashSet == null)
            {
                refreshData();
            }
            return itemsHashSet[itemId].name;
        }

        public static ItemDetailsData getItemDetails(int itemId)
        {
            if(itemsHashSet == null)
            {
                refreshData();
            }
            return itemsHashSet[itemId];
        }

        public  static Dictionary<int, ItemDetailsData> getAllItems()
        {
            if (itemsHashSet == null)
            {
                refreshData();
            }
            Dictionary<int, ItemDetailsData> returnList = new Dictionary<int, ItemDetailsData>();
            foreach(var kvp in itemsHashSet)
            {
                if(!kvp.Value.custom2.Equals(ItemDetailsHelper.CUSTOM_2_POST_TAX))
                {
                    returnList.Add(kvp.Key,kvp.Value);
                }
            }
            return returnList;
        }

        internal static Dictionary<int, ItemDetailsData> getAllPostTaxItems()
        {
            if (itemsHashSet == null)
            {
                refreshData();
            }
            Dictionary<int, ItemDetailsData> returnList = new Dictionary<int, ItemDetailsData>();
            foreach (var kvp in itemsHashSet)
            {
                if (kvp.Value.custom2.Equals(ItemDetailsHelper.CUSTOM_2_POST_TAX))
                {
                    returnList.Add(kvp.Key, kvp.Value);
                }
            }
            return returnList;
        }
    }

    public class ItemDetailsData
    {
        public string name;
        public double price;
        public double tax;
        public ItemCategory category;
        public double? actualTax;
        public string custom2;
        public double stockSize;
        public string custom3;
    }
}
